<?php get_header(); ?>

<?php DefaultHook::homepage_slider(); ?>

<?php DefaultHook::homepage_jumbotron(); ?>

<?php DefaultHook::homepage_services(); ?>

<?php DefaultHook::homepage_quote(); ?>

<?php DefaultHook::contents_homepage(); ?>

<?php DefaultHook::footer(); ?>