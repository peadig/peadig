<?php $pd_options = get_option('peadig'); ?>


    </div> <!-- body -->


    <?php if(!empty($pd_options['sidebar_below_content'])) { ?>
        <div class="container">
            <div class="row">
                <div id="below-content-widgets" class="row col-lg-12 col-md-12 col-sm-12">
                    <?php dynamic_sidebar( 'below-content' ); ?>
                </div>
            </div>
        </div>
    <?php } ?>


    <?php CustomHook::footer_before_footer() ?>
	<footer class="container">
    <?php CustomHook::footer_top() ?>
        <div class="row">
            <hr>
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="source-org vcard copyright">
                    <div id="footer-widgets">
                        <?php dynamic_sidebar( 'footer' ); ?>
                    </div>
                    <div id="footer-text">
                        <?php
                        if(!empty($pd_options['footer_text'])) {
                            echo do_shortcode($pd_options['footer_text']);
                        } else {
                            echo '&copy; '.date('Y').' '.do_shortcode('[wp-url]');
                        }
                        if(!empty($pd_options['footer_att'])) {
                            echo '<span class="pull-right">Powered by <a href="http://peadig.com/">Peadig</a></span>';
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <?php CustomHook::footer_bottom() ?>
	</footer>
    <?php CustomHook::footer_after_footer() ?>

<?php CustomHook::footer_above_wp_footer() ?>
<?php wp_footer(); ?>
<?php CustomHook::footer_below_wp_footer() ?>

</body>
</html>