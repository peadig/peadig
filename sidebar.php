<?php
    $pd_options = get_option('peadig');
    $sidebar_span = (12-$pd_options['main_span']);
?>
    <div id="secondary" class="widget-area <?php echo 'col-lg-'.$sidebar_span.' col-md-'.$sidebar_span.' col-sm-'.$sidebar_span; ?>" role="complementary">

        <?php do_action( 'before_sidebar' ); ?>
        <?php if ( ! dynamic_sidebar( 'sidebar-1' ) ) { ?>
            <aside id="search" class="widget widget_search">
            <?php get_search_form(); ?>
            </aside>
            <aside id="archives" class="widget">
                <h1 class="widget-title"><?php _e( 'Archives', 'toolbox' ); ?></h1>
                <ul>
                    <?php wp_get_archives( array( 'type' => 'monthly' ) ); ?>
                </ul>
            </aside>
            <aside id="meta" class="widget">
                <h1 class="widget-title"><?php _e( 'Meta', 'toolbox' ); ?></h1>
                <ul>
                <?php wp_register(); ?>
                <aside><?php wp_loginout(); ?></aside>
                <?php wp_meta(); ?>
                </ul>
            </aside>
        <?php } else {
        //dynamic_sidebar( 'sidebar-1' );
        } // end sidebar widget area ?>
    </div><!-- #secondary .widget-area -->

    <?php if ( is_active_sidebar( 'sidebar-2' ) && !empty($pd_options['second_sidebar']) ) : ?>
    <div id="tertiary" class="widget-area <?php echo 'col-lg-'.$sidebar_span.' col-md-'.$sidebar_span.' col-sm-'.$sidebar_span; ?>" role="complementary">
        <?php dynamic_sidebar( 'sidebar-2' ); ?>
    </div><!-- #tertiary .widget-area -->
    <?php endif; ?>