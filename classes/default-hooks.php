<?php


/*
 * Default Hooks, to override just add in an action to dh_FUNCTION_NAME example below
 *
 *   function overwrite(){
         echo "hello";
     } add_action( 'dh_header',  'overwrite' );
 *
 *
 ********************************************************************************/








     class DefaultHook{

    /*
     * A footer with no sidebar
     *
     */

    static function sidebar(){
      if(has_action('dh_sidebar')) {
        do_action('dh_sidebar');
      } else {
        CustomHook::before_sidebar();
        get_sidebar();
        CustomHook::after_sidebar();
      }
    }




    /*
     * Normal footer
     *
     */

    static function footer(){
      if(has_action('dh_footer')) {
        do_action('dh_footer');
      } else {
        CustomHook::before_footer();
        get_footer();
        CustomHook::after_footer();
      }
    }




    /*
     * Nav for under posts
     *
     */

    static function nav(){
      if(has_action('dh_nav')) {
        do_action('dh_nav');
      } else { ?>
      <div class="navigation">
        <div class="next-posts"><?php next_posts_link('&laquo; Older Entries') ?></div>
        <div class="prev-posts"><?php previous_posts_link('Newer Entries &raquo;') ?></div>
      </div>
      <?php
    }
  }



    /*
     * Full header
     *
     */

    static function header() {
      if(has_action('dh_header')) {
        do_action('dh_header');
      } else {
        $pd_options = get_option('peadig');
        $home_url = home_url(); ?>

        <div class="<?php if($pd_options['use_header_social_icons']==1) {
          echo 'col-lg-6 col-md-6 col-sm-6';
        } else {
          echo 'col-lg-12 col-md-12 col-sm-12';
        }?>">

        <?php if(empty($pd_options['use_logo_image'])) { ?>
        <h2><a href="<?php echo $home_url; ?>"><?php echo get_bloginfo('name'); ?></a></h2>
        <p class="lead text-muted"><?php echo get_bloginfo('description'); ?></p>
        <?php } else { ?>
        <a href="<?php echo $home_url; ?>"><img id="logo" src="<?php
        if(!empty($pd_options['logo']['url'])) {
          echo $pd_options['logo']['url'].'" width="'.$pd_options['logo']['width'].'" height="'.$pd_options['logo']['height'].'"';
        } else {
          echo get_template_directory_uri().'/img/peadig-200x50.png" width="200" height="50"';
        }
        echo ' alt="'.get_bloginfo('name');
        ?>" class="img-responsive"></a>
        <?php } ?>
      </div>


      <?php if($pd_options['use_header_social_icons']==1) { ?>
      <div class="col-lg-6 col-md-6 col-sm-6">
        <?php if((!empty($pd_options['use_email']) && !empty($pd_options['email'])) || (!empty($pd_options['use_phone']) && !empty($pd_options['phone']))) { ?>
        <div class="contact col-lg-5 col-md-5 col-sm-5 col-lg-offset-7 col-md-offset-7 col-sm-offset-7">
          <address>
            <?php } ?>
            <?php if(!empty($pd_options['use_email']) && !empty($pd_options['email'])) {
                if ($pd_options['email_link']=='page' && !empty($pd_options['email_page'])) {$link=get_permalink($pd_options['email_page']);}
                else {$link='mailto:'.antispambot($pd_options['email']);}
              echo'<span class="email"><i class="fa fa-envelope"></i> <strong><a href="'.$link.'">'.antispambot($pd_options['email']).'</a></strong></span>';
            } ?>
            <?php if(!empty($pd_options['use_phone']) && !empty($pd_options['phone'])) {
              echo'<span class="phone"><i class="fa fa-phone-square"></i> <strong><a href="tel:'.$pd_options['phone'].'">'.$pd_options['phone'].'</a></strong></span>';
            } ?>

            <?php if((!empty($pd_options['use_email']) && !empty($pd_options['email'])) || (!empty($pd_options['use_phone']) && !empty($pd_options['phone']))) { ?>
          </address>
        </div>
        <?php } ?>
        <?php DefaultHook::header_social_links() ?>
      </div>


      <?php } ?>

      <?php

    }
  }










     /*
     * Social buttons on posts
     *
     */

     static function social_buttons() {
       if(has_action('dh_social_buttons')) {
         do_action('dh_social_buttons');
       } else {
         $pd_options = get_option('peadig');
         if (is_singular()) {
           $custom_fields = get_post_custom();
           if (!empty($custom_fields)) {
             foreach ($custom_fields as $field_key => $field_values) {
               foreach ($field_values as $key => $value)
                             $post_meta[$field_key] = $value; // builds array
                         }
                       }
                     }
                     $pd_options = get_option('peadig');
                     ?><div class="peadigsocial clearfix"><?php
                     if (!empty($pd_options['twitter_tweet'])) {
                       $twitter_profile = get_the_author_meta('twitter');
                       if (!empty($twitter_profile)) {
                         $byauthor = ' by @' . $twitter_profile;
                       } else {$byauthor='';}
                       if (!empty($post_meta['_peadig_twitter_text'])) {
                         $tweetext = $post_meta['_peadig_twitter_text'];
                       } else {
                         $tweetext = get_the_title();
                       }
                       echo '<div class="tweet"><a rel="nofollow" href="https://twitter.com/share" class="twitter-share-button" data-url="' . get_permalink() . '" data-text="' . $tweetext . $byauthor . '" data-via="' . $pd_options['twitter_username'] . '" data-related="' . $twitter_profile . '">Tweet</a>
                       </div>';
                     }
                     if (!empty($pd_options['facebook_like'])) {
                       if (!empty($pd_options['facebook_share'])) { $share = ' data-share="true"'; } else { $share = ""; }
                       echo '<div class="fb-like" data-href="' . get_permalink() . '" data-width="150" data-layout="button_count" data-action="like" data-show-faces="false"'.$share.'></div>';
                     }
                     if (empty($pd_options['facebook_like']) && !empty($pd_options['facebook_share'])) {
                       echo '<div class="fb-share-button" data-href="' . get_permalink() . '" data-type="button_count"></div>';
                     }
                     if (!empty($pd_options['google_plusone'])) {
                       echo '<div class="plusone">
                       <div class="g-plusone" data-size="medium" callback="plusone" data-href="' . get_permalink() . '"></div>
                       </div>';
                     }
                     if (!empty($pd_options['buffer_button'])) {
                       $twitter_profile = get_the_author_meta('twitter');
                       if (!empty($twitter_profile)) {
                         $byauthor = ' by @' . $twitter_profile;
                       }
                       if (!empty($post_meta['_peadig_twitter_text'])) {
                         $tweetext = $post_meta['_peadig_twitter_text'];
                       } else {
                         $tweetext = get_the_title();
                       }
                       echo '<div class="buffer">
                       <a href="http://bufferapp.com/add" class="buffer-add-button" data-text="' . $tweetext . $byauthor . '" data-count="horizontal" data-via="' . $pd_options['twitter_username'] . '">Buffer</a></div>';
                     }
                     if (!empty($pd_options['linkedin_share'])) {
                       echo '<div class="inshare"><script type="IN/Share" data-counter="right"></script></div>';
                     }
                     if (!empty($pd_options['pocket_button'])) {
                       echo '<div class="pocket"><a data-pocket-label="pocket" data-pocket-count="none" class="pocket-btn" data-lang="en"></a></div>';
                     }
                     if (!empty($pd_options['pinterest_pinit'])) {
                       echo '<div class="pinterest-btn">';
                       ?><a href="//www.pinterest.com/pin/create/button/" data-pin-do="buttonBookmark" ><img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_gray_20.png" /></a>
                       </div><?php
                     }
                     if (!empty($pd_options['stumble_upon'])) {
                       echo '<div class="stumble"><su:badge layout="2"></su:badge></div>';
                     }
                     if (!empty($pd_options['reddit_share'])) {
                       ?>
                       <div class="reddit">
                         <script type="text/javascript">
                         reddit_url = "<?php
                         the_permalink();
                         ?>";
                         reddit_title = "<?php
                         the_title();
                         ?>";
                         </script>
                         <script type="text/javascript" src="http://www.reddit.com/static/button/button1.js"></script></div>
                         <?php
                       }
                       echo '</div>';
                     }  
                   }




    /*
    * Social Buttons, Powered by Fontawesome
    *
    */

    static function header_social_links() {
      if(has_action('dh_header_social_links')) {
        do_action('dh_header_social_links');
      } else {
        $pd_options = get_option('peadig'); ?>
        <div class="social">
          <?php if(!empty($pd_options['twitter_username'])): ?>
          <a href="https://twitter.com/intent/user?screen_name=<?php echo $pd_options['twitter_username']; ?>"><i class="fa fa-twitter-square"></i></a>
        <?php endif; ?>

        <?php if(!empty($pd_options['facebook_page_url'])): ?>
        <a href="<?php echo $pd_options['facebook_page_url']; ?>" rel="external"><i class="fa fa-facebook-square"></i></a>
      <?php endif; ?>

      <?php if(!empty($pd_options['google_plus_page_id'])): ?>
      <a href="<?php echo $pd_options['google_plus_page_id']; ?>" rel="external"><i class="fa fa-google-plus-square"></i></a>
    <?php endif; ?>

    <?php if(!empty($pd_options['linkedin_url'])): ?>
    <a href="<?php echo $pd_options['linkedin_url']; ?>" rel="external"><i class="fa fa-linkedin-square"></i></a>
  <?php endif; ?>

  <?php if(!empty($pd_options['foursquare_url'])): ?>
  <a href="<?php echo $pd_options['foursquare_url']; ?>" rel="external"><i class="fa fa-foursquare"></i></a>
<?php endif; ?>


<?php if(!empty($pd_options['flickr_profile_url'])): ?>
  <a href="<?php echo $pd_options['flickr_profile_url']; ?>" rel="external"><i class="fa fa-flickr"></i></a>
<?php endif; ?>


<?php if(!empty($pd_options['instagram'])): ?>
  <a href="http://instagram.com/<?php echo $pd_options['instagram']; ?>" rel="external"><i class="fa fa-instagram"></i></a>
<?php endif; ?>

<?php if(!empty($pd_options['pinterest'])): ?>
  <a href="http://www.pinterest.com/<?php echo $pd_options['pinterest']; ?>" rel="external"><i class="fa fa-pinterest-square"></i></a>
<?php endif; ?>

<?php if(!empty($pd_options['youtube_profile_url'])): ?>
  <a href="<?php echo $pd_options['youtube_profile_url']; ?>" rel="external"><i class="fa fa-youtube-square"></i></a>
<?php endif; ?>

<?php if(!empty($pd_options['vimeo'])): ?>
  <a href="http://vimeo.com/<?php echo $pd_options['vimeo']; ?>" rel="external"><i class="fa fa-vimeo-square"></i></a>
<?php endif; ?>



<?php
if(!empty($pd_options['use_rss_icon'])) {
  if(empty($pd_options['rss_feed'])) { 
    echo '<a href="'.get_bloginfo('rss2_url').'" rel="external"><i class="fa fa-rss-square"></i></a>';
  } else { 
    echo'<a href="'.$pd_options['rss_feed'].'" rel="external"><i class="fa fa-rss-square"></i></a>';
  }
}
?>




</div>
<?php

}
}


    /*
     * Facebook SDK calls
     *
     */

    static function facebook_sdk() {
      if(has_action('dh_facebook_sdk')) {
        do_action('dh_facebook_sdk');
      } else {
        $pd_options = get_option('peadig'); ?>
        <div id="fb-root"></div>
        <script type="text/javascript">
        (function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/<?php if (!empty($pd_options['facebook_lang'])) {echo $pd_options['facebook_lang'];} else {echo 'en_US';} ?>/sdk.js#xfbml=1&appId=<?php echo $pd_options['facebook_app_id']; ?>&version=v2.3";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        </script>
        <?php

      }
    }

    /*
     * Old post notifications
     *
     */

    static function old_post_notifications() {
      if(has_action('dh_old_post_notifications')) {
        do_action('dh_old_post_notifications');
      } else {

        $pd_options = get_option('peadig');
        if (!empty($pd_options['old_post_notif'])) {
          $ageunix = get_the_time('U');
          $days_old_in_seconds = ((time() - $ageunix));
          $days_old = (($days_old_in_seconds/86400));
          if ($days_old > $pd_options['how_many_days']) : ?>
          <div class="alert alert-info">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <?php _e('This post was written','peadig'); ?> <strong><?php echo human_time_diff( get_the_time('U') ) . ' ago'; ?></strong> <?php _e('and therefore may not be as accurate as more recent posts.','peadig'); ?>
          </div>
          <?php endif;
        }

      }
    }








    /*
     * Breadcrumb Nav
     *
     */

    static function breadcrumbs() {
      if(has_action('dh_breadcrumbs')) {
        do_action('dh_breadcrumbs');
      } else {?>
      <nav id="breadcrumb">
        <?php if ( function_exists('yoast_breadcrumb') ) {
          yoast_breadcrumb('<p id="breadcrumbs" class="breadcrumb">','</p>');
        } else {
          DefaultHook::breadcrumbs_builder();
        } ?>
      </nav>
      <?php
    }
  }











    /*
     * Breadcrumb Builder
     *
     */

    static function breadcrumbs_builder() {
      if(has_action('dh_breadcrumbs_builder')) {
        do_action('dh_breadcrumbs_builder');
      } else {

            /*
            * Breadcrumb function by Dimox
            * http://dimox.net/wordpress-breadcrumbs-without-a-plugin/
            */
            /* === OPTIONS === */
            $text['home']     = 'Home'; // text for the 'Home' link
            $text['category'] = 'Archive by Category "%s"'; // text for a category page
            $text['search']   = 'Search Results for "%s" Query'; // text for a search results page
            $text['tag']      = 'Posts Tagged "%s"'; // text for a tag page
            $text['author']   = 'Articles Posted by %s'; // text for an author page
            $text['404']      = 'Error 404'; // text for the 404 page

            $showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
            $showOnHome  = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
            $delimiter   = ' &raquo; '; // delimiter between crumbs
            $before      = '<span class="current">'; // tag before the current crumb
            $after       = '</span>'; // tag after the current crumb
            /* === END OF OPTIONS === */

            global $post;
            $homeLink = home_url('/');
            $linkBefore = '<span typeof="v:Breadcrumb">';
            $linkAfter = '</span>';
            $linkAttr = ' rel="v:url" property="v:title"';
            $link = $linkBefore . '<a' . $linkAttr . ' href="%1$s">%2$s</a>' . $linkAfter;

            if (is_home() || is_front_page()) {

              if ($showOnHome == 1){
                echo '<p id="breadcrumbs" class="breadcrumb"><a href="' . $homeLink . '">' . $text['home'] . '</a></p>';
              }

            } else {

              echo '<p id="breadcrumbs" class="breadcrumb" xmlns:v="http://rdf.data-vocabulary.org/#">' . sprintf($link, $homeLink, $text['home']) . $delimiter;

              if ( is_category() ) {
                $thisCat = get_category(get_query_var('cat'), false);

                if ($thisCat->parent != 0) {
                  $cats = get_category_parents($thisCat->parent, TRUE, $delimiter);
                  $cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
                  $cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
                  echo $cats;
                }
                echo $before . sprintf($text['category'], single_cat_title('', false)) . $after;

              } elseif ( is_search() ) {

                echo $before . sprintf($text['search'], get_search_query()) . $after;

              } elseif ( is_day() ) {

                echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
                echo sprintf($link, get_month_link(get_the_time('Y'),get_the_time('m')), get_the_time('F')) . $delimiter;
                echo $before . get_the_time('d') . $after;

              } elseif ( is_month() ) {

                echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $delimiter;
                echo $before . get_the_time('F') . $after;

              } elseif ( is_year() ) {

                echo $before . get_the_time('Y') . $after;

              } elseif ( is_single() && !is_attachment() ) {

                if ( get_post_type() != 'post' ) {
                  $post_type = get_post_type_object(get_post_type());
                  $slug = $post_type->rewrite;
                  printf($link, $homeLink . '/' . $slug['slug'] . '/', $post_type->labels->singular_name);
                  if ($showCurrent == 1) echo $delimiter . $before . get_the_title() . $after;
                } else {
                  $cat = get_the_category(); $cat = $cat[0];
                  $cats = get_category_parents($cat, TRUE, $delimiter);
                  if ($showCurrent == 0) $cats = preg_replace("#^(.+)$delimiter$#", "$1", $cats);
                  $cats = str_replace('<a', $linkBefore . '<a' . $linkAttr, $cats);
                  $cats = str_replace('</a>', '</a>' . $linkAfter, $cats);
                  echo $cats;
                  if ($showCurrent == 1) echo $before . get_the_title() . $after;
                }

              } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {

                $post_type = get_post_type_object(get_post_type());
                echo $before . $post_type->labels->singular_name . $after;

              } elseif ( is_page() && !$post->post_parent ) {

                if ($showCurrent == 1) echo $before . get_the_title() . $after;

              } elseif ( is_page() && $post->post_parent ) {

                $parent_id  = $post->post_parent;
                $breadcrumbs = array();
                while ($parent_id) {
                  $page = get_page($parent_id);
                  $breadcrumbs[] = sprintf($link, get_permalink($page->ID), get_the_title($page->ID));
                  $parent_id  = $page->post_parent;
                }
                $breadcrumbs = array_reverse($breadcrumbs);
                for ($i = 0; $i < count($breadcrumbs); $i++) {
                  echo $breadcrumbs[$i];
                  if ($i != count($breadcrumbs)-1) echo $delimiter;
                }
                if ($showCurrent == 1) echo $delimiter . $before . get_the_title() . $after;

              } elseif ( is_tag() ) {

                echo $before . sprintf($text['tag'], single_tag_title('', false)) . $after;

              } elseif ( is_author() ) {

                global $author;
                $userdata = get_userdata($author);
                echo $before . sprintf($text['author'], $userdata->display_name) . $after;

              } elseif ( is_404() ) {

                echo $before . $text['404'] . $after;

              }

              if ( get_query_var('paged') ) {
                if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
                  echo __('Page', 'peadig-framework') . ' ' . get_query_var('paged');
                  if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
}

echo '</p>';

}

}

}













    /*
     * Nav for under posts
     *
     */

    static function meta_top (){
      if(has_action('dh_meta_top')) {
        do_action('dh_meta_top');
      } else {
        ?>
        <footer class="meta" itemscope itemtype="http://schema.org/Person">
          <?php
          $pd_options = get_option('peadig');

          if (!is_page()) { ?>
          <div class="bio row col-lg-12 col-md-12 col-sm-12<?php if (!empty($pd_options['meta_top_well']) && is_singular()) { echo ' well'; } ?>">

            <div class="avatar col-lg-1 col-md-1 col-sm-1">
              <?php echo get_avatar( get_the_author_meta('user_email'), '48', '' , get_the_author('') ); ?>
            </div>

            <div class="col-lg-11 col-md-11 col-sm-11">Posted by <span itemprop="name"><?php echo the_author_posts_link(); ?></span>
              <?php if (!is_page()): ?>
              on <time datetime="<?php echo get_the_date(DATE_W3C); ?>" pubdate class="updated"><?php echo get_the_date(); ?></time>

              <?php if ($pd_options['active_comment_tab']=='fbcom'): ?>
              <a href="<?php the_permalink(); ?>#comments"><fb:comments-count href="<?php echo get_permalink(); ?>"></fb:comments-count> comments</a>
            <?php  elseif ($pd_options['active_comment_tab']=='gplus'): ?>
            <a href="<?php the_permalink(); ?>#comments"><div class="g-commentcount" data-href="<?php echo get_permalink(); ?>"></div></a>
          <?php elseif ($pd_options['active_comment_tab']=='disqus'): ?>
          <script type="text/javascript">

          var disqus_shortname = 'peadig';
          var disqus_title = '<?php the_title(); ?>';
          var disqus_url = '<?php the_permalink(); ?>';

          (function () {
            var s = document.createElement('script'); s.async = true;
            s.type = 'text/javascript';
            s.src = '//' + disqus_shortname + '.disqus.com/count.js';
            (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
          }());
        </script><a href="#disqus" data-toggle="tab">#disqus_thread">
        <?php else :
        comments_popup_link('No comments', '1 comment', '% comments', 'comments-link', '');
        endif; ?>
        <?php endif; 
        if (!empty($pd_options['follow_btns_top'])) {
          ?>
          <div class="social"><?php
          $google_plus = get_the_author_meta( 'googleplus' );
          $twitter = get_the_author_meta( 'twitter' );
          $facebook = get_the_author_meta( 'facebook' );
          $linkedin = get_the_author_meta( 'linkedin' );

          if ( !empty($twitter) && (!empty($pd_options['meta_twitter_follow'])) ) {
            echo '<div class="twitter_follow"><a href="http://twitter.com/'.$twitter.'" class="twitter-follow-button">Follow @'.$twitter.'</a></div>';
          }
          if ( !empty($google_plus) && (!empty($pd_options['meta_gplus_follow'])) ) {
            echo '<div class="google_circle"><div class="g-follow" data-annotation="bubble" data-height="20" data-href="'.$google_plus.'"" data-rel="author"></div></div>';
          }
          if ( !empty($facebook) && (!empty($pd_options['meta_facebook_follow'])) ) { ?>
          <div class="fb-subscribe" data-href="<?php
          echo $facebook; ?>" data-layout="button_count" data-show-faces="false" data-colorscheme="<?php echo $pd_options['fblike_color']; ?>" data-width="120"></div>
          <?php }
          if ( !empty($linkedin) && (!empty($pd_options['meta_linkedin_follow'])) ) { ?>
          <div class="linkedinbox">
            <script type="IN/MemberProfile" data-id="<?php echo $linkedin; ?>" data-format="hover" data-related="false"></script>
          </div>
          <?php } ?>
        </div>
        <?php } ?>
      </div>
    </div>
    <?php } ?>
  </footer>
  <?php

}
}



    /*
     * Meta bottom, shows author box
     *
     */

    static function meta_bottom(){
      if(has_action('dh_meta_bottom')) {
        do_action('dh_meta_bottom');
      } else {
        ?>
        <footer class="meta" itemscope itemtype="http://schema.org/Person">
          <?php
          $pd_options = get_option('peadig');
          if (is_singular() && !is_page()) {
            ?>
            <div class="bio row well col-lg-12 col-md-12 col-sm-12">

              <div class="avatar col-lg-3 col-md-3 col-sm-3"><?php echo get_avatar( get_the_author_meta('user_email'), '120', '' , get_the_author('') ); ?></div>

              <div class="col-lg-9 col-md-9 col-sm-9"><h3 class="text-left">Posted by <span itemprop="name"><?php echo the_author_posts_link(); ?></span></h3>
               <?php
               if (!empty($pd_options['follow_btns_bottom'])) {
                ?>
                <div class="social"><?php
                $google_plus = get_the_author_meta( 'googleplus' );
                $twitter = get_the_author_meta( 'twitter' );
                $facebook = get_the_author_meta( 'facebook' );
                $linkedin = get_the_author_meta( 'linkedin' );
                ?>
                <?php if ( !empty($twitter) && (!empty($pd_options['meta_twitter_follow'])) ): ?>
                <div class="twitter_follow"><a href="http://twitter.com/<?php echo $twitter; ?>" class="twitter-follow-button">Follow @<?php echo $twitter; ?>'</a></div>
              <?php endif; ?>

              <?php if ( !empty($google_plus) && (!empty($pd_options['meta_gplus_follow'])) ): ?>
              <div class="google_circle"><div class="g-follow" data-annotation="bubble" data-height="20" data-href="<?php echo $google_plus; ?>" data-rel="author"></div></div>
            <?php endif; ?>

            <?php if ( !empty($facebook) && (!empty($pd_options['meta_facebook_follow'])) ): ?>
            <div class="fb-subscribe" data-href="<?php
            echo $facebook; ?>" data-layout="button_count" data-show-faces="false" data-colorscheme="<?php echo $pd_options['fblike_color']; ?>" data-width="120"></div>
          <?php endif; ?>

          <?php if ( !empty($linkedin) && (!empty($pd_options['meta_linkedin_follow'])) ): ?>
          <div class="linkedinbox"><script type="IN/MemberProfile" data-id="<?php echo $linkedin; ?>" data-format="hover" data-related="false"></script></div>
        <?php endif; ?>
      </div>
      <?php } ?>
      <p><?php the_author_meta('description'); ?></p>
    </div>
    </div><?php
  }
  ?>
</footer>
<?php
}
}











    /*
     * Numerical nav
     *
     */

    static function numeric_posts_nav() {
      if(has_action('dh_numeric_posts_nav')) {
        do_action('dh_numeric_posts_nav');
      } else {
        if( is_singular() )
          return;
        global $wp_query;

        if( $wp_query->max_num_pages <= 1 )
          return;
        $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
        $max   = intval( $wp_query->max_num_pages );

        if ( $paged >= 1 ){
          $links[] = $paged;
        }

            // Add the pages around the current page to the array
        if ( $paged >= 3 ) {
          $links[] = $paged - 1;
          $links[] = $paged - 2;
        }
        if ( ( $paged + 2 ) <= $max ) {
          $links[] = $paged + 2;
          $links[] = $paged + 1;
        }
        echo '<div class="navigation row col-lg-12 col-md-12 col-sm-12 col-xs-12"><ul class="pagination pagination-small">' . "\n";

        if ( get_previous_posts_link() ){
          printf( '<li>%s</li>' . "\n", get_previous_posts_link() );
        }
        if ( ! in_array( 1, $links ) ) {
          $class = 1 == $paged ? ' class="active"' : '';

          printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

          if ( ! in_array( 2, $links ) ){
            echo '<li class="disabled"><a href="#">...</a></li>';

          }
        }
        sort( $links );

        foreach ( (array) $links as $link ) {
          $class = $paged == $link ? ' class="active"' : '';
          printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
        }

        if ( ! in_array( $max, $links ) ) {

          if ( ! in_array( $max - 1, $links ) ){
            echo '<li class="disabled"><span>...</span></li>' . "\n";
          }

          $class = $paged == $max ? ' class="active"' : '';
          printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
        }

        if ( get_next_posts_link() ){
          printf( '<li>%s</li>' . "\n", get_next_posts_link() );
        }
        echo '</ul></div>' . "\n";
      }
    }











    /*
     * Top menu navigation
     *
     */

    static function header_navigation() {
      if(has_action('dh_header_navigation')) {
        do_action('dh_header_navigation');
      } else {
        $pd_options = get_option('peadig'); ?>

        <?php CustomHook::header_above_nav() ?>
        <nav id="access" role="navigation">



          <div class="navbar navbar-default<?php
          if ($pd_options['fixed_nav'] == '1') {
            echo " navbar-fixed-".$pd_options['fixed_nav_pos'];
          }
          if ($pd_options['inverse_nav'] == '1') {
            echo " navbar-inverse";
          }
          ?>">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand visible-xs" href="#"><?php echo $pd_options['nav_mobile_text']; ?></a>
          </div>  <!-- header -->

          <div class="navbar-collapse collapse">
            <?php
            $justified = "";
            if ($pd_options['justified_nav'] == '1') {$justified = ' nav-justified';}
            $navcss = 'nav navbar-nav '.$pd_options['nav_css'].$justified;
            wp_nav_menu( array(  'menu_class' => $navcss, 'theme_location' => 'primary', 'container' => 'false', 'walker' => new Nfr_Menu_Walker() ) );

            if (!empty($pd_options['nav_login'])) {
              echo do_shortcode('[login-form class="navbar-form navbar-right" inline=yes forgotpass=no rememberbtn=no]');
            }
            ?>
          </div>
        </div>

      </nav>
      <?php CustomHook::header_below_nav() ?>


      <?php
    }
  }










    /*
     * 404 Page content
     *
     */

    static function contents_404() {
      if(has_action('dh_contents_404')) {
        do_action('dh_contents_404');
      } else {
        $pd_options = get_option('peadig');
        $home_url = home_url();
        ?>

        <div class="container">
          <div class="row" role="main">
            <div id="primary" class="col-lg-12 trans">

              <?php CustomHook::e404_before_h1() ?>
              <h1><?php _e('Error 404 - Page Not Found','peadig'); ?></h1>
              <?php CustomHook::e404_after_h1() ?>

              <p><?php _e('Sorry, but the page you requested has not been found.','peadig'); ?></p>
              <p><?php _e('You can go back to our <a href="','peadig'); echo $home_url; _e('">homepage</a> or search for something:','peadig'); ?></p>

              <?php CustomHook::e404_before_search() ?>
              <?php get_search_form(); ?>
              <?php CustomHook::e404_after_search() ?>

            </div>
          </div>
        </div>

        <?php
      }
    }








    /*
     * Archive content
     *
     */

    static function contents_archive() {
      if(has_action('dh_contents_archive')) {
        do_action('dh_contents_archive');
      } else {
        global $posts;
        $pd_options = get_option('peadig'); ?>

        <div class="container">
          <div class="row" role="main">
            <?php if($pd_options['taxonomy_sidebar']=="2"){
              CustomHook::before_sidebar();
              get_sidebar();
              CustomHook::after_sidebar();

            }
            if ($pd_options['taxonomy_sidebar']=="4") {
              if ($pd_options['main_span']=='11' || $pd_options['main_span']=='9' || $pd_options['main_span']=='7') {
                $pd_options['main_span']=$pd_options['main_span']-1;
              }

              $sidebar_span = (12-$pd_options['main_span']) / 2;
              $sidebar_span = round($sidebar_span, 0, PHP_ROUND_HALF_DOWN);
              ?>
              <div id="secondary" class="widget-area <?php echo 'col-lg-'.$sidebar_span.' col-md-'.$sidebar_span.' col-sm-'.$sidebar_span; ?>" role="complementary">
                <?php CustomHook::leftsidebarright_before_sidebar_1() ?>
                <?php dynamic_sidebar('sidebar-1'); ?>
                <?php CustomHook::leftsidebarright_after_sidebar_1() ?>
              </div>
              <?php }
              if($pd_options['taxonomy_sidebar']!="1"): ?>
              <div id="primary" class="col-lg-<?php echo $pd_options['main_span']; ?> col-md-<?php echo $pd_options['main_span']; ?> col-sm-<?php echo $pd_options['main_span']; ?>" role="main">
              <?php  else : ?>
              <div id="primary" class="col-lg-12 col-md-12 col-sm-12" role="main">
              <?php endif; ?>

              <?php CustomHook::archive_before_loop() ?>
              <?php if (have_posts()) : ?>

              <?php $post = $posts[0]; ?>

              <?php CustomHook::archive_before_h1() ?>
              <?php if (is_category()) : ?>
              

              <h1><?php _e('Archive for the','peadig'); ?> &#8216;<?php single_cat_title(); ?>&#8217; <?php _e('Category','peadig'); ?></h1>

            <?php elseif( is_tag() ) : ?>
            <h1><?php _e('Posts Tagged','peadig'); ?> &#8216;<?php single_tag_title(); ?>&#8217;</h1>

          <?php elseif (is_day()) : ?>
          <h1><?php _e('Archive for','peadig'); ?> <?php the_time('F jS, Y'); ?></h1>

        <?php elseif (is_month()) : ?>
        <h1><?php _e('Archive for','peadig'); ?> <?php the_time('F, Y'); ?></h1>

      <?php elseif (is_year()) : ?>
      <h1 class="pagetitle"><?php _e('Archive for','peadig'); ?> <?php the_time('Y'); ?></h1>

    <?php elseif (is_author()) : ?>
    <h1 class="pagetitle"><?php _e('Author Archive','peadig'); ?></h1>

  <?php elseif(!empty($_GET['paged']) && !empty($_GET['paged'])) : ?>
  <h1 class="pagetitle"><?php _e('Blog Archives','peadig'); ?></h1>

<?php endif; ?>

<?php CustomHook::archive_after_h1() ?>

<?php DefaultHook::numeric_posts_nav(); ?>

<?php while (have_posts()) : the_post(); ?>

  <?php CustomHook::archive_before_article() ?>
  <article <?php post_class() ?>>
    <?php CustomHook::archive_article_top() ?>

    <h2 id="post-<?php the_ID(); ?>"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
    <?php if (!empty($pd_options['post_meta_archives'])) {
      DefaultHook::meta_top();
    } ?>
    <div class="entry">
      <?php if (!empty($pd_options['sharing_archives'])) {
        DefaultHook::social_buttons();
      } ?>

      <?php if ( has_post_thumbnail()) : ?>
      <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
        <?php the_post_thumbnail('post-thumbnail', array('class' => $pd_options['featured_image_class'])); ?>
      </a>
    <?php endif; ?>

    <?php CustomHook::archive_before_excerpt() ?>
    <?php the_excerpt(); ?>
    <?php CustomHook::archive_after_excerpt() ?>

  </div>
  <?php CustomHook::archive_article_bottom() ?>
</article>
<?php CustomHook::archive_after_article() ?>
<hr>
<?php endwhile; ?>

<?php CustomHook::archive_before_numeric_posts_nav() ?>
<?php DefaultHook::numeric_posts_nav();  ?>
<?php CustomHook::archive_after_numeric_posts_nav() ?>

<?php else : ?>

  <h2>Nothing found</h2>
  <?php CustomHook::archive_after_nothing_found() ?>

<?php endif; ?>
<?php CustomHook::archive_after_loop() ?>

</div>

<?php if($pd_options['taxonomy_sidebar']=="3"){
  CustomHook::before_sidebar();
  get_sidebar();
  CustomHook::after_sidebar();
}

if ($pd_options['taxonomy_sidebar']=="4"): ?>
<div id="tertiary" class="widget-area <?php echo 'col-lg-'.$sidebar_span.' col-md-'.$sidebar_span.' col-sm-'.$sidebar_span; ?>" role="complementary">
  <?php CustomHook::leftsidebarright_before_sidebar_2() ?>
  <?php dynamic_sidebar('sidebar-2'); ?>
  <?php CustomHook::leftsidebarright_after_sidebar_2() ?>
</div>
<?php endif; ?>

</div>
</div>

<?php
}
}








    /*
     * Author Contents
     *
     */

    static function contents_author() {
      if(has_action('dh_contents_author')) {
        do_action('dh_contents_author');
      } else {
        $author = get_the_author();
        $pd_options = get_option('peadig'); ?>
        <div class="container">
          <div class="row">
            <div id="primary" class="col-lg-<?php echo $pd_options['main_span']; ?>">

              <?php CustomHook::author_before_bio() ?>
              <div class="bio row well col-lg-12">

                <div class="avatar col-lg-3 col-md-3 col-sm-3"><?php
                if(function_exists('get_avatar')) {
                  echo get_avatar( get_the_author_meta('ID') , 120, "#646464" );
                }
                                //echo get_avatar( get_the_author_meta('user_email'), '120', '' , get_the_author('') ); ?>
                              </div>
                              <div class="col-lg-9 col-md-9 col-sm-9" itemscope itemtype="http://schema.org/Person">

                                <?php CustomHook::author_before_display_name() ?>
                                <h1 class="text-left" itemprop="name"><?php echo $author ?></h1>
                                <?php CustomHook::author_before_after_name() ?>

                                <div class="social">
                                  <?php CustomHook::author_social_top() ?>
                                  <div class="social"><?php
                                  $google_plus     = get_the_author_meta( 'googleplus');
                                  $twitter        = get_the_author_meta( 'twitter' );
                                  $facebook       = get_the_author_meta( 'facebook' );
                                  $linkedin       = get_the_author_meta( 'linkedin' );

                                  if ( !empty($twitter) && (!empty($pd_options['meta_twitter_follow'])) ) : ?>
                                  <div class="twitter_follow">
                                   <a href="http://twitter.com/<?php echo $twitter; ?>" class="twitter-follow-button">Follow @<?php echo $twitter; ?></a>
                                 </div>
                                 <?php endif;
                                 if ( !empty($google_plus) && (!empty($pd_options['meta_gplus_follow'])) ) : ?>
                                 <div class="google_circle">
                                  <div class="g-follow" data-annotation="bubble" data-height="20" data-href="<?php echo $google_plus ?>" data-rel="author"></div>
                                </div>
                                <?php endif;
                                if ( !empty($facebook) && (!empty($pd_options['meta_facebook_follow'])) ) : ?>
                                <div class="fb-subscribe" data-href="<?php echo $facebook; ?>" data-layout="button_count" data-show-faces="false" data-colorscheme="<?php echo $pd_options['fblike_color']; ?>" data-width="120"></div>
                                <?php endif;

                                if ( !empty($linkedin) && (!empty($pd_options['meta_linkedin_follow'])) ) : ?>
                                <div class="linkedinbox">
                                  <script type="IN/MemberProfile" data-id="<?php echo $linkedin; ?>" data-format="hover" data-related="false"></script>
                                </div>
                              <?php endif; ?>
                            </div>
                          </div>

                          <?php CustomHook::author_social_bottom() ?>

                          <?php CustomHook::author_before_description() ?>
                          <p><?php the_author_meta('description'); ?></p>
                          <?php CustomHook::author_after_description() ?>

                        </div>

                      </div>

                      <?php CustomHook::author_after_bio() ?>

                      <?php CustomHook::author_before_posts_by() ?>

                      <h2>Posts by <?php echo $author ?>:</h2>
                      <?php CustomHook::author_after_posts_by() ?>
                      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                      <article <?php post_class() ?>>
                        <h2 id="post-<?php the_ID(); ?>"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
                        <?php DefaultHook::meta_top(); ?>
                        <div class="entry">
                          <?php if (!empty($pd_options['sharing_archives'])) {
                            DefaultHook::social_buttons();
                          } ?>
                          <?php if ( has_post_thumbnail()) : ?>
                          <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
                            <?php the_post_thumbnail('post-thumbnail', array('class' => $pd_options['featured_image_class'])); ?>
                          </a>
                        <?php endif; ?>
                        <?php the_excerpt(); ?>
                      </div>
                    </article>

                    <hr>

                  <?php endwhile; else: ?>

                  <?php CustomHook::author_before_no_posts() ?>
                  <p>No posts by <?php echo $author ?>... yet</p>
                  <?php CustomHook::author_after_no_posts() ?>

                <?php endif; ?>

              </div>

              <?php DefaultHook::sidebar(); ?>
            </div>
          </div>
          <?php
        }
      }








    /*
     * Homepage Slider
     *
     */

    static function homepage_slider() {
      if(has_action('dh_homepage_slider')) {
        do_action('dh_homepage_slider');
      } else {
        $pd_options = get_option('peadig');

        $slider_pages = "";

        CustomHook::front_page_before_home_slider();

        if(!empty($pd_options['use_homepage_slider'])) { ?>

        <?php if(empty( $pd_options['full_width_slider'] ) || $pd_options['full_width_slider']==0): ?>
        <div class="container">
          <div class="row">
          <?php endif; ?>


          <?php if ((!empty($pd_options['use_nextgen_gallery']) && !empty($pd_options['nextgen_id'])) || ($pd_options['slider_type']=='nextgen' && !empty($pd_options['nextgen_id']))) {
            echo do_shortcode('[nggallery id='.$pd_options['nextgen_id'].' template=peadig]');
          } else { ?>

          <div id="homeCarousel" class="carousel slide<?php if(!empty($pd_options['additional_slider_css'])) {
            echo ' '.$pd_options['additional_slider_css'];
          } 
          if (!empty($pd_options['animation'])) {
                if (!empty($pd_options['animation_interval'])) {
                    echo ' data-interval="'.$pd_options['animation_interval'].'"';
                } else {
                    echo ' data-interval="7000"';
                }
          }
          if (!empty($pd_options['slider_fixed_height'])) {
            echo ' style="max-height:'.$pd_options['slider_height'].'px"';
          }
          ?>  
          ">

          <?php if(!empty($pd_options['slider_navigation'])): ?>

          <ol class="carousel-indicators">
            <?php
            if ($pd_options['slider_type']=='custom') {

                            //print_r($pd_options['slides']);


            if(!empty ($pd_options['slides']) && $pd_options['slides']!="") {

                $x = 0;
                foreach($pd_options['slides'] as $item): ?>
                <li data-target="#homeCarousel" data-slide-to="<?php echo $x; ?>"<?php if ($x=="0") { echo " class=\"active\"";} ?>></li>
                <?php $x++;
                endforeach;

                ?>
              </ol>

              <div class="carousel-inner">
                <?php
                $active = 0;
                foreach( $pd_options['slides'] as $item ) :
                  $active++;
                ?>

                <div class="item<?php if ($active=="1") { echo " active"; } ?>">
                  <img src="<?php
                  if ($item['image']!="") {
                    echo $item['image'];
                  } else {
                    echo get_template_directory_uri()."/img/no-image-avail.png";
                  }
                  ?>" alt="<?php echo $item['title']; ?>" class="img-responsive">

                  <div class="carousel-caption">
                    <h3><?php if (!empty($item['url'])) {echo '<a href="'.$item['url'].'">'; } ?><?php echo $item['title']; ?><?php if (!empty($item['url'])) {echo '</a>'; } ?></h3>
                    <p><?php echo $item['description']; ?></p>
                  </div>
                  <?php if (!empty($item['url'])) { echo '<a href="'.$item['url'].'" class="whole"></a>'; } ?>
                </div>
                <?php endforeach;
              }
            } else {
              if ( !empty( $pd_options['posts_pages'] ) && ($pd_options['posts_pages'] == 1 || $pd_options['slider_type']=='posts' ) ) {

                if( !empty( $pd_options['post_categories'] ) ) {
                  $sliderpostcats = implode(",", $pd_options['post_categories']);
                }

                if ( empty($pd_options['number_of_slider_items']) ) {
                  $number_of_slider_items = 3;
                } else {
                  $number_of_slider_items = $pd_options['number_of_slider_items'];
                }


                $args = array( 'posts_per_page' => $number_of_slider_items, 'cat' =>  $sliderpostcats);
                if ( !empty($sliderpostcats) ) {
                  $args = array( 'posts_per_page' => $number_of_slider_items, 'cat' =>  $sliderpostcats);
                } else {
                  $args = array( 'posts_per_page' => $number_of_slider_items);
                }
                //$args = array( 'posts_per_page' => $number_of_slider_items, 'cat' =>  $sliderpostcats);
                $items = get_posts( $args );

              } elseif ($pd_options['slider_type']=='pages' || empty( $pd_options['posts_pages'] )) {

                if( !empty( $pd_options['select_pages'] )) {
                  $slider_pages = implode(",", $pd_options['select_pages']);
                }

                $args = array( 'post_status' => 'publish', 'include' =>  $slider_pages);
                $items = get_pages($args);

              }

                        //print_r($items);

              $x = 0;
              foreach($items as $item): ?>
              <li data-target="#homeCarousel" data-slide-to="<?php echo $x; ?>"<?php if ($x=="0") { echo " class=\"active\"";} ?>></li>
              <?php $x++;
              endforeach;

              ?>
            </ol>

            <div class="carousel-inner">
              <?php
              $active = 0;
              foreach( $items as $post ) : setup_postdata($post);
              $active++;
              ?>

              <div class="item<?php if ($active=="1") { echo " active"; } ?>">
                <img src="<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
                if ($image[0]!="") {
                  echo $image[0];
                } else {
                  echo get_template_directory_uri()."/img/no-image-avail.png";
                }
                ?>" alt="<?php echo get_the_title( $post->ID ); ?>" class="img-responsive">

                <div class="carousel-caption">
                  <h3><a href="<?php echo get_permalink( $post->ID );?>"><?php echo get_the_title( $post->ID ); ?></a></h3>
                  <p><?php
                  $excerpt = strip_tags(get_the_excerpt());
                  echo wp_trim_words( $excerpt, 25 ); ?></p>
                  <a href="<?php echo get_permalink( $post->ID );?>" class="whole"></a>
                </div>
              </div>
              <?php endforeach;

              wp_reset_postdata();
            } ?>
          </div>

          <?php if(!empty($pd_options['use_prev_next_buttons'])) : ?>
          <a class='left carousel-control' href='#homeCarousel' role='button' data-slide='prev'>
        <span class='glyphicon glyphicon-chevron-left' aria-hidden='true'></span>
        <span class='sr-only'>Previous</span>
      </a>
      <a class='right carousel-control' href='#homeCarousel' role='button' data-slide='next'>
        <span class='glyphicon glyphicon-chevron-right' aria-hidden='true'></span>
        <span class='sr-only'>Next</span>
      </a>
        <?php endif; ?>

      </div>

      <?php
      endif; ?>



      <?php if( empty( $pd_options['full_width_slider'] ) ) : ?>
    </div>
  </div>
<?php endif; ?>

<?php
}
}
wp_reset_query();

CustomHook::front_page_after_home_slider();

}
}







    /*
     * Homepage Jumbotron
     *
     */

    static function homepage_jumbotron() {
      if(has_action('dh_homepage_jumbotron')) {
        do_action('dh_homepage_jumbotron');
      } else {
        $pd_options = get_option('peadig');

        CustomHook::front_page_before_home_jumbotron();

        if(!empty($pd_options['jumbotron'])) { ?>

        <?php if(empty($pd_options['full_width_jumbotron'])): ?>
        <div id="jumbotron" class="container">
          <div class="row" <?php if(!is_home()) { echo 'role="main"';} ?>>
          <?php endif; ?>
          <div class="jumbotron">
            <h2><?php if(!empty($pd_options['jumbotron_title'])) {
              echo $pd_options['jumbotron_title'];
            } else {
              echo 'Welcome to '.get_bloginfo('name').'!';
            }  ?></h2>
            <p><?php if(!empty($pd_options['jumbotron_tagline'])) {
              echo do_shortcode($pd_options['jumbotron_tagline']);
            } else {
              echo bloginfo('description');
            }  ?></p>
            <?php if(!empty($pd_options['use_jumbotron_button'])): ?>
            <p><a href="<?php echo get_permalink($pd_options['jumbotron_button_link']); ?>" class="btn <?php
            if($pd_options['jumbotron_button_css']){
              echo $pd_options['jumbotron_button_css']; }
              else {
                echo "btn-default btn-large";
              } ?>"><?php
              if(!empty($pd_options['jumbotron_button_text_override'])) {
                echo $pd_options['jumbotron_button_text_override'];
              } else {
                echo get_the_title($pd_options['jumbotron_button_link']).' &raquo;';
              } ?></a></p>
            <?php endif; ?>
          </div>
          <?php if(empty($pd_options['full_width_hero'])): ?>
        </div>
      </div>
      <?php endif;
    }

    CustomHook::front_page_after_home_jumbotron();

  }
}







    /*
     * Homapage Services
     *
     */

    static function homepage_services() {
      if(has_action('dh_homepage_services')) {
        do_action('dh_homepage_services');
      } else {
        $pd_options = get_option('peadig');

        CustomHook::front_page_before_home_services();

        if(!empty($pd_options['services_unit'])) {
          $image_size = $pd_options['services_unit_imgsize'];
          $image_class = $pd_options['services_unit_imgclass'];

          ?>
          <div id="services" class="container">
            <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-4">

                <?php

                $services_page_1 = "";
                $services_page_2 = "";                          
                $services_page_3 = "";

                if ( !empty( $pd_options['services_page_1'] ) ) {

                  $services_page_1 = $pd_options['services_page_1'];

                }

                if ( !empty( $pd_options['services_page_2'] ) ) {

                  $services_page_2 = $pd_options['services_page_2'];

                }

                if ( !empty( $pd_options['services_page_3'] ) ) {

                  $services_page_3 = $pd_options['services_page_3'];

                }                          

                ?>


                <a href="<?php echo get_permalink($services_page_1); ?>"><?php
                if ( has_post_thumbnail($services_page_1)) {
                  echo get_the_post_thumbnail($services_page_1, array($image_size,$image_size), array('class' => "$image_class", 'alt'	=> get_the_title($services_page_1))  );
                } else {
                  echo '<img class="'.$pd_options['services_unit_imgclass'].'" src="'.get_template_directory_uri().'/img/250x250.jpg" height="'.$pd_options['services_unit_imgsize'].'" width="'.$pd_options['services_unit_imgsize'].'">';
                }
                ?>
                <h2><?php echo get_the_title($services_page_1); ?></h2></a>
                <p><?php echo do_shortcode($pd_options['services_desc_1']); ?></p>
                <p><a class="btn btn-default btn-primary" href="<?php echo get_permalink($services_page_1); ?>"><?php echo $pd_options['services_button_1']; ?></a></p>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-4">
                <a href="<?php echo get_permalink($services_page_2); ?>"><?php
                if ( has_post_thumbnail($services_page_2)) {
                  echo get_the_post_thumbnail($services_page_2,array($image_size,$image_size), array('class' => "$image_class", 'alt'	=> get_the_title($services_page_2))  );
                } else {
                  echo '<img class="'.$pd_options['services_unit_imgclass'].'" src="'.get_template_directory_uri().'/img/250x250.jpg" height="'.$pd_options['services_unit_imgsize'].'" width="'.$pd_options['services_unit_imgsize'].'">';
                }
                ?>
                <h2><?php echo get_the_title($services_page_2); ?></h2></a>
                <p><?php echo do_shortcode($pd_options['services_desc_2']); ?></p>
                <p><a class="btn btn-default btn-primary" href="<?php echo get_permalink($services_page_2); ?>"><?php echo $pd_options['services_button_2']; ?></a></p>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-4">
                <a href="<?php echo get_permalink($services_page_3); ?>"><?php
                if ( has_post_thumbnail($services_page_3)) {
                  echo get_the_post_thumbnail($services_page_3, array($image_size,$image_size), array('class' => "$image_class", 'alt'	=> get_the_title($services_page_3))  );
                } else {
                  echo '<img class="'.$pd_options['services_unit_imgclass'].'" src="'.get_template_directory_uri().'/img/250x250.jpg" height="'.$pd_options['services_unit_imgsize'].'" width="'.$pd_options['services_unit_imgsize'].'">';
                }
                ?>
                <h2><?php echo get_the_title($services_page_3); ?></h2></a>
                <p><?php echo do_shortcode($pd_options['services_desc_3']); ?></p>
                <p><a class="btn btn-default btn-primary" href="<?php echo get_permalink($services_page_3); ?>"><?php echo $pd_options['services_button_3']; ?></a></p>
              </div>
            </div>
          </div>
          <?php }

          CustomHook::front_page_after_home_services();

        }
      }







    /*
     * Homepage Quote
     *
     */

    static function homepage_quote() {
      if(has_action('dh_homepage_quote')) {
        do_action('dh_homepage_quote');
      } else {
        $pd_options = get_option('peadig');

        CustomHook::front_page_before_home_quote();

        if(!empty($pd_options['quote'])) { ?>
        <div id="quote" class="container">
          <div class="row" role="main">
            <div class="page-header">
              <h2><?php
              if(!empty($pd_options['quote_line_1'])) {
                echo $pd_options['quote_line_1'];
              } else {
                echo 'This is the first line of a quote';
              }  ?><small><?php
              if(!empty($pd_options['quote_line_2'])) {
                echo $pd_options['quote_line_2'];
              } else {
                echo '...and this is the second!';
              }  ?></small></h2>
            </div>
          </div>
        </div>
        <?php
      }

      CustomHook::front_page_after_home_quote();

    }
  }







    /*
     * Homepage Contents
     *
     */

    static function contents_homepage() {
      if(has_action('dh_contents_homepage')) {
        do_action('dh_contents_homepage');
      } else {
        $pd_options = get_option('peadig');

        CustomHook::front_page_before_home_content();

        if(!empty($pd_options['content_on_homepage'])) { ?>
        <div id="main" class="container" role="main">
          <div class="row">
            <?php if($pd_options['homepage_sidebar']=="2"){
              DefaultHook::sidebar();
            }

            if ($pd_options['homepage_sidebar']=="4") {
              if ($pd_options['main_span']=='11' || $pd_options['main_span']=='9' || $pd_options['main_span']=='7') {
                $pd_options['main_span']=$pd_options['main_span']-1;
              }

              $sidebar_span = (12-$pd_options['main_span']) / 2;
              $sidebar_span = round($sidebar_span, 0, PHP_ROUND_HALF_DOWN);
              ?>

              <div id="secondary" class="widget-area <?php echo 'col-lg-'.$sidebar_span.' col-md-'.$sidebar_span.' col-sm-'.$sidebar_span; ?>" role="complementary">
                <?php CustomHook::leftsidebarright_before_sidebar_1() ?>
                <?php dynamic_sidebar('sidebar-1'); ?>
                <?php CustomHook::leftsidebarright_after_sidebar_1() ?>
              </div>
              <?php
            }


            if($pd_options['homepage_sidebar']!="1"): ?>
            <div id="primary" class="col-lg-<?php echo $pd_options['main_span']; ?> col-md-<?php echo $pd_options['main_span']; ?> col-sm-<?php echo $pd_options['main_span']; ?>" role="main">
            <?php else: ?>
            <div id="primary" class="col-lg-12 col-md-12 col-sm-12" role="main">
            <?php endif; ?>

            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <article <?php post_class() ?> id="post-<?php the_ID(); ?>">

              <?php if(!is_page()): ?>
              <h2 class="entry-title"><a href="<?php echo get_permalink()?>"><?php echo get_the_title()?></a></h2>
            <?php else: ?>
            <?php if (empty($pd_options['noh1'])) { ?><h1><?php echo get_the_title()?></h1><?php } ?>
          <?php endif; ?>

          <?php DefaultHook::meta_top(); ?>

          <div class="entry">
            <?php if ( has_post_thumbnail()) : ?>
            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
              <?php the_post_thumbnail('post-thumbnail', array('class' => $pd_options['featured_image_class'])); ?>
            </a>
            <?php endif;

            if(!is_page()):
              the_excerpt();
            else:
              the_content();
            endif; ?>
          </div>

          <footer class="postmetadata">
            <?php CustomHook::front_page_footer_top(); ?>

            <?php if(!is_page()): ?>
            <?php the_tags('Tags: ', ', ', '<br>'); ?>
            Categories: <?php the_category(', ');


            if ( comments_open() ) {
              ?> | <?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;');
            } ?>

            <?php CustomHook::front_page_footer_bottom() ?>

          <?php endif; ?>

        </footer>
      </article>

      <?php if(!is_page()): ?>
      <hr>
    <?php endif; ?>

  <?php endwhile; ?>
  <?php DefaultHook::numeric_posts_nav(); ?>
<?php else : ?>
  <h1><?php _e('Not Found','peadig');?></h1>
<?php endif; ?>
</div>

<?php if($pd_options['homepage_sidebar']=="3"):
CustomHook::before_sidebar();
get_sidebar();
CustomHook::after_sidebar();
endif;

if ($pd_options['homepage_sidebar']=="4"): ?>
<div id="tertiary" class="widget-area <?php echo 'col-lg-'.$sidebar_span.' col-md-'.$sidebar_span.' col-sm-'.$sidebar_span; ?>" role="complementary">
  <?php CustomHook::leftsidebarright_before_sidebar_2() ?>
  <?php dynamic_sidebar('sidebar-2'); ?>
  <?php CustomHook::leftsidebarright_after_sidebar_2() ?>
</div>
<?php endif; ?>

</div>
</div>
<?php //DefaultHook::sidebar();

}

CustomHook::front_page_after_home_content();

}
}







    /*
     * Home Contents
     *
     */

    static function contents_home() {
      if(has_action('dh_contents_home')) {
        do_action('dh_contents_home');
      } else {
        $pd_options = get_option('peadig');?>

        <div class="container">

          <div class="row" role="main">
            <?php if ($pd_options['taxonomy_sidebar'] == "2") {
              DefaultHook::sidebar();
            }

            if ($pd_options['taxonomy_sidebar'] == "4") {

              if ($pd_options['main_span'] == '11' || $pd_options['main_span'] == '9' || $pd_options['main_span'] == '7') {
                $pd_options['main_span'] = $pd_options['main_span'] - 1;
              }
              $sidebar_span = (12 - $pd_options['main_span']) / 2;
              $sidebar_span = round($sidebar_span, 0, PHP_ROUND_HALF_DOWN); ?>
              <div class="row">
                <div id="secondary" class="widget-area <?php echo 'col-lg-' . $sidebar_span . ' col-md-' . $sidebar_span . ' col-sm-' . $sidebar_span; ?>" role="complementary">
                  <?php CustomHook::leftsidebarright_before_sidebar_1() ?>
                  <?php dynamic_sidebar('sidebar-1'); ?>
                  <?php CustomHook::leftsidebarright_after_sidebar_1() ?>
                </div>
                <?php }
                if ($pd_options['taxonomy_sidebar'] != "1"){ ?>
                <div id="primary" class="col-lg-<?php echo $pd_options['main_span']; ?> col-md-<?php echo $pd_options['main_span']; ?> col-sm-<?php echo $pd_options['main_span']; ?>" role="main">
                  <?php } else { ?>
                  <div id="primary" class="col-lg-12 col-md-12 col-sm-12" role="main">
                    <?php } ?>

                    <?php CustomHook::archive_before_loop() ?>
                    <?php CustomHook::archive_before_h1() ?>
                    <?php if (empty($pd_options['noh1'])) { ?><h1><?php single_post_title(); ?></h1><?php } ?>
                    <?php CustomHook::archive_after_h1() ?>

                    <?php if (have_posts()) : ?>
                    <?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
                    <?php DefaultHook::numeric_posts_nav(); ?>
                    <?php while (have_posts()) : the_post(); ?>
                    <?php CustomHook::archive_before_article() ?>
                    <article <?php post_class() ?>>

                      <?php CustomHook::archive_article_top() ?>
                      <h2 id="post-<?php the_ID(); ?>"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a> </h2>

                      <?php if (!empty($pd_options['post_meta_top'])) {
                        DefaultHook::meta_top();
                      } ?>

                      <div class="entry">

                        <?php if (!empty($pd_options['sharing_archives'])) {
                          DefaultHook::social_buttons();
                        } ?>

                        <?php if (has_post_thumbnail()) : ?><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail('post-thumbnail', array('class' => $pd_options['featured_image_class'])); ?></a>
                      <?php endif; ?>

                      <?php CustomHook::archive_before_excerpt() ?>
                      <?php the_excerpt(); ?>
                      <?php CustomHook::archive_after_excerpt() ?>

                    </div>
                    <?php CustomHook::archive_article_bottom() ?>
                  </article>
                  <?php CustomHook::archive_after_article() ?>
                  <hr>
                <?php endwhile; ?>
                <?php CustomHook::archive_before_numeric_posts_nav() ?>
                <?php DefaultHook::numeric_posts_nav(); ?>
                <?php CustomHook::archive_after_numeric_posts_nav() ?>
              <?php else : ?>
              <h2>Nothing found</h2>
              <?php CustomHook::archive_after_nothing_found() ?>
            <?php endif; ?>
            <?php CustomHook::archive_after_loop() ?>
          </div>


          <?php if ($pd_options['taxonomy_sidebar'] == "3") {
            DefaultHook::sidebar();
          }



          if ($pd_options['taxonomy_sidebar'] == "4") { ?>
          <div id="tertiary"
          class="widget-area <?php echo 'col-lg-' . $sidebar_span . ' col-md-' . $sidebar_span . ' col-sm-' . $sidebar_span; ?>"
          role="complementary">

          <?php CustomHook::leftsidebarright_before_sidebar_2() ?>
          <?php dynamic_sidebar('sidebar-2'); ?>
          <?php CustomHook::leftsidebarright_after_sidebar_2() ?>
        </div>
        <?php } ?>
      </div>
    </div>
    <?php
  }
}










    /*
     * Index Contents
     *
     */

    static function contents_index() {
      if(has_action('dh_contents_index')) {
        do_action('dh_contents_index');
      } else {
        $pd_options = get_option('peadig'); ?>
        <div id="content"  class="container">
          <div class="row" role="main">
            <div id="primary" class="col-lg-<?php echo $pd_options['main_span']; ?>">

              <?php CustomHook::index_before_loop() ?>

              <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

              <?php CustomHook::index_before_article() ?>
              <article <?php post_class() ?> id="post-<?php the_ID(); ?>">
                <?php CustomHook::index_article_top() ?>

                <?php CustomHook::page_before_loop() ?>
                <h2 class="entry-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>

                <div class="entry">
                  <?php the_content(); ?>
                </div>

                <footer class="postmetadata">
                  <?php the_tags('Tags: ', ', ', '<br>'); ?>
                  Posted in <?php the_category(', ') ?> |
                  <?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?>
                </footer>
                <?php CustomHook::index_article_bottom() ?>
              </article>
              <?php CustomHook::index_after_article() ?>
            <?php endwhile; ?>

            <?php DefaultHook::numeric_posts_nav(); ?>

          <?php else : ?>

          <h1><?php _e('Not Found','peadig');?></h1>

        <?php endif; ?>

        <?php CustomHook::index_after_loop() ?>
      </div>
    </div>
  </div>

  <?php
}
}









    /*
     * Page Template
     *
     */

    static function page_template() {
      if(has_action('dh_page_template')) {
        do_action('dh_page_template');
      } else {
        $pd_options = get_option('peadig');

        ?>

        <div class="container">
          <div class="row" <?php if(!is_home()) { echo 'role="main"';} ?>>
            <section id="primary" class="col-lg-<?php echo $pd_options['main_span']; ?> col-md-<?php echo $pd_options['main_span']; ?> col-sm-<?php echo $pd_options['main_span']; ?>">

              <?php CustomHook::page_before_loop() ?>

              <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

              <?php CustomHook::page_before_article() ?>
              <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <?php CustomHook::page_article_top() ?>

                <?php if (empty($pd_options['noh1'])) { echo '<h1>'.get_the_title().'</h1>'; } ?>

                <?php if (!empty($pd_options['sharing_pages'])) {
                  DefaultHook::social_buttons();
                } ?>

                <div class="entry">
                  <?php edit_post_link('Edit', '<p><button type="button" class="btn btn-info">', '</button></p>'); ?>
                  <?php the_content(); ?>
                  <?php wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number')); ?>
                  <?php edit_post_link('Edit', '<p><button type="button" class="btn btn-info">', '</button></p>'); ?>
                </div>

                <?php CustomHook::page_article_bottom() ?>
              </article>
              <?php CustomHook::page_after_article() ?>

              <?php if(!empty($pd_options['comments_pages'])) {
                comments_template();
              } ?>

            <?php endwhile; endif; ?>

            <?php CustomHook::page_after_loop() ?>
          </section>

          <?php DefaultHook::sidebar();  ?>

        </div>
      </div>
      <?php
    }
  }









    /*
     * WooCommerce Template
     *
     */

    static function woocommerce_template() {
      if(has_action('dh_woocommerce_template')) {
        do_action('dh_woocommerce_template');
      } else {
        $pd_options = get_option('peadig'); ?>

        <div class="container">
          <div class="row" <?php if(!is_home()) { echo 'role="main"';} ?>>
            <section id="primary" class="col-lg-<?php echo $pd_options['main_span']; ?> col-md-<?php echo $pd_options['main_span']; ?> col-sm-<?php echo $pd_options['main_span']; ?>">

              <?php woocommerce_content(); ?>

            </section>

            <?php DefaultHook::sidebar();  ?>

          </div>
        </div>
        <?php
      }
    }










    /*
     * Full Width Page Template
     *
     */

    static function page_fullwidth_template() {
      if(has_action('dh_page_fullwidth_template')) {
        do_action('dh_page_fullwidth_template');
      } else {
        $pd_options = get_option('peadig');
        ?>

        <div class="container">
          <div class="row" <?php if(!is_home()) { echo 'role="main"';} ?>>
            <section id="primary"  role="main" class="col-lg-12 col-md-12 col-sm-12">
              <?php CustomHook::fullwidth_before_loop() ?>
              <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
              <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                <?php CustomHook::fullwidth_before_article() ?>
                <article>
                  <?php CustomHook::fullwidth_article_top() ?>

                  <?php if (empty($pd_options['noh1'])) { echo '<h1 class="entry-title">'.get_the_title().'</h1>'; } ?>
                  <?php if (!empty($pd_options['sharing_pages'])) {
                    DefaultHook::social_buttons();
                  } ?>
                  <div class="entry-content">
                    <?php edit_post_link('Edit', '<p><button type="button" class="btn btn-info">', '</button></p>'); ?>
                    <?php the_content(); ?>
                    <?php wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number')); ?>
                    <?php the_tags( 'Tags: ', ', ', ''); ?>
                    <?php edit_post_link('Edit', '<p><button type="button" class="btn btn-info">', '</button></p>'); ?>
                  </div>
                  <?php CustomHook::fullwidth_article_bottom() ?>
                </article>
                <?php CustomHook::fullwidth_after_article() ?>

              </div>
              <?php if(!empty($pd_options['comments_pages'])) {
                comments_template();
              } ?>
            <?php endwhile; endif; ?>
            <?php CustomHook::fullwidth_after_loop() ?>
          </section>
        </div>
      </div>

      <?php
    }
  }










    /*
     * Contact Page Template
     *
     */

    static function page_contact_template($stage, $post=NULL) {
      if(has_action('dh_page_contact_template')) {
        do_action('dh_page_contact_template');
      } else {
        $pd_options = get_option('peadig');

        if($stage == 1){
          if(trim($post['contactName']) === '') {
            $name_error = 'Please enter your name.';
            $has_error = true;
          } else {
            $name = trim($post['contactName']);
          }

          if(trim($post['email']) === '')  {
            $email_error = 'Please enter your email address.';
            $has_error = true;
          } else if (!preg_match("/^[[:alnum:]][a-z0-9_.-]*@[a-z0-9.-]+\.[a-z]{2,4}$/i", trim($post['email']))) {
            $email_error = 'You entered an invalid email address.';
            $has_error = true;
          } else {
            $email = trim($post['email']);
          }

          if(trim($post['comments']) === '') {
            $comment_error = 'Please enter a message.';
            $has_error = true;
          } else {
            if(function_exists('stripslashes')) {
              $comments = stripslashes(trim($post['comments']));
            } else {
              $comments = trim($post['comments']);
            }
          }

          if(!isset($has_error)) {
            $emailTo = $pd_options['email'];
            if (!isset($emailTo) || ($emailTo == '') ){
              $emailTo = get_option('admin_email');
            }
            $site = get_bloginfo('name');
            $subject = $site.' Contact Form: '.$name;
            $body = "Name: $name \n\nEmail: $email \n\nMessage:\n$comments";
            $headers = 'From: '.$name.' <'.$emailTo.'>' . "\r\n" . 'Reply-To: ' . $email;

            wp_mail($emailTo, $subject, $body, $headers);
            $email_sent = true;
          }
        }
        ?>


        <div class="container">
          <div class="row" <?php if(!is_home()) {echo 'role="main"';} ?>>

            <div class="col-lg-12">
              <?php if (empty($pd_options['noh1'])) { echo '<h1>'.get_the_title().'</h1>'; } ?>
            </div>


            <div id="primary" class="col-lg-6 col-md-6 col-sm-6">

              <?php CustomHook::contact_before_loop() ?>

              <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

              <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <?php CustomHook::contact_before_article() ?>
                <article>
                  <?php CustomHook::contact_article_top() ?>

                  <div class="entry">

                    <?php edit_post_link('Edit', '<p><button type="button" class="btn btn-default btn info">', '</button></p>'); ?>

                    <?php the_content(); ?>

                    <?php wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number')); ?>
                    <?php edit_post_link('Edit', '<p><button type="button" class="btn btn-info">', '</button></p>'); ?>
                  </div>

                  <?php CustomHook::contact_article_bottom() ?>
                </article>
                <?php CustomHook::contact_after_article() ?>
              </div>

              <?php if(!empty($pd_options['comments_pages'])) {
                comments_template();
              } ?>
            <?php endwhile; ?>
          <?php endif; ?>
          <?php CustomHook::contact_after_loop() ?>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6">
          <?php global $user_ID; ?>

          <?php if( $user_ID ): ?>

          <?php if( current_user_can('level_10') ): ?>

          <div class="alert alert-info fade in">
            <h4>About the Contact Form <span class="text-muted"><em><small>- only logged in admins can see this message...</small></em></span></h4> This is a simple contact form that sends emails to the address set <a href="<?php $url = site_url(); echo $url.'/wp-admin/options-general.php'; ?>">here</a>.<br>If you want more options we suggest you use <a href="">Gravity Forms</a>.
          </div>

        <?php endif; ?>

      <?php endif; ?>

      <?php if(isset($email_sent) && $email_sent == true): ?>
      <div class="alert alert-success fade in">
        <strong>Thanks!</strong> Your email was sent successfully. We'll get back to you as soon as we can.
      </div>
    <?php else: ?>

    <?php if(isset($has_error) || isset($captcha_error)) { ?>
    <div class="alert alert-error fade in">
      <strong>Sorry!</strong> Your email wasn't sent. Please try again.
    </div>
    <?php } ?>

    <form role="form" action="<?php the_permalink(); ?>" id="contactForm" method="post">
      <div class="form-group<?php if($name_error != '') { ?> has-error <?php } ?>">
        <label class="control-label" for="contactName">Name</label>
        <input type="text" name="contactName" id="contactName" value="<?php
        if(isset($post['contactName'])) {
          echo $post['contactName'];
        } ?>" class="form-control" placeholder="Enter your name...">
      </div>
      <div class="form-group<?php if($email_error != '') { ?> has-error <?php } ?>">
        <label class="control-label" for="email">Email<?php if($email_error != '') { ?> - <?php echo $email_error; } ?></label>

        <input type="email" name="email" id="email" value="<?php
        if(isset($post['email'])) {
          echo $post['email'];
        }?>" class="form-control email" placeholder="you@yourdomain.com">
      </div>

      <div class="form-group<?php if($comment_error != '') { ?> has-error <?php } ?>">
        <label class="control-label" for="commentsText">Message<?php if($comment_error != '') { ?> - <?php echo $comment_error; } ?></label>
        <textarea name="comments" id="commentsText" class="form-control" rows="8" placeholder="Type your message here..."><?php
        if(isset($post['comments'])) {
          if(function_exists('stripslashes')) {
            echo stripslashes($post['comments']);
          } else {
            echo $post['comments'];
          }
        } ?></textarea>
      </div>

      <button type="submit" name="submit" class="btn btn-default btn-primary pull-right">Send Email</button>&nbsp;<button type="reset" class="btn btn-default pull-right btn-reset-margin">Reset</button>
      <br class="clearfix">
      <input type="hidden" name="submitted" id="submitted" value="true">
    </form>

  <?php endif; ?>
</div>
</div>
<?php
}
}












    /*
     * Left Sidebar Template
     *
     */

    static function page_leftsidebar_template() {
      if(has_action('dh_page_leftsidebar_template')) {
        do_action('dh_page_leftsidebar_template');
      } else {
        $pd_options = get_option('peadig');
        ?>

        <div class="container">
          <div class="row" <?php if(!is_home()) { echo 'role="main"';} ?>>

            <?php DefaultHook::sidebar();  ?>

            <section id="primary" class="col-lg-<?php echo $pd_options['main_span']; ?> col-md-<?php echo $pd_options['main_span']; ?> col-sm-<?php echo $pd_options['main_span']; ?>">

              <?php CustomHook::page_before_loop() ?>

              <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

              <?php CustomHook::page_before_article() ?>
              <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <?php CustomHook::page_article_top() ?>

                <?php if (empty($pd_options['noh1'])) { ?><h1><?php the_title(); ?></h1><?php }?>

                <?php if (!empty($pd_options['sharing_pages'])) {
                  DefaultHook::social_buttons();
                } ?>

                <div class="entry">
                  <?php edit_post_link('Edit', '<p><button type="button" class="btn btn-info">', '</button></p>'); ?>
                  <?php the_content(); ?>
                  <?php wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number')); ?>
                  <?php edit_post_link('Edit', '<p><button type="button" class="btn btn-info">', '</button></p>'); ?>
                </div>

                <?php CustomHook::page_article_bottom() ?>
              </article>
              <?php CustomHook::page_after_article() ?>

              <?php if(!empty($pd_options['comments_pages'])) {
                comments_template();
              } ?>

            <?php endwhile; endif; ?>

            <?php CustomHook::page_after_loop() ?>
          </section>

        </div>
      </div>

      <?php
    }
  }











    /*
     * Double Sidebar Template
     *
     */

    static function page_sidebarcontentsidebar_template() {
      if(has_action('dh_page_sidebarcontentsidebar_template')) {
        do_action('dh_page_sidebarcontentsidebar_template');
      } else {
        $pd_options = get_option('peadig');

        if ($pd_options['main_span']=='11' || $pd_options['main_span']=='9' || $pd_options['main_span']=='7') {
          $pd_options['main_span']=$pd_options['main_span']-1;
        }

        $sidebar_span = (12-$pd_options['main_span']) / 2;
        $sidebar_span = round($sidebar_span, 0, PHP_ROUND_HALF_DOWN);
        ?>
        <div class="container">
          <div class="row">
            <div id="secondary" class="widget-area <?php echo 'col-lg-'.$sidebar_span.' col-md-'.$sidebar_span.' col-sm-'.$sidebar_span; ?>" role="complementary">
              <?php CustomHook::leftsidebarright_before_sidebar_1() ?>
              <?php dynamic_sidebar('sidebar-1'); ?>
              <?php CustomHook::leftsidebarright_after_sidebar_1() ?>
            </div>
            <section <?php if(!is_home()) {echo 'role="main"';} ?> id="primary" class="<?php echo 'col-lg-'.$pd_options['main_span'].' col-md-'.$pd_options['main_span'].' col-sm-'.$pd_options['main_span']; ?>">
              <?php CustomHook::leftsidebarright_before_loop() ?>
              <?php if (have_posts()) : ?>
              <?php while (have_posts()) : the_post(); ?>

              <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <article>
                  <?php if (empty($pd_options['noh1'])) { echo '<h1>'.get_the_title().'</h1>'; } ?>
                  <?php if (!empty($pd_options['sharing_pages'])) {
                    DefaultHook::social_buttons();
                  } ?>
                  <div class="entry">
                    <?php edit_post_link('Edit', '<p><button type="button" class="btn btn-default btn info">', '</button></p>'); ?>
                    <?php the_content(); ?>
                    <?php wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number')); ?>
                    <?php edit_post_link('Edit', '<p><button type="button" class="btn btn-default btn info">', '</button></p>'); ?>
                  </div>
                </article>
              </div>

              <?php if(!empty($pd_options['comments_pages'])) {
                comments_template();
              } ?>

            <?php endwhile; ?>
          <?php endif; ?>
          <?php CustomHook::leftsidebarright_after_loop() ?>
        </section>

        <div id="tertiary" class="widget-area <?php echo 'col-lg-'.$sidebar_span.' col-md-'.$sidebar_span.' col-sm-'.$sidebar_span; ?>" role="complementary">
          <?php CustomHook::leftsidebarright_before_sidebar_2() ?>
          <?php dynamic_sidebar('sidebar-2'); ?>
          <?php CustomHook::leftsidebarright_after_sidebar_2() ?>
        </div>
      </div>
    </div>

    <?php
  }
}










    /*
     * Single Page Contents
     *
     */

    static function contents_single() {
      if(has_action('dh_contents_single')) {
        do_action('dh_contents_single');
      } else {
        $pd_options = get_option('peadig'); ?>

        <div class="container">
          <div class="row">
            <?php if($pd_options['post_sidebar']=="2"){
              DefaultHook::sidebar();
            }


            if ($pd_options['post_sidebar']=="4") {
              if ($pd_options['main_span']=='11' || $pd_options['main_span']=='9' || $pd_options['main_span']=='7') {
                $pd_options['main_span']=$pd_options['main_span']-1;

              }
              $sidebar_span = (12-$pd_options['main_span']) / 2;
              $sidebar_span = round($sidebar_span, 0, PHP_ROUND_HALF_DOWN);
              ?>

              <div id="secondary" class="widget-area <?php echo 'col-lg-'.$sidebar_span.' col-md-'.$sidebar_span.' col-sm-'.$sidebar_span; ?>" role="complementary">
                <?php CustomHook::leftsidebarright_before_sidebar_1() ?>
                <?php dynamic_sidebar('sidebar-1'); ?>
                <?php CustomHook::leftsidebarright_after_sidebar_1() ?>
              </div>
              <?php }

              if($pd_options['post_sidebar']!="1"): ?>
              <section id="primary" class="col-lg-<?php echo $pd_options['main_span']; ?> col-md-<?php echo $pd_options['main_span']; ?> col-sm-<?php echo $pd_options['main_span']; ?>" role="main">
              <?php else: ?>
              <section id="primary" class="col-lg-12 col-md-12 col-sm-12" role="main">
              <?php endif; ?>

              <?php CustomHook::single_before_loop() ?>

              <?php if (have_posts()) : ?>
              <?php while (have_posts()) : the_post(); ?>
              <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <?php CustomHook::single_before_article() ?>
                <article>
                  <div class="postmeta">
                    <?php CustomHook::single_before_header() ?>
                    <?php if (empty($pd_options['noh1'])) { echo '<h1 class="entry-title">'.get_the_title().'</h1>'; } ?>
                    <?php CustomHook::single_after_header() ?>

                    <?php if (!empty($pd_options['post_meta_top'])) {
                      DefaultHook::meta_top();
                    } ?>
                    <?php if (!empty($pd_options['sharing_posts']) && !empty($pd_options['share_before'])) {
                      DefaultHook::social_buttons();
                    } ?>
                  </div>
                  <div class="entry-content">
                    <?php edit_post_link('Edit', '<p><button type="button" class="btn btn-info btn-sm">', '</button></p>'); ?>
                    <?php
                    if ( has_post_thumbnail() && !empty($pd_options['featimg'])) : echo '<p>'; the_post_thumbnail('post-thumbnail', array('class' => $pd_options['featured_image_class'])); echo '</p>'; endif;
                    DefaultHook::old_post_notifications();
                    the_content(); ?>
                    <div class="entry-info clearfix">
                      <?php wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number')); ?>
                      <?php edit_post_link('Edit', '<p><button type="button" class="btn btn-info btn-sm">', '</button></p>'); ?>
                      <p>Categories: <?php the_category(', ') ?></p>
                      <?php the_tags( 'Tags: ', ', ', ''); ?>
                    </div>
                  </div>
                  <div class="postmetabtm clearfix">
                    <?php if (!empty($pd_options['sharing_posts']) && !empty($pd_options['share_after'])) {
                      DefaultHook::social_buttons();
                    } ?>
                    <?php DefaultHook::meta_bottom(); ?>
                  </div>
                </article>
                <?php CustomHook::single_after_article() ?>
              </div>
              <?php comments_template(); ?>
            <?php endwhile;?>
          <?php endif; ?>
          <?php CustomHook::single_after_loop() ?>
        </section>

        <?php if($pd_options['post_sidebar']=="3"){
          DefaultHook::sidebar();
        }

        if ($pd_options['post_sidebar']=="4") { ?>
        <div id="tertiary" class="widget-area <?php echo 'col-lg-'.$sidebar_span.' col-md-'.$sidebar_span.' col-sm-'.$sidebar_span; ?>" role="complementary">
          <?php CustomHook::leftsidebarright_before_sidebar_2() ?>
          <?php dynamic_sidebar('sidebar-2'); ?>
          <?php CustomHook::leftsidebarright_after_sidebar_2() ?>
        </div>
        <?php } ?>

      </div>
    </div>

    <?php
  }
}

}

