<?php




/*
 * All the functions for the frontend side of WordPress live here
 *
 ********************************************************************************/










define ('SHOW_HOOKS', false);

class CustomHook {

    /*
     * 404 page hooks
     *
     */

    static function e404_before_h1() {
        do_action('e404_before_h1');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function e404_after_h1() {
        do_action('e404_after_h1');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function e404_before_search() {
        do_action('e404_before_search');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function e404_after_search() {
        do_action('e404_after_search');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function e404_before_recent_posts() {
        do_action('e404_before_recent_posts');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function e404_after_recent_posts() {
        do_action('e404_after_recent_posts');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }









    /*
     * Archive hooks
     *
     */

    static function archive_before_loop() {
        do_action('archive_before_loop');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }
    static function archive_after_loop() {
        do_action('archive_after_loop');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function archive_before_h1() {
        do_action('archive_before_h1');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function archive_after_h1() {
        do_action('archive_after_h1');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function archive_before_article() {
        do_action('archive_before_article');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function archive_after_article() {
        do_action('archive_after_article');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function archive_article_top() {
        do_action('archive_article_top');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function archive_article_bottom() {
        do_action('archive_article_bottom');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function archive_before_excerpt() {
        do_action('archive_before_excerpt');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function archive_after_excerpt() {
        do_action('archive_after_excerpt');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function archive_before_numeric_posts_nav() {
        do_action('archive_before_numeric_posts_nav');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function archive_after_numeric_posts_nav() {
        do_action('archive_after_numeric_posts_nav');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function archive_after_nothing_found() {
        do_action('archive_after_nothing_found');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }









    /*
     * Author hooks
     *
     */

    static function author_before_bio() {
        do_action('author_before_bio');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function author_after_bio() {
        do_action('author_after_bio');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function author_before_display_name() {
        do_action('author_before_bio');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function author_before_after_name() {
        do_action('author_before_after_name');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function author_social_top() {
        do_action('author_social_top');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function author_social_bottom() {
        do_action('author_social_bottom');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function author_before_description() {
        do_action('author_before_description');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function author_after_description() {
        do_action('author_after_description');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function author_before_posts_by() {
        do_action('author_before_posts_by');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function author_after_posts_by() {
        do_action('author_after_posts_by');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function author_before_no_posts() {
        do_action('author_before_no_posts');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function author_after_no_posts() {
        do_action('author_after_no_posts');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }









    /*
     * Comments hooks
     *
     */

    static function comments_before_heading() {
        do_action('comments_before_heading');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function comments_after_heading() {
        do_action('comments_after_heading');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function comments_before_comment_tab() {
        do_action('comments_before_comment_tab');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function comments_after_comment_tab() {
        do_action('comments_after_comment_tab');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function comments_before_tab_content() {
        do_action('comments_before_tab_content');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function comments_after_tab_content() {
        do_action('comments_after_tab_content');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }









    /*
     * Footer hooks
     *
     */

    static function footer_before_footer() {
        do_action('footer_before_footer');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function footer_after_footer() {
        do_action('footer_after_footer');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function footer_top() {
        do_action('footer_top');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function footer_bottom() {
        do_action('footer_bottom');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function footer_above_wp_footer() {
        do_action('footer_above_wp_footer');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function footer_below_wp_footer() {
        do_action('footer_below_wp_footer');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }









    /*
     * Front Page hooks
     *
     */

    static function front_page_before_home_slider() {
        do_action('front_page_before_home_slider');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }
    static function front_page_after_home_slider() {
        do_action('front_page_after_home_slider');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }
    static function front_page_before_home_jumbotron() {
        do_action('front_page_before_home_jumbotron');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }
    static function front_page_after_home_jumbotron() {
        do_action('front_page_after_home_jumbotron');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }
    static function front_page_before_home_services() {
        do_action('front_page_before_home_services');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }
    static function front_page_after_home_services() {
        do_action('front_page_after_home_services');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }
    static function front_page_before_home_quote() {
        do_action('front_page_before_home_quote');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }
    static function front_page_after_home_quote() {
        do_action('front_page_after_home_quote');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }
    static function front_page_before_home_content() {
        do_action('front_page_before_home_content');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }
    static function front_page_after_home_content() {
        do_action('front_page_after_home_content');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function front_page_footer_top() {
        do_action('front_page_footer_top');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function front_page_footer_bottom() {
        do_action('front_page_footer_bottom');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }









    /*
     * Header hooks
     *
     */

    static function header_before_html() {
        do_action('header_before_html');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function header_inside_head_top() {
        do_action('header_inside_head_top');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function header_above_title() {
        do_action('header_above_title');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function header_below_title() {
        do_action('header_below_title');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function header_inside_head_above_wp_head() {
        do_action('header_inside_head_above_wp_head');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function header_inside_head_below_wp_head() {
        do_action('header_inside_head_below_wp_head');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function header_inside_body_top() {
        do_action('header_inside_body_top');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function header_above_header() {
        do_action('header_above_header');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function header_inside_header_top() {
        do_action('header_inside_header_top');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function header_inside_header_bottom() {
        do_action('header_inside_header_bottom');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function header_below_header() {
        do_action('header_below_header');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function header_above_nav() {
        do_action('header_above_nav');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function header_below_nav() {
        do_action('header_below_nav');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function header_above_breadcrumb() {
        do_action('header_above_breadcrumb');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function header_below_breadcrumb() {
        do_action('header_below_breadcrumb');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }









    /*
     * Peadig custom footer function hooks
     *
     */

    static function before_sidebar() {
        do_action('before_sidebar');
        if(SHOW_HOOKS){
            CustomHook::the_function(__FUNCTION__);
        }
    }

    static function after_sidebar() {
        do_action('after_sidebar');
        if(SHOW_HOOKS){
            CustomHook::the_function(__FUNCTION__);
        }
    }

    static function before_footer() {
        do_action('before_footer');
        if(SHOW_HOOKS){
            CustomHook::the_function(__FUNCTION__);
        }
    }

    static function after_footer() {
        do_action('after_footer');
        if(SHOW_HOOKS){
            CustomHook::the_function(__FUNCTION__);
        }
    }









    /*
     * Index hooks
     *
     */

    static function index_before_loop() {
        do_action('index_before_loop');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function index_after_loop() {
        do_action('index_after_loop');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function index_before_article() {
        do_action('index_before_article');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function index_after_article() {
        do_action('index_after_article');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function index_article_top() {
        do_action('index_article_top');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function index_article_bottom() {
        do_action('index_article_bottom');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }









    /*
     * Page hooks
     *
     */

    static function page_before_article() {
        do_action('page_before_article');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function page_after_article() {
        do_action('page_after_article');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function page_article_top() {
        do_action('page_article_top');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function page_article_bottom() {
        do_action('page_article_bottom');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function page_before_loop() {
        do_action('page_before_loop');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function page_after_loop() {
        do_action('page_after_loop');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }









    /*
     * Page Contact hooks
     *
     */

    static function contact_before_loop() {
        do_action('contact_before_loop');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function contact_after_loop() {
        do_action('contact_after_loop');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function contact_before_article() {
        do_action('contact_before_article');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function contact_after_article() {
        do_action('contact_after_article');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function contact_article_top() {
        do_action('contact_article_top');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function contact_article_bottom() {
        do_action('contact_article_bottom');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }









    /*
     * Page Full Width hooks
     *
     */

    static function fullwidth_before_loop() {
        do_action('fullwidth_before_loop');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function fullwidth_after_loop() {
        do_action('fullwidth_after_loop');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function fullwidth_before_article() {
        do_action('fullwidth_before_article');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function fullwidth_after_article() {
        do_action('fullwidth_after_article');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function fullwidth_article_top() {
        do_action('fullwidth_article_top');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function fullwidth_article_bottom() {
        do_action('fullwidth_article_bottom');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }









    /*
     * Page Left Sidebar hooks
     *
     */

    static function leftsidebar_before_loop() {
        do_action('leftsidebar_before_loop');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function leftsidebar_after_loop() {
        do_action('leftsidebar_after_loop');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function leftsidebar_before_article() {
        do_action('leftsidebar_before_article');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function leftsidebar_after_article() {
        do_action('leftsidebar_after_article');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function leftsidebar_article_top() {
        do_action('leftsidebar_article_top');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function leftsidebar_article_bottom() {
        do_action('leftsidebar_article_bottom');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }









    /*
     * Page leftsidebarright hooks
     *
     */

    static function leftsidebarright_before_loop() {
        do_action('leftsidebarright_before_loop');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function leftsidebarright_after_loop() {
        do_action('leftsidebarright_after_loop');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function leftsidebarright_before_sidebar_1() {
        do_action('leftsidebarright_before_sidebar_1');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function leftsidebarright_after_sidebar_1() {
        do_action('leftsidebarright_after_sidebar_1');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function leftsidebarright_before_sidebar_2() {
        do_action('leftsidebarright_before_sidebar_2');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function leftsidebarright_after_sidebar_2() {
        do_action('leftsidebarright_after_sidebar_2');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }









    /*
     * Search hooks
     *
     */

    static function search_before_loop() {
        do_action('search_before_loop');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function search_after_loop() {
        do_action('search_after_loop');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function search_before_header() {
        do_action('search_before_header');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function search_after_header() {
        do_action('search_after_header');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }









    /*
     * Single hooks
     *
     */

    static function single_before_loop() {
        do_action('single_before_loop');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function single_after_loop() {
        do_action('single_after_loop');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function single_before_header() {
        do_action('single_before_header');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function single_after_header() {
        do_action('single_after_header');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function single_before_article() {
        do_action('single_before_article');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

    static function single_after_article() {
        do_action('single_after_article');
        if(SHOW_HOOKS){
             CustomHook::the_function(__FUNCTION__);
        }
    }

















    /*
     * Function to display hooks inline in the source code for admins
     *
     */

    static function the_function($function){
        global $user_ID;
        if( $user_ID ) {
            if( current_user_can('level_10') ) {

                echo "\n<!-- Action: ".$function." -->\n\n";
            }
        }
    }


}