<?php

class ShortcodeMCEBuilder {
    private $sc_number = "";

    private $options = "";

    private $span_options = '<option value="">Not Set</option><option value="1">1 Column</option><option value="2">2 Columns</option><option value="3">3 Columns</option><option value="4">4 Columns</option><option value="5">5 Columns</option><option value="6">6 Columns</option><option value="7">7 Columns</option><option value="8">8 Columns</option><option value="9">9 Columns</option><option value="10">10 Columns</option><option value="11">11 Columns</option><option value="12">12 Columns</option>';

    private $visibility_options = '<option value="">Not Set</option><option value="visible">Visible</option> <option value="hidden">Hidden</option>';

    private $list_group_options = '<option value="">Not Set</option><option value="linked">Linked</option>';

    private $bool_options = '<option value="">Not Set</option><option value="yes">Yes</option> <option value="no">No</option>';

    private $type_options = '<option value="">Not Set</option><option value="text-muted">Muted</option><option value="text-primary">Primary</option> <option value="text-warning">Warning</option><option value="text-danger">Danger</option><option value="text-info">Info</option><option value="text-success">Success</option>';

    private $list_order_options = '<option value="ul">Unordrered</option><option value="ol">Ordered</option> <option value="no">No</option>';

    private $pre_code_options = '<option value="yes">Pre</option><option value="no">Code</option>';

    private $panel_colors = '<option value="">Not Set</option><option value="panel-muted">Muted</option><option value="panel-primary">Primary</option> <option value="panel-warning">Warning</option><option value="panel-danger">Danger</option><option value="panel-info">Info</option><option value="panel-success">Success</option>';

    private $label_options = '<option value="badge">Badge</option><option value="label-default">Default Label</option><option value="label-warning">Warning Label</option><option value="label-danger">Danger Label</option><option value="label-info">Info Label</option><option value="label-success">Success Label</option>';

    private $progress_options = '<option value="progress-bar-info">Info</option><option value="progress-bar-danger">Danger</option><option value="progress-bar-success">Warning</option><option value="progress-bar-success">Success</option>';

    private $alert_options = '<option value="alert-info">Info</option><option value="alert-danger">Danger Label</option><option value="alert-success">Success Label</option>';

    private $button_color_options = '<option value="default">Default</option><option value="primary">Primary</option> <option value="warning">Warning</option><option value="danger">Danger</option><option value="info">Info</option><option value="success">Success</option><option value="link">Text Link</option>';

    private $button_size_options = '<option value="">Normal</option><option value="btn-lg">Large</option> <option value="btn-sm">Small</option><option value="btn-xs">Extra Small</option>';

    private $tooltip_options = '<option value="top">Top</option><option value="right">Right</option> <option value="bottom">Bottom</option><option value="left">Left</option>';

    private $glyphicons_options = '<option value="glass">glass</option><option value="music">music</option><option value="search">search</option><option value="envelope">envelope</option><option value="heart">heart</option><option value="star">star</option><option value="star-empty">star-empty</option><option value="user">user</option><option value="film">film</option><option value="th-large">th-large</option><option value="th">th</option><option value="th-list">th-list</option><option value="ok">ok</option><option value="remove">remove</option><option value="zoom-in">zoom-in</option><option value="zoom-out">zoom-out</option><option value="off">off</option><option value="signal">signal</option><option value="cog">cog</option><option value="trash">trash</option><option value="home">home</option><option value="file">file</option><option value="time">time</option><option value="road">road</option><option value="download-alt">download-alt</option><option value="download">download</option><option value="upload">upload</option><option value="inbox">inbox</option><option value="play-circle">play-circle</option><option value="repeat">repeat</option><option value="refresh">refresh</option><option value="list-alt">list-alt</option><option value="lock">lock</option><option value="flag">flag</option><option value="headphones">headphones</option><option value="volume-off">volume-off</option><option value="volume-down">volume-down</option><option value="volume-up">volume-up</option><option value="qrcode">qrcode</option><option value="barcode">barcode</option><option value="tag">tag</option><option value="tags">tags</option><option value="book">book</option><option value="bookmark">bookmark</option><option value="print">print</option><option value="camera">camera</option><option value="font">font</option><option value="bold">bold</option><option value="italic">italic</option><option value="text-height">text-height</option><option value="text-width">text-width</option><option value="align-left">align-left</option><option value="align-center">align-center</option><option value="align-right">align-right</option><option value="align-justify">align-justify</option><option value="list">list</option><option value="indent-left">indent-left</option><option value="indent-right">indent-right</option><option value="facetime-video">facetime-video</option><option value="picture">picture</option><option value="pencil">pencil</option><option value="map-marker">map-marker</option><option value="adjust">adjust</option><option value="tint">tint</option><option value="edit">edit</option><option value="share">share</option><option value="check">check</option><option value="move">move</option><option value="step-backward">step-backward</option><option value="fast-backward">fast-backward</option><option value="backward">backward</option><option value="play">play</option><option value="pause">pause</option><option value="stop">stop</option><option value="forward">forward</option><option value="fast-forward">fast-forward</option><option value="step-forward">step-forward</option><option value="eject">eject</option><option value="chevron-left">chevron-left</option><option value="chevron-right">chevron-right</option><option value="plus-sign">plus-sign</option><option value="minus-sign">minus-sign</option><option value="remove-sign">remove-sign</option><option value="ok-sign">ok-sign</option><option value="question-sign">question-sign</option><option value="info-sign">info-sign</option><option value="screenshot">screenshot</option><option value="remove-circle">remove-circle</option><option value="ok-circle">ok-circle</option><option value="ban-circle">ban-circle</option><option value="arrow-left">arrow-left</option><option value="arrow-right">arrow-right</option><option value="arrow-up">arrow-up</option><option value="arrow-down">arrow-down</option><option value="share-alt">share-alt</option><option value="resize-full">resize-full</option><option value="resize-small">resize-small</option><option value="plus">plus</option><option value="minus">minus</option><option value="asterisk">asterisk</option><option value="exclamation-sign">exclamation-sign</option><option value="gift">gift</option><option value="leaf">leaf</option><option value="fire">fire</option><option value="eye-open">eye-open</option><option value="eye-close">eye-close</option><option value="warning-sign">warning-sign</option><option value="plane">plane</option><option value="calendar">calendar</option><option value="random">random</option><option value="comment">comment</option><option value="magnet">magnet</option><option value="chevron-up">chevron-up</option><option value="chevron-down">chevron-down</option><option value="retweet">retweet</option><option value="shopping-cart">shopping-cart</option><option value="folder-close">folder-close</option><option value="folder-open">folder-open</option><option value="resize-vertical">resize-vertical</option><option value="resize-horizontal">resize-horizontal</option><option value="hdd">hdd</option><option value="bullhorn">bullhorn</option><option value="bell">bell</option><option value="certificate">certificate</option><option value="thumbs-up">thumbs-up</option><option value="thumbs-down">thumbs-down</option><option value="hand-right">hand-right</option><option value="hand-left">hand-left</option><option value="hand-up">hand-up</option><option value="hand-down">hand-down</option><option value="circle-arrow-right">circle-arrow-right</option><option value="circle-arrow-left">circle-arrow-left</option><option value="circle-arrow-up">circle-arrow-up</option><option value="circle-arrow-down">circle-arrow-down</option><option value="globe">globe</option><option value="wrench">wrench</option><option value="tasks">tasks</option><option value="filter">filter</option><option value="briefcase">briefcase</option><option value="fullscreen">fullscreen</option><option value="dashboard">dashboard</option><option value="paperclip">paperclip</option><option value="heart-empty">heart-empty</option><option value="link">link</option><option value="phone">phone</option><option value="pushpin">pushpin</option><option value="euro">euro</option><option value="usd">usd</option><option value="gbp">gbp</option><option value="sort">sort</option><option value="sort-by-alphabet">sort-by-alphabet</option><option value="sort-by-alphabet-alt">sort-by-alphabet-alt</option><option value="sort-by-order">sort-by-order</option><option value="sort-by-order-alt">sort-by-order-alt</option><option value="sort-by-attributes">sort-by-attributes</option><option value="sort-by-attributes-alt">sort-by-attributes-alt</option><option value="unchecked">unchecked</option><option value="expand">expand</option><option value="collapse">collapse</option><option value="collapse-top">collapse-top</option>';

    public function __construct($sc, $options){
        $this->sc_number = $sc;
        $this->options = $options;
    }

    public function build(  $name = false, $description = false){

        $output = ' <div class="pd_sc_codes" id="pd_sc_'.$this->sc_number.'">';

            if($name != false){
                $output .= $this->build_name($name);
            }

            if($description != false){
                $output .= $this->build_description($description);
            }

            if(in_array("span",$this->options)){
                $output .= $this->build_form("Span", 'span', 'span');
            }

            if(in_array("col-lg-offset-",$this->options)){
                $output .= $this->build_form("Offset", 'col-lg-offset-', 'span');
            }

            if(in_array("id",$this->options)){
                $output .= $this->build_form("ID", 'id', 'input');
            }

            if(in_array("class",$this->options)){
                $output .= $this->build_form("Class", 'class', 'input');
            }

            if(in_array("active",$this->options)){
                $output .= $this->build_form("Active", 'active', 'bool');
            }

            if(in_array("type_lg",$this->options)){
                $output .= $this->build_form("Type", 'type_lg', 'type_lg');
            }

            if(in_array("type_dd",$this->options)){
                $output .= $this->build_form("Type", 'type_dd', 'type_dd');
            }

            if(in_array("type_btn",$this->options)){
                $output .= $this->build_form("Type", 'type_dd', 'type_dd');
            }

            if(in_array("type",$this->options)){
                $output .= $this->build_form("Type", 'type', 'input');
            }

            if(in_array("initialism",$this->options)){
                $output .= $this->build_form("Initialism", 'initialism', 'bool');
            }

            if(in_array("horizontal",$this->options)){
                $output .= $this->build_form("Horizontal", 'horizontal', 'bool');
            }

            if(in_array("bordered",$this->options)){
                $output .= $this->build_form("Bordered", 'bordered', 'bool');
            }

            if(in_array("hover",$this->options)){
                $output .= $this->build_form("Hover", 'hover', 'bool');
            }

            if(in_array("condensed",$this->options)){
                $output .= $this->build_form("Condensed", 'condensed', 'bool');
            }

            if(in_array("animated",$this->options)){
                $output .= $this->build_form("Animated", 'animated', 'bool');
            }

            if(in_array("rowspan",$this->options)){
                $output .= $this->build_form("Row Span", 'rowspan', 'input');
            }

            if(in_array("colspan",$this->options)){
                $output .= $this->build_form("Column Span", 'colspan', 'input');
            }

            if(in_array("amount",$this->options)){
                $output .= $this->build_form("Amount", 'amount', 'input');
            }

            if(in_array("panel_color",$this->options)){
                $output .= $this->build_form("Color", 'color', 'panel_color');
            }

            if(in_array("type_icon",$this->options)){
                $output .= $this->build_form("Icon Type", 'type', 'icon_type');
            }

            if(in_array("type_btn",$this->options)){
                $output .= $this->build_form("Button Color", 'type', 'button_type');
            }

            if(in_array("type_label",$this->options)){
                $output .= $this->build_form("Label Type", 'type', 'type_label');
            }

            if(in_array("type_alert",$this->options)){
                $output .= $this->build_form("Alert Type", 'type', 'type_alert');
            }

            if(in_array("progress_type",$this->options)){
                $output .= $this->build_form("Progress Type", 'type', 'type_progress');
            }

            if(in_array("btn_size",$this->options)){
                $output .= $this->build_form("Button Size", 'size', 'button_size');
            }

            if(in_array("header",$this->options)){
                $output .= $this->build_form("Header", 'header', 'input', true);
            }

            if(in_array("footer",$this->options)){
                $output .= $this->build_form("Footer", 'footer', 'input', true);
            }

            if(in_array("right",$this->options)){
                $output .= $this->build_form("Right", 'right', 'bool');
            }

            if(in_array("white",$this->options)){
                $output .= $this->build_form("White", 'white', 'bool');
            }

            if(in_array("unstyled",$this->options)){
                $output .= $this->build_form("Unstyled", 'unstyled', 'bool');
            }

            if(in_array("list-inline",$this->options)){
                $output .= $this->build_form("Inline", 'inline', 'bool');
            }

            if(in_array("ordered",$this->options)){
                $output .= $this->build_form("Ordered", 'ordered', 'list_order');
            }

            if(in_array("block",$this->options)){
                $output .= $this->build_form("Block", 'block', 'block');
            }

            if(in_array("block_btn",$this->options)){
                $output .= $this->build_form("Block", 'block', 'bool');
            }

            if(in_array("striped",$this->options)){
                $output .= $this->build_form("Striped", 'striped', 'bool');
            }

            if(in_array("Animated",$this->options)){
                $output .= $this->build_form("Animated", 'animated', 'bool');
            }

            if(in_array("dismissbutton",$this->options)){
                $output .= $this->build_form("Dismiss Button", 'dismissbutton', 'bool');
            }

            if(in_array("closebutton",$this->options)){
                $output .= $this->build_form("Close Button", 'closebutton', 'bool');
            }

            if(in_array("xbutton",$this->options)){
                $output .= $this->build_form("X Button", 'xbutton', 'bool');
            }

            if(in_array("block_alert",$this->options)){
                $output .= $this->build_form("Block", 'block', 'bool');
            }

            if(in_array("vertical",$this->options)){
                $output .= $this->build_form("Vertical", 'vertical', 'bool');
            }

            if(in_array("justified",$this->options)){
                $output .= $this->build_form("Justified", 'justified', 'bool');
            }

            if(in_array("closetext",$this->options)){
                $output .= $this->build_form("Close Text", 'closetext', 'input');
            }

            if(in_array("alt",$this->options)){
                $output .= $this->build_form("Alt", 'alt', 'input');
            }

            if(in_array("accordianid",$this->options)){
                $output .= $this->build_form("Accordian ID", 'accordianid', 'input');
            }

            if(in_array("allowhtml",$this->options)){
                $output .= $this->build_form("Allow HTML", 'allowhtml', 'bool');
            }

            if(in_array("placement",$this->options)){
                $output .= $this->build_form("Placement", 'placement', 'type_tooltip');
            }



        // content blocks

            if(in_array("image",$this->options)){
                $output .= $this->build_form("Image", 'image', 'input', true);
            }
            if(in_array("title",$this->options)){
                $output .= $this->build_form("Title", 'title', 'input', true);
            }
            if(in_array("link",$this->options)){
                $output .= $this->build_form("Link", 'link', 'input', true);
            }
            if(in_array("caption",$this->options)){
                $output .= $this->build_form("Caption", 'caption', 'input', true);
            }
            if(in_array("text",$this->options)){
                $output .= $this->build_form("Text", 'text', 'input', true);
            }
            if(in_array("source",$this->options)){
                $output .= $this->build_form("Source", 'source', 'input', true);
            }







        if(in_array("fluid",$this->options) || in_array("lg",$this->options) || in_array("sm",$this->options) || in_array("md",$this->options) ){
            $output .= $this->build_responsive_header();

            if(in_array("fluid",$this->options)){
                $output .= $this->build_form("Fluid", 'fluid', 'visibility');
            }

            if(in_array("lg",$this->options)){
                $output .= $this->build_form("Desktop", 'lg', 'visibility');
            }

            if(in_array("sm",$this->options)){
                $output .= $this->build_form("Phone", 'sm', 'visibility');
            }

            if(in_array("md",$this->options)){
                $output .= $this->build_form("Tablet", 'md', 'visibility');
            }



        }

        $output .= '<div class="pd_sc_optrow_button"><p class="pd_sc_fakelink pd_sc_insert" data-value="'.$this->sc_number.'">Insert into Post</p></div><div class="clear"></div></div>';


        echo $output;


    }

    private function build_name($name){

        return '<h2>'.$name.'</h2>';

    }

    private function build_description($desc){

        return '<p>'.$desc.'</p>';

    }

    private function build_responsive_header(){

        return '<div class="clear"></div><h2>Responsive Options</h2>';

    }

    private function build_form($name,$slug, $type, $wide = false){

        if($wide){$content = '<div class="clear"></div>';}

        $content = '<div class="pd_sc_optrow';

        if($wide){$content .= "_wide";}

        $content .= '"><p><label>';

        $content .= $name;

        $content .= '</label><br>';

        switch($type){
            case 'span':
                $content .= '<select class="pd_sc_'.$this->sc_number.'_';
                $content .= $slug;
                $content .= '">';
                $content .= $this->span_options;
                $content .= '</select>';
            break;
            case 'visibility':
                $content .= '<select class="pd_sc_'.$this->sc_number.'_';
                $content .= $slug;
                $content .= '">';
                $content .= $this->visibility_options;
                $content .= '</select>';
            break;
            case 'bool':
                $content .= '<select class="pd_sc_'.$this->sc_number.'_';
                $content .= $slug;
                $content .= '">';
                $content .= $this->bool_options;
                $content .= '</select>';
            break;
            case 'type':
                $content .= '<select class="pd_sc_'.$this->sc_number.'_';
                $content .= $slug;
                $content .= '">';
                $content .= $this->type_options;
                $content .= '</select>';
            break;
            case 'list_order':
                $content .= '<select class="pd_sc_'.$this->sc_number.'_';
                $content .= $slug;
                $content .= '">';
                $content .= $this->list_order_options;
                $content .= '</select>';
            break;
            case 'block':
                $content .= '<select class="pd_sc_'.$this->sc_number.'_';
                $content .= $slug;
                $content .= '">';
                $content .= $this->pre_code_options;
                $content .= '</select>';
            break;
            case 'type_lg':
                $content .= '<select class="pd_sc_'.$this->sc_number.'_';
                $content .= $slug;
                $content .= '">';
                $content .= $this->list_group_options;
                $content .= '</select>';
            break;
            case 'type_dd':
                $content .= '<select class="pd_sc_'.$this->sc_number.'_';
                $content .= $slug;
                $content .= '">';
                $content .= $this->list_group_options;
                $content .= '</select>';
            break;
            case 'panel_color':
                $content .= '<select class="pd_sc_'.$this->sc_number.'_';
                $content .= $slug;
                $content .= '">';
                $content .= $this->panel_colors;
                $content .= '</select>';
            break;
            case 'button_type':
                $content .= '<select class="pd_sc_'.$this->sc_number.'_';
                $content .= $slug;
                $content .= '">';
                $content .= $this->button_color_options;
                $content .= '</select>';
            break;
            case 'button_size':
                $content .= '<select class="pd_sc_'.$this->sc_number.'_';
                $content .= $slug;
                $content .= '">';
                $content .= $this->button_size_options;
                $content .= '</select>';
            break;
            case 'icon_type':
                $content .= '<select class="pd_sc_'.$this->sc_number.'_';
                $content .= $slug;
                $content .= '">';
                $content .= $this->glyphicons_options;
                $content .= '</select>';
            break;
            case 'type_label':
                $content .= '<select class="pd_sc_'.$this->sc_number.'_';
                $content .= $slug;
                $content .= '">';
                $content .= $this->label_options;
                $content .= '</select>';
            break;
            case 'type_alert':
                $content .= '<select class="pd_sc_'.$this->sc_number.'_';
                $content .= $slug;
                $content .= '">';
                $content .= $this->alert_options;
                $content .= '</select>';
            break;
            case 'type_progress':
                $content .= '<select class="pd_sc_'.$this->sc_number.'_';
                $content .= $slug;
                $content .= '">';
                $content .= $this->progress_options;
                $content .= '</select>';
            break;
            case 'type_tooltip':
                $content .= '<select class="pd_sc_'.$this->sc_number.'_';
                $content .= $slug;
                $content .= '">';
                $content .= $this->tooltip_options;
                $content .= '</select>';
            break;
            case 'input':
                $content .= '<input class="pd_sc_'.$this->sc_number.'_';
                $content .= $slug;
                $content .= '">';
            break;
        }

        $content .= '</p></div>';

        return $content;

    }


}