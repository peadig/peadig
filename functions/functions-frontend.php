<?php


/*
 * All the functions for the frontend side of WordPress live here
 *
 ********************************************************************************/


/*
 * Loding of CSS, JS etc
 *
 */


function pd_styles() {
	$pd_options = get_option( 'peadig' );
	if ( ! empty( $pd_options['bootstrap_css'] ) ) {
		if ( empty( $pd_options['bootstrap_cdn'] ) ) {
			wp_register_style( 'bootstrap-style', get_template_directory_uri() . '/css/bootstrap.min.css' );
		} else {
			wp_register_style( 'bootstrap-style', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css' );
		}
		wp_enqueue_style( 'bootstrap-style' );
	}

	wp_register_style( 'default-style', get_bloginfo( 'stylesheet_url' ) );
	wp_enqueue_style( 'default-style' );

	if ( ! empty( $pd_options['use_fontawesome'] ) ) {
		if ( empty( $pd_options['bootstrap_cdn'] ) ) {
			wp_register_style( 'fontawesome-style', get_template_directory_uri() . '/css/font-awesome.min.css' );
		} else {
			wp_register_style( 'fontawesome-style', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' );
		}
		wp_enqueue_style( 'fontawesome-style' );
	}

	wp_enqueue_script( 'jquery' );

	if ( ! empty( $pd_options['bootstrap_js'] ) ) {
		if ( empty( $pd_options['bootstrap_cdn'] ) ) {
			wp_enqueue_script( 'bootstrap-script', get_template_directory_uri() . '/js/bootstrap.min.js', array( 'jquery' ), '', 'true' );
		} else {
			wp_enqueue_script( 'bootstrap-script', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', array( 'jquery' ), '', 'true' );
		}
	}

}

add_action( 'wp_enqueue_scripts', 'pd_styles' );

/*
 * Backwards compatibility for wp_title
 *
 */
if ( ! function_exists( '_wp_render_title_tag' ) ) {
    function peadig_slug_render_title() {
?><title><?php wp_title( '|', true, 'right' ); ?></title><?php
    }
    add_action( 'wp_head', 'peadig_slug_render_title' );
}



/*
 * Move wpautop filter to AFTER shortcode is processed
 *
 */
$pd_options = get_option( 'peadig' );


if ( $pd_options['wpautop'] == 1 ) {
	remove_filter( 'the_content', 'wpautop' );
	add_filter( 'the_content', 'wpautop', 99 );
	add_filter( 'the_content', 'shortcode_unautop', 100 );
}


add_filter( 'widget_text', 'do_shortcode' );


/*
 * Making LESS work and then enqueue it
 *
 */
function pd_less_styles( $tag, $handle ) {

	global $wp_styles;

	$match_pattern = '/\.less$/U';

	if ( preg_match( $match_pattern, $wp_styles->registered[ $handle ]->src ) ) {
		$handle = $wp_styles->registered[ $handle ]->handle;
		$media  = $wp_styles->registered[ $handle ]->args;
		$href   = $wp_styles->registered[ $handle ]->src;
		$rel    = isset( $wp_styles->registered[ $handle ]->extra['alt'] ) && $wp_styles->registered[ $handle ]->extra['alt'] ? 'alternate stylesheet' : 'stylesheet';
		$title  = isset( $wp_styles->registered[ $handle ]->extra['title'] ) ? "title='" . esc_attr( $wp_styles->registered[ $handle ]->extra['title'] ) . "'" : '';

		$tag = "<link rel='stylesheet' id='$handle' $title href='$href' type='text/less' media='$media' />";
	}

	return $tag;
}

add_filter( 'style_loader_tag', 'pd_less_styles', 5, 2 );


/*
 * Adding logo to the login screen
 *
 */

function pd_login_logo() {

	$pd_options = get_option( 'peadig' );
	$version    = get_bloginfo( 'version' );

	if ( ! empty( $pd_options['admin_login_logo']['url'] ) ) {
		$img = $pd_options['admin_login_logo']['url'];
	} elseif ( $version < 3.8 ) {
		$img = get_template_directory_uri() . '/img/wordpress-peadig-old.png';
		$css = 'width: 274px !important;background-size: 274px 63px !important;';
	} else {
		$img = get_template_directory_uri() . '/img/wordpress-peadig.png';
		$css = 'width: 80px !important;background-size: 80px 80px !important;';
	}
	echo '<style type="text/css">
    h1 a { background-image: url(' . $img . ') !important;
    ' . $css . '}
    </style>';
}

add_action( 'login_head', 'pd_login_logo' );


/*
 * Cleaning up the <head> tags
 *
 */

function remove_head_links() {
	remove_action( 'wp_head', 'rsd_link' );
	remove_action( 'wp_head', 'wlwmanifest_link' );
}

add_action( 'init', 'remove_head_links' );
remove_action( 'wp_head', 'wp_generator' );
add_theme_support( 'automatic-feed-links' );


/*******************
 * MENU SETUP
 *******************/


function peadig_page_menu_args( $args ) {

	$args['show_home'] = true;

	return $args;

}

add_filter( 'wp_page_menu_args', 'peadig_page_menu_args' );


/**
 * REDIRECT TO SAME URL AFTER FORM LOGIN FAIL
 */
if ( ( isset( $_GET['action'] ) && $_GET['action'] != 'logout' ) || ( isset( $_POST['login_location'] ) && ! empty( $_POST['login_location'] ) ) ) {
	add_filter( 'login_redirect', 'my_login_redirect', 10, 3 );
	function my_login_redirect() {
		$location = $_SERVER['HTTP_REFERER'];
		wp_safe_redirect( $location );
		exit();
	}
}

/**
 * ADD CSS CLASS TO AVATARS
 */

add_filter( 'get_avatar', 'change_avatar_css' );

function change_avatar_css( $class ) {
	$pd_options = get_option( 'peadig' );
	$addition   = "class='" . $pd_options['avatar_class'];
	$class      = str_replace( "class='avatar", $addition, $class );

	return $class;
}


/*******************
 * PAGE NAVIGATION
 *******************/

if ( ! function_exists( 'peadig_content_nav' ) ):
	function peadig_content_nav( $nav_id ) {
		global $wp_query;
		?>
		<nav id="<?php
		echo $nav_id;
		?>">
			<h3 class="assistive-text section-heading">
				<?php
				_e( 'Post navigation', 'peadig' );
				?>
			</h3>
			<?php
			if ( is_single() ): // navigation links for single posts
				?>
				<?php
				previous_post_link( '<div class="nav-previous">%link</div>', '<span class="meta-nav">' . _x( '&larr;', 'Previous post link', 'peadig' ) . '</span> %title' );
				?>
				<?php
				next_post_link( '<div class="nav-next">%link</div>', '%title <span class="meta-nav">' . _x( '&rarr;', 'Next post link', 'peadig' ) . '</span>' );
				?>
				<?php
			elseif ( $wp_query->max_num_pages > 1 && ( is_home() || is_archive() || is_search() ) ): // navigation links for home, archive, and search pages
				?>
				<?php
				if ( get_next_posts_link() ):
					?>
					<div class="nav-previous">
						<?php
						next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'peadig' ) );
						?>
					</div>
					<?php
				endif;
				?>
				<?php
				if ( get_previous_posts_link() ):
					?>
					<div class="nav-next">
						<?php
						previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'peadig' ) );
						?>
					</div>
					<?php
				endif;
				?>
				<?php
			endif;
			?>
		</nav>
		<!-- #<?php
		echo $nav_id;
		?> -->
		<?php
	}
endif; // peadig_content_nav


/**
 * Adds custom classes to the array of body classes.
 */
function peadig_body_classes( $classes ) {
	if ( ! is_multi_author() ) {
		$classes[] = 'single-author';
	}

	return $classes;
}

add_filter( 'body_class', 'peadig_body_classes' );

/**
 * Returns true if a blog has more than 1 category
 */
function peadig_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'all_the_cool_cats' ) ) ) {
		// Create an array of all the categories that are attached to posts
		$all_the_cool_cats = get_categories( array(
			'hide_empty' => 1
		) );

		// Count the number of categories that are attached to the posts
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'all_the_cool_cats', $all_the_cool_cats );
	}

	if ( '1' != $all_the_cool_cats ) {
		// This blog has more than 1 category so peadig_categorized_blog should return true
		return true;
	} else {
		// This blog has only 1 category so peadig_categorized_blog should return false
		return false;
	}
}


/*******************
 * FETCHING ONE CUSTOM FIELD WHEN NEEDED
 *******************/

function get_custom_field( $key, $echo = false ) {
	global $post;
	$custom_field = get_post_meta( $post->ID, $key, true );
	if ( $echo == false ) {
		return $custom_field;
	}
	echo $custom_field;
}


/**
 * 404 LOGGING
 */
function stop_404_guessing( $url ) {
	if ( is_404() ) {
		return false;
	}

	return $url;
}

add_filter( 'redirect_canonical', 'stop_404_guessing' );


/**
 * HEADER SCRIPTS
 */


//ADD XFBML
add_filter( 'language_attributes', 'schema' );
function schema( $attr ) {
	$attr .= " itemscope itemtype=\"http://schema.org/Article\" xmlns:og=\"http://ogp.me/ns#\" xmlns:fb=\"http://ogp.me/ns/fb#\"";

	return $attr;
}

//SOCIAL STRUCTURED DATA - TITLE
function peadig_sdtitle() {
	global $post;

	if ( is_singular() ) {
		$custom_fields = get_post_custom();
		if ( ! empty( $custom_fields ) ) {
			foreach ( $custom_fields as $field_key => $field_values ) {
				foreach ( $field_values as $key => $value ) {
					$post_meta[ $field_key ] = $value;
				} // builds array
			}
		}
		if ( ! empty( $post_meta['_peadig_fbog_title'] ) ) {
			$output = $post_meta['_peadig_fbog_title'];
		}
	} elseif ( is_category() ) {
		$output = single_cat_title() . ' - ' . get_bloginfo( 'name' );
		/* If this is a tag archive */
	} elseif ( is_tag() ) {
		$output = single_tag_title() . ' - ' . get_bloginfo( 'name' );
		/* If this is a daily archive */
	} elseif ( is_day() ) {
		$output = the_time( 'F jS, Y' ) . ' - ' . get_bloginfo( 'name' );
		/* If this is a monthly archive */
	} elseif ( is_month() ) {
		$output = the_time( 'F, Y' ) . ' - ' . get_bloginfo( 'name' );
		/* If this is a yearly archive */
	} elseif ( is_year() ) {
		$output = the_time( 'Y' ) . ' - ' . get_bloginfo( 'name' );
		/* If this is an author archive */
	} elseif ( is_author() ) {
		$output = get_the_author() . ' - author at ' . get_bloginfo( 'name' );
	} elseif ( is_home() || is_front_page() ) {
		$output = get_bloginfo( 'name' );
	}
	if ( $output == '' ) {
		$output = the_title() . ' - ' . get_bloginfo( 'name' );
	}

	return $output;
}

//SOCIAL STRUCTURED DATA - DESCRIPTION
function peadig_sddesc() {
	global $post;

	if ( is_category() ) {
		$cat    = get_query_var( 'cat' );
		$output = str_replace( "\n", " ", strip_tags( category_description( $cat ) ) );
		/* If this is a tag archive */
	} elseif ( is_tag() ) {
		$output = str_replace( "\n", " ", strip_tags( tag_description( $post->ID ) ) );
		/* If this is an author archive */
	} elseif ( is_day() ) {
		$output = the_time( 'F jS, Y' ) . ' - ' . get_bloginfo( 'name' );
		/* If this is a monthly archive */
	} elseif ( is_month() ) {
		$output = the_time( 'F, Y' ) . ' - ' . get_bloginfo( 'name' );
		/* If this is a yearly archive */
	} elseif ( is_year() ) {
		$output = the_time( 'Y' ) . ' - ' . get_bloginfo( 'name' );
		/* If this is an author archive */
	} elseif ( is_author() ) {
		$output = str_replace( "\n", " ", strip_tags( get_the_author_meta( 'description' ) ) );
	} elseif ( is_home() || is_front_page() ) {
		$output = get_bloginfo( 'description' );
	}
	if ( $output == '' ) {
		global $wp_query;
		$ID = $wp_query->post->ID;
		//echo $ID;
		if ( has_excerpt( $ID ) ) {
			$output = get_the_excerpt( $ID );
		} else {
			$output = htmlentities( substr( str_replace( "\n", " ", strip_tags( strip_shortcodes( $wp_query->post->post_content ) ) ), 0, 156 ) );
		}

	}

	return $output;

}

//SOCIAL STRUCTURED DATA - THUMBNAIL IMAGE
function peadig_sdimage() {
	if ( is_singular() ) {
		global $post;

		$args = array(
			'numberposts'    => 1,
			'order'          => 'ASC',
			'post_mime_type' => 'image',
			'post_parent'    => $post->ID,
			'post_status'    => null,
			'post_type'      => 'attachment',
		);

		$attachments = get_children( $args );

		if ( $attachments ) {

			foreach ( $attachments as $attachment ) {
				$image_attributes = wp_get_attachment_image_src( $attachment->ID, 'thumbnail' ) ? wp_get_attachment_image_src( $attachment->ID, 'thumbnail' ) : wp_get_attachment_image_src( $attachment->ID, 'full' );
				$output           = wp_get_attachment_thumb_url( $attachment->ID );
			}

		}
	}
	if ( empty( $output ) ) {

		$pd_options = get_option( 'peadig' );

		if ( ! empty( $pd_options['default_image']['url'] ) ) {
			$output = $pd_options['default_image']['url'];
		} elseif ( ! empty( $pd_options['logo']['url'] ) ) {
			$output = $pd_options['logo']['url'];
		}
		if ( empty( $output ) ) {
			$output = get_template_directory_uri() . '/img/250x250.jpg';
		}

	}
	print $output;
}

//SOCIAL STRUCTURED DATA - URL
function peadig_sdurl() {
	global $wp_query;
	$ID = $wp_query->post->ID;
	if ( is_category() ) {
		$output = get_category_link( get_query_var( 'cat' ) );
		/* If this is a tag archive */
	} elseif ( is_tag() ) {
		$output = get_tag_link( get_query_var( 'tag_id' ) );
		/* If this is a daily archive */
	} elseif ( is_day() ) {
		$archive_year  = get_the_time( 'Y' );
		$archive_month = get_the_time( 'm' );
		$archive_day   = get_the_time( 'd' );
		$output        = get_day_link( $archive_year, $archive_month, $archive_day );
		/* If this is a monthly archive */
	} elseif ( is_month() ) {
		$archive_year  = get_the_time( 'Y' );
		$archive_month = get_the_time( 'm' );
		$output        = get_month_link( $archive_year, $archive_month );
		/* If this is a yearly archive */
	} elseif ( is_year() ) {
		$archive_year = get_the_time( 'Y' );
		$output       = get_year_link( $archive_year );
		/* If this is an author archive */
	} elseif ( is_author() ) {
		$output = get_author_posts_url( get_the_author_meta( 'ID' ) );
	} elseif ( is_home() || is_front_page() ) {
		$output = home_url();
	}
	if ( empty( $output ) ) {
		$output = get_permalink();
	}

	return $output;
}


//SOCIAL STRUCTURED DATA - FORMATION
function peadig_structured_data() {
	$pd_options    = get_option( 'peadig' );
	$custom_fields = get_post_custom();
	if ( ! empty( $custom_fields ) ) {
		foreach ( $custom_fields as $field_key => $field_values ) {
			foreach ( $field_values as $key => $value ) {
				$post_meta[ $field_key ] = $value;
			} // builds array
		}
	}
	if ( is_front_page() && ! empty( $pd_options['google_plus_page_id'] ) ) {
		?>
		<link href="<?php
		echo $pd_options['google_plus_page_id'];
		?>" rel="publisher"/>
		<?php
	}
	?>
	<!-- Google+ META Data -->
	<meta itemprop="name" content="<?php
	if ( ! empty( $post_meta['_peadig_gplus_title'] ) && ( is_singular() || is_front_page() ) ) {
		echo $post_meta['_peadig_gplus_title'];
	} elseif ( ! empty( $post_meta['_peadig_fbog_title'] ) && ( is_singular() || is_front_page() ) ) {
		echo $post_meta['_peadig_fbog_title'];
	} else {
		echo peadig_sdtitle();
	} ?>">
	<meta itemprop="description" content="<?php
	if ( ! empty( $post_meta['_peadig_gplus_desc'] ) && ( is_singular() || is_front_page() ) ) {
		echo $post_meta['_peadig_gplus_desc'];
	} elseif ( ! empty( $post_meta['_peadig_fbog_desc'] ) && ( is_singular() || is_front_page() ) ) {
		echo $post_meta['_peadig_fbog_desc'];
	} elseif ( ! empty( $post_meta['_yoast_wpseo_metadesc'] ) && ( is_singular() || is_front_page() ) ) {
		echo $post_meta['_yoast_wpseo_metadesc'];
	} else {
		echo peadig_sddesc();
	}
	?>">
	<meta itemprop="image" content="<?php
	if ( ! empty( $post_meta['_peadig_gplus_image'] ) && ( is_singular() || is_front_page() ) ) {
		echo $post_meta['_peadig_gplus_image'];
	} elseif ( ! empty( $post_meta['_peadig_fbog_image'] ) && ( is_singular() || is_front_page() ) ) {
		echo $post_meta['_peadig_fbog_image'];
	} elseif ( ! empty( $post_meta['_yoast_wpseo_opengraph-image'] ) && ( is_singular() || is_front_page() ) ) {
		echo $post_meta['_yoast_wpseo_opengraph-image'];
	} else {
		echo peadig_sdimage();
	}
	?>">
	<?php
	$yoast_social = get_option( 'wpseo_social' );
	if ( empty( $yoast_social['opengraph'] ) || $yoast_social['opengraph'] != 'on' ) {
		?>
		<!-- Facebook Open Graph -->
		<?php
		if ( is_front_page() && ! empty( $pd_options['facebook_page_id'] ) && ( is_singular() || is_front_page() ) ) {
			?>
			<meta property="fb:page_id" content="<?php
			echo $pd_options['facebook_page_id'];
			?>" /><?php
		}
		if ( ! empty( $pd_options['facebook_page_url'] ) && ( is_singular() || is_front_page() ) ) {
			?>
			<meta property="article:publisher" content="<?php
			echo $pd_options['facebook_page_url'];
			?>" /><?php
		}
		$fbauthor = get_the_author_meta( 'facebook' );
		if ( ! empty( $fbauthor ) ) {
			?>
			<meta property="article:author" content="<?php echo $fbauthor; ?>" /><?php
		}
		if ( ! empty( $pd_options['facebook_app_id'] ) ) {
			?>
			<meta property="fb:app_id" content="<?php
			echo $pd_options['facebook_app_id'];
			?>"/><?php
		}
		?>

		<meta property="og:title" content="<?php
		if ( ! empty( $post_meta['_peadig_fbog_title'] ) && ( is_singular() || is_front_page() ) ) {
			echo $post_meta['_peadig_fbog_title'];
		} else {
			echo peadig_sdtitle();
		}
		?>"/>
		<meta property="og:description" content="<?php
		if ( ! empty( $post_meta['_peadig_fbog_desc'] ) && ( is_singular() || is_front_page() ) ) {
			echo $post_meta['_peadig_fbog_desc'];
		} elseif ( ! empty( $post_meta['_yoast_wpseo_metadesc'] ) && ( is_singular() || is_front_page() ) ) {
			echo $post_meta['_yoast_wpseo_metadesc'];
		} else {
			echo peadig_sddesc();
		}
		?>"/>
		<meta property="og:url" content="<?php
		echo peadig_sdurl();
		?>"/>
		<meta property="og:image" content="<?php
		if ( ! empty( $post_meta['_peadig_fbog_image'] ) && ( is_singular() || is_front_page() ) ) {
			echo $post_meta['_peadig_fbog_image'];
		} elseif ( ! empty( $post_meta['_yoast_wpseo_opengraph-image'] ) && ( is_singular() || is_front_page() ) ) {
			echo $post_meta['_yoast_wpseo_opengraph-image'];
		} else {
			echo peadig_sdimage();
		}
		?>"/>
		<meta property="og:type" content="<?php
		if ( is_front_page() ) {
			echo "website";
		} else {
			echo "article";
		}
		?>"/>
		<meta property="og:site_name" content="<?php
		bloginfo( 'name' );
		?>"/>
		<?php
	}
	if ( empty( $yoast_social['twitter'] ) || $yoast_social['twitter'] != 'on' ) {
		?>
		<!-- Twitter Cards -->
		<meta name="twitter:card" value="summary_large_image">
		<meta name="twitter:site" value="@<?php
		echo $pd_options['twitter_username'];
		?>">
		<meta name="twitter:url" value="<?php
		echo peadig_sdurl();
		?>">
		<meta name="twitter:title" value="<?php
		if ( ! empty( $post_meta['_peadig_twitter_title'] ) && ( is_singular() || is_front_page() ) ) {
			echo $post_meta['_peadig_twitter_title'];
		} elseif ( ! empty( $post_meta['_peadig_fbog_title'] ) && ( is_singular() || is_front_page() ) ) {
			echo $post_meta['_peadig_fbog_title'];
		} else {
			echo peadig_sdtitle();
		}
		?>">
		<meta name="twitter:description" content="<?php
		if ( ! empty( $post_meta['_peadig_twitter_desc'] ) && ( is_singular() || is_front_page() ) ) {
			echo $post_meta['_peadig_twitter_desc'];
		} elseif ( ! empty( $post_meta['_peadig_fbog_desc'] ) && ( is_singular() || is_front_page() ) ) {
			echo $post_meta['_peadig_fbog_desc'];
		} elseif ( ! empty( $post_meta['_yoast_wpseo_metadesc'] ) && ( is_singular() || is_front_page() ) ) {
			echo $post_meta['_yoast_wpseo_metadesc'];
		} else {
			echo peadig_sddesc();
		}
		?>">
		<meta name="twitter:image" value="<?php
		if ( ! empty( $post_meta['_peadig_twitter_image_1'] ) && ( is_singular() || is_front_page() ) ) {
			echo $post_meta['_peadig_twitter_image_1'];
		} elseif ( ! empty( $post_meta['_peadig_fbog_image'] ) && ( is_singular() || is_front_page() ) ) {
			echo $post_meta['_peadig_fbog_image'];
		} elseif ( ! empty( $post_meta['_yoast_wpseo_opengraph-image'] ) && ( is_singular() || is_front_page() ) ) {
			echo $post_meta['_yoast_wpseo_opengraph-image'];
		} else {
			echo peadig_sdimage();
		}
		?>">
		<?php
		$twitter_profile = get_the_author_meta( 'twitter' );
		if ( is_singular() && ! empty( $twitter_profile ) ) {
			?>
			<meta name="twitter:creator" value="@<?php
			echo $twitter_profile;
			?>">
			<?php
		}
		?>
		<?php
	}
	?>
	<?php
}

add_action( 'wp_head', 'peadig_structured_data', 1 );


//OTHER SCRIPTS
function header_scripts() {
	$pd_options = get_option( 'peadig' );

	$custom_fields = get_post_custom();
	if ( ! empty( $custom_fields ) ) {
		foreach ( $custom_fields as $field_key => $field_values ) {
			foreach ( $field_values as $key => $value ) {
				$post_meta[ $field_key ] = $value;
			} // builds array
		}
	}

	?>


	<style type="text/css">
		<?php
		//print_r($pd_options['h_font']);
		if ($pd_options['bg_color'] && $pd_options['bg_color'] != '#ffffff') {
		  echo "body {background-color: ".$pd_options['bg_color'].";}\n";
	  }
	  echo 'body {';
	  if (!empty($pd_options['text_font']['color']) || $pd_options['text_font']['color'] != '#333333') {
		echo 'color: '.$pd_options['text_font']['color'].';';
	}
	if (!empty($pd_options['text_font']['font-family']) && $pd_options['text_font']['font-family'] != 'Arial,Helvetica,Arial,sans-serif') {
		echo 'font-family: '.$pd_options['text_font']['font-family'].';';
	}
	if (!empty($pd_options['text_font']['font-size']) || $pd_options['text_font']['font-size'] != '14px') {
		echo 'font-size: '.$pd_options['text_font']['font-size'].';';
	}
	if (!empty($pd_options['text_font']['font-weight']) || $pd_options['text_font']['font-weight'] != '400') {
		echo 'font-weight: '.$pd_options['text_font']['font-weight'].';';
	}
	if (!empty($pd_options['text_font']['font-style']) || $pd_options['text_font']['font-style'] != '') {
		echo 'font-style: '.$pd_options['text_font']['font-style'].';';
	}
	echo '}
	';

	echo 'h1, h2, h3, h4, h5, h6 {';
	if (!empty($pd_options['h_font']['color']) || $pd_options['h_font']['color'] != '#333333') {
		echo 'color: '.$pd_options['h_font']['color'].';';
	}
	if (!empty($pd_options['h_font']['font-family']) && $pd_options['h_font']['font-family'] != 'Arial,Helvetica,Arial,sans-serif') {
		echo 'font-family: '.$pd_options['h_font']['font-family'].';';
	}
	if (!empty($pd_options['h_font']['font-weight']) || $pd_options['h_font']['font-weight'] != '400') {
		echo 'font-weight: '.$pd_options['h_font']['font-weight'].';';
	}
	if (!empty($pd_options['h_font']['font-style']) || $pd_options['h_font']['font-style'] != '') {
		echo 'font-style: '.$pd_options['h_font']['font-style'].';';
	}
	echo '}
	';

	if (!empty($pd_options['container_color'])) {
	  echo ".container {background-color: ".$pd_options['container_color'].";}\n";
	}
	if (!empty($pd_options['link_color']['regular']) && $pd_options['link_color']['regular'] != '#428bca') {
	  echo "a {color: ".$pd_options['link_color']['regular'].";}\n";
	}
	if (!empty($pd_options['link_color']['hover']) && $pd_options['link_color']['hover'] != '#2a6496') {
	  echo "a:hover {color: ".$pd_options['link_color']['hover'].";}\n";
	}
	if (!empty($pd_options['link_color']['active']) && $pd_options['link_color']['active'] != '#2a6496') {
	  echo "a:active {color: ".$pd_options['link_color']['active'].";}\n";
	}
	if (!empty($pd_options['nav_bg']) && $pd_options['nav_bg'] != '#f8f8f8') {
	  echo ".navbar-default {background-color: ".$pd_options['nav_bg'].";}\n";
	}
	if (!empty($pd_options['nav_border']) && $pd_options['nav_border'] != '#e7e7e7') {
	  echo ".navbar-default {border-color: ".$pd_options['nav_border'].";}\n";
	}
	if (!empty($pd_options['fixed_nav'])) {
	  if ($pd_options['fixed_nav_pos']=='top') {
		 echo "body {padding-top: 70px;}\n";
	 }
	 if ($pd_options['fixed_nav_pos']=='bottom') {
		 echo "body {padding-bottom: 70px;}\n";
	 }
	}
	if (!empty($pd_options['fixed_container_width'])) {
	 echo ".container {max-width:".$pd_options['max_container_width']['width']."; margin:0px auto;}\n";
	}
	if (!empty($pd_options['facebook_like']) && empty($pd_options['facebook_share'])) {
		echo '.peadigsocial .fb-like  {width: 90px;}';
	}
	?>
	</style>

	<?php
	if ( ! empty( $pd_options['header_js'] ) ): ?>
		<script type="text/javascript">
			<?php echo $pd_options['header_js']; ?>
		</script>
	<?php endif; ?>

	<?php
	if ( ! empty( $post_meta['_peadig_header_js'] ) ): ?>
		<script type="text/javascript">
			<?php echo $post_meta['_peadig_header_js'] ?>
		</script>
	<?php endif; ?>


	<?php
	if ( $pd_options['text_font']['google'] === true ) {
		$fonturl = '//fonts.googleapis.com/css?family=';
		$font    = preg_replace( "/ /", "+", $pd_options['text_font']['font-family'] );
		$font    = str_replace( "'", "", $font );
		//echo $fonturl.$font;
		wp_register_style( 'gf-text', $fonturl . $font );
		wp_enqueue_style( 'gf-text' );
	}
	if ( $pd_options['h_font']['google'] === true ) {
		$fonturl = '//fonts.googleapis.com/css?family=';
		$font    = preg_replace( "/ /", "+", $pd_options['h_font']['font-family'] );
		$font    = str_replace( "'", "", $font );
		//echo $fonturl.$font;
		wp_register_style( 'gf-h', $fonturl . $font );
		wp_enqueue_style( 'gf-h' );
	}

	if ( $pd_options['linkedin_share'] == 1 ) {
		wp_enqueue_script( 'linkedin-script', '//platform.linkedin.com/in.js' );
	}


}

add_action( 'wp_head', 'header_scripts', 1001 );

function remove_cssjs_ver( $src ) {
	if ( strpos( $src, '?ver=' ) ) {
		$src = remove_query_arg( 'ver', $src );
	}

	return $src;
}

add_filter( 'style_loader_src', 'remove_cssjs_ver', 1000 );
add_filter( 'script_loader_src', 'remove_cssjs_ver', 1000 );

/**
 * FOOTER SCRIPTS
 *
 */

function footer_scripts() {
	$pd_options = get_option( 'peadig' );

	$custom_fields = get_post_custom();
	if ( ! empty( $custom_fields ) ) {
		foreach ( $custom_fields as $field_key => $field_values ) {
			foreach ( $field_values as $key => $value ) {
				$post_meta[ $field_key ] = $value;
			} // builds array
		}
	}

	?>
	<script type="text/javascript">

		<?php if ( ! empty( $pd_options['tracking_code'] ) ) {
			echo $pd_options['tracking_code'];
		} ?>

		<?php if ( ! empty( $pd_options['footer_js'] ) ) {
			echo $pd_options['footer_js'];
		} ?>


		<?php if (! empty( $post_meta['_peadig_footer_js'] )): ?>

		<?php echo $post_meta['_peadig_footer_js'] ?>

		<?php endif; ?>
		!function (d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
			if (!d.getElementById(id)) {
				js = d.createElement(s);
				js.id = id;
				js.src = p + '://platform.twitter.com/widgets.js';
				fjs.parentNode.insertBefore(js, fjs);
			}
		}(document, 'script', 'twitter-wjs');


		(function () {
			var po = document.createElement('script');
			po.type = 'text/javascript';
			po.async = true;
			po.src = 'https://apis.google.com/js/platform.js';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(po, s);
		})();

		jQuery(document).ready(function ($) {

			$(function () {
				$("[rel='tooltip']").tooltip();
				$("[rel='popover']").popover();
			});

			$("a[rel=popover]")
				.popover()
				.click(function (e) {
					e.preventDefault();
				});

			$("a[rel=tooltip]")
				.tooltip()
				.click(function (e) {
					e.preventDefault();
				});
			<?php
			if (! empty( $pd_options['rel_external'] )) {
			?>
			$(function () {
				$('a[rel*=external]').click(function () {
					window.open(this.href);
					return false;
				});
			});
			<?php
			}
			if (! empty( $pd_options['fixed_meta_div'] ) && is_singular()) {
			?>
			$(function () {
				var msie6 = $.browser == 'msie' && $.browser.version < 7;
				if (!msie6) {
					var top = $('.postmeta.col-lg-offset').top - parseFloat($('.postmeta').css('margin-top').replace('/auto/', 0));

					$(window).scroll(function (event) {
						// what the y position of the scroll is
						var y = $(this).scrollTop();
						// whether that's below the form
						if (y >= top) {
							// if so, ad the fixed class
							$('.postmeta').addClass('gofixed col-lg-12 col-md-12 col-sm-12');
							$('.entry-title').addClass('col-lg-offset-1');
							$('.peadigsocial').addClass('col-lg-offset-1');
							$('#bio').addClass('col-lg-offset-1');
							$('#bio').removeClass('well');
						} else {
							// otherwise remove it
							$('.postmeta').removeClass('gofixed col-lg-12 col-md-12 col-sm-12');
							$('.entry-title').removeClass('col-lg-offset-1');
							$('#bio').removeClass('col-lg-offset-1');
							$('.peadigsocial').removeClass('col-lg-offset-1');

							<?php
							if (! empty( $pd_options['meta_top_well'] )) {
							?>$('#bio').addClass('well');<?php
							}
							?>
						}
					});
				}
			});
			<?php } ?>
		});

	</script>
	<?php
	if ( is_singular() || is_archive() ) {

		if ( ! empty( $pd_options['buffer_button'] ) ) { ?>
			<script type="text/javascript" src="http://static.bufferapp.com/js/button.js"></script>
		<?php }

		if ( ! empty( $pd_options['pocket_button'] ) ) {
			?>
			<script type="text/javascript">!function (d, i) {
					if (!d.getElementById(i)) {
						var j = d.createElement("script");
						j.id = i;
						j.src = "https://widgets.getpocket.com/v1/j/btn.js?v=1";
						var w = d.getElementById(i);
						d.body.appendChild(j);
					}
				}(document, "pocket-btn-js");</script><?php
		}

		if ( ! empty( $pd_options['stumble_upon'] ) ) { ?>

			<script type="text/javascript">
				(function () {
					var li = document.createElement('script');
					li.type = 'text/javascript';
					li.async = true;
					li.src = ('https:' == document.location.protocol ? 'https:' : 'http:') + '//platform.stumbleupon.com/1/widgets.js';
					var s = document.getElementsByTagName('script')[0];
					s.parentNode.insertBefore(li, s);
				})();
			</script>

			<?php
		}

		if ( ! empty( $pd_options['pinterest_pinit'] ) ) {
			?>
			<script type="text/javascript" async src="//assets.pinterest.com/js/pinit.js"></script><?php
		}

	}
}

add_action( 'wp_footer', 'footer_scripts', 100 );


/**
 * COMMENT FORM
 */
function peadig_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	extract( $args, EXTR_SKIP );

	if ( 'div' == $args['style'] ) {
		$tag       = 'div';
		$add_below = 'comment';
	} else {
		$tag       = 'li';
		$add_below = 'div-comment';
	}
	?>
	<<?php
	echo $tag;
	?><?php
	comment_class( empty( $args['has_children'] ) ? '' : 'parent' );
	?> id="comment-<?php
	comment_ID();
	?>">
	<?php
	if ( 'div' != $args['style'] ):
		?>
		<div id="div-comment-<?php
	comment_ID();
	?>" class="comment-body row">
		<?php
	endif;
	?>
	<div class="comment-author vcard col-lg-2 col-md-2 col-sm-2">
		<?php
		if ( $args['avatar_size'] != 0 ) {
			echo get_avatar( $comment, 64 );
		}
		?>
	</div>
	<div class="col-lg-10 col-md-10 col-sm-10">
	<?php
	printf( __( '<strong><cite class="fn">%s</cite></strong>' ), get_comment_author_link() );
	?>
	<?php
	if ( $comment->comment_approved == '0' ):
		?>
		<em class="text-warning"><?php
			_e( 'Your comment is awaiting moderation.', 'peadig-framework' );
			?></em>
		<br/>
		<?php
	endif;
	?>
	<?php
	comment_text();
	?>
	<span class="comment-meta">
	<a class="text-muted" href="<?php
	echo htmlspecialchars( get_comment_link( $comment->comment_ID ) );
	?>"><?php
		printf( __( '%1$s at %2$s', 'peadig-framework' ), get_comment_date(), get_comment_time() );
		?></a>
	<button type="button" class="btn btn-default btn-xs"><?php
		comment_reply_link( array_merge( $args, array(
			'add_below' => $add_below,
			'depth'     => $depth,
			'max_depth' => $args['max_depth']
		) ) );
		?></button>
	<?php
	edit_comment_link( __( 'Edit', 'peadig-framework' ), '<button type="button" class="btn btn-default btn-xs">', '</button>' );
	?>
	<?php
	if ( 'div' != $args['style'] ):
		?>
		</span>
		</div>
		</div>
		<?php
	endif;
}

?>