<?php

/**
 * WORDPRESS URL AND SITE NAME LINK
 */
function url_shortcode() {
	$output = '<a href="'.esc_url( home_url( '/' ) ).'">'.get_bloginfo('name').'</a>';
	return $output;
}
add_shortcode('wp-url','url_shortcode');


/**
 * PRIVATE CONTENT
 */

add_shortcode( 'user-content', 'echo_private_content' );
function echo_private_content($atts, $content = null) {
	extract(shortcode_atts(array(
		"lg" => '',
		"md" => '',
		"sm" => '',
		"xs" => '',
		), $atts));
	if ($lg=='visible') {$class.=' visible-lg';} elseif ($lg=='hidden') {$class.=' hidden-lg';}
	if ($md=='visible') {$class.=' visible-md';} elseif ($md=='hidden') {$class.=' hidden-md';}
	if ($sm=='visible') {$class.=' visible-sm';} elseif ($sm=='hidden') {$class.=' hidden-sm';}
	if ($xs=='visible') {$class.=' visible-xs';} elseif ($xs=='hidden') {$class.=' hidden-xs';}
	if (is_user_logged_in()) {$output = do_shortcode($content);}
	return $output;
}


add_shortcode( 'non-user-content', 'echo_non_private_content' );
function echo_non_private_content($atts, $content = null) {
	extract(shortcode_atts(array(
		"lg" => '',
		"md" => '',
		"sm" => '',
		"xs" => '',
		), $atts));
	if ($lg=='visible') {$class.=' visible-lg';} elseif ($lg=='hidden') {$class.=' hidden-lg';}
	if ($md=='visible') {$class.=' visible-md';} elseif ($md=='hidden') {$class.=' hidden-md';}
	if ($sm=='visible') {$class.=' visible-sm';} elseif ($sm=='hidden') {$class.=' hidden-sm';}
	if ($xs=='visible') {$class.=' visible-xs';} elseif ($xs=='hidden') {$class.=' hidden-xs';}
	if (!is_user_logged_in()) {$output = do_shortcode($content);}
	return $output;
}



/**
 * LOGIN FORM
 */

add_shortcode( 'login-form', 'echo_pd_login' );
function echo_pd_login($atts, $content = null) {
	extract(shortcode_atts(array(
		"id" => '',
		"class" => '',
		"inline" => '',
		"usrtext" => 'Username',
		"pwtext" => 'Password',
		"logintext" => 'Login',
		"logouttext" => 'Logout',
		"forgotpass" => '',
		"rememberbtn" => '',
		"inputsize" => '10',
		"lg" => '',
		"lg" => '',
		"md" => '',
		"sm" => '',
		"xs" => '',
		), $atts));
	if ($lg=='visible') {$class.=' visible-lg';} elseif ($lg=='hidden') {$class.=' hidden-lg';}
	if ($md=='visible') {$class.=' visible-md';} elseif ($md=='hidden') {$class.=' hidden-md';}
	if ($sm=='visible') {$class.=' visible-sm';} elseif ($sm=='hidden') {$class.=' hidden-sm';}
	if ($xs=='visible') {$class.=' visible-xs';} elseif ($xs=='hidden') {$class.=' hidden-xs';}
	if ($inline=='yes') {$class.=' form-inline';}
	if ($forgotpass!='no') {$forgot='<a href="'.$pd_url.'/wp-login.php?action=lostpassword">Lost your password?</a>';}
	if ($rememberbtn=='no') {$hiddenchk=' hidden';}
	$pd_url = home_url();
	if (!is_user_logged_in()){
		$output = '<form action="'.$pd_url.'/wp-login.php" method="post" class="'.$class.'">
		<input type="hidden" name="redirect_to" value="'.$pd_url. $_SERVER["REQUEST_URI"].'" />
		<div class="form-group">
		<input type="text" name="log" id="log" value="'.wp_specialchars(stripslashes($user_login), 1).'" size="'.$inputsize.'" placeholder="'.$usrtext.'" class="form-control" />
		</div>
		<div class="form-group">
		<input type="password" name="pwd" id="pwd" size="'.$inputsize.'" placeholder="'.$pwtext.'" class="form-control" />
		</div>
		<div class="checkbox'.$hiddenchk.'">
		<label><input name="rememberme" id="rememberme" type="checkbox" checked="checked" value="forever" /> Remember me</label>
		</div><button type="submit" name="submit" class="btn btn-primary" />'.$logintext.'</button>
		</form>'.$forgot;
	} else {
		$output = '<span class="'.$class.'"><a href="'.wp_logout_url( $pd_url ).'" class="btn btn-default">'.$logouttext.'</a></span>';
	}
	return $output;
}




/**
 * SCAFFOLDING
 */

add_shortcode( 'peadig-row', 'echo_peadig_row' );
add_shortcode( 'peadig-row-nest1', 'echo_peadig_row' );
add_shortcode( 'peadig-row-nest2', 'echo_peadig_row' );
add_shortcode( 'peadig-row-nest3', 'echo_peadig_row' );
function echo_peadig_row($atts, $content = null) {
	extract(shortcode_atts(array(
		"cols" => '',
		"id" => '',
		"class" => '',
		"lg" => '',
		"md" => '',
		"sm" => '',
		"xs" => '',
		"offset" => '',
		), $atts));
	if ($offset!='') {$offset = " offset".$offset;}
	if ($id!='') {$id = " id=\"".$id."\"";}
	if ($lg=='visible') {$class.=' visible-lg';} elseif ($lg=='hidden') {$class.=' hidden-lg';}
	if ($md=='visible') {$class.=' visible-md';} elseif ($md=='hidden') {$class.=' hidden-md';}
	if ($sm=='visible') {$class.=' visible-sm';} elseif ($sm=='hidden') {$class.=' hidden-sm';}
	if ($xs=='visible') {$class.=' visible-xs';} elseif ($xs=='hidden') {$class.=' hidden-xs';}
	$peadig_row = "<div".$id." class=\"row".$fluidappendix." col-lg-".$cols." col-md-".$cols." col-sm-".$cols." ".$class."\">".do_shortcode($content)."</div>";
	if ($cols<'12') {$peadig_row='<div class="col-lg-12 col-md-12 col-sm-12">'.$peadig_row.'</div>';}
	return $peadig_row;
}



add_shortcode( 'peadig-col', 'echo_peadig_col' );
add_shortcode( 'peadig-col-nest1', 'echo_peadig_col' );
add_shortcode( 'peadig-col-nest2', 'echo_peadig_col' );
add_shortcode( 'peadig-col-nest3', 'echo_peadig_col' );
function echo_peadig_col($atts, $content = null) {
	extract(shortcode_atts(array(
		"span" => '12',
		"cols" => '12',
		"offset" => '',
		"id" => '',
		"class" => '',
		"lg" => '',
		"md" => '',
		"sm" => '',
		"xs" => '',
		), $atts));
	if ($cols!='' && $cols!='12') { $span = $cols;}
	if ($offset!='') {$offset = " offset".$offset;}
	if ($id!='') {$id = " id=\"".$id."\"";}
	if ($lg=='visible') {$class.=' visible-lg';} elseif ($lg=='hidden') {$class.=' hidden-lg';}
	if ($md=='visible') {$class.=' visible-md';} elseif ($md=='hidden') {$class.=' hidden-md';}
	if ($sm=='visible') {$class.=' visible-sm';} elseif ($sm=='hidden') {$class.=' hidden-sm';}
	if ($xs=='visible') {$class.=' visible-xs';} elseif ($xs=='hidden') {$class.=' hidden-xs';}
	$output = '<div'.$id.' class="col-lg-'.$span.' col-md-'.$span.' col-sm-'.$span.$offset.' '.$class.'">'.do_shortcode($content).'</div>';
	return $output;
}












/**
 * TYPOGRAPHY
 */


//18
add_shortcode( 'jumbotron', 'echo_jumbotron' );
function echo_jumbotron($atts, $content = null) {
	extract(shortcode_atts(array(
		"id" => '',
		"class" => '',
		"title" => '',
		"lg" => '',
		"md" => '',
		"sm" => '',
		"xs" => '',
		), $atts));
	if ($id!='') {$id = " id=\"".$id."\"";}
	if ($lg=='visible') {$class.=' visible-lg';} elseif ($lg=='hidden') {$class.=' hidden-lg';}
	if ($md=='visible') {$class.=' visible-md';} elseif ($md=='hidden') {$class.=' hidden-md';}
	if ($sm=='visible') {$class.=' visible-sm';} elseif ($sm=='hidden') {$class.=' hidden-sm';}
	if ($xs=='visible') {$class.=' visible-xs';} elseif ($xs=='hidden') {$class.=' hidden-xs';}
	$ouput = '<div'.$id.' class="jumbotron '.$class.'"><h2>'.$title.'</h2>'.do_shortcode($content).'</div>';
	return $ouput;
}

//19
add_shortcode( 'well', 'echo_well' );
function echo_well($atts, $content = null) {
	extract(shortcode_atts(array(
		"id" => '',
		"class" => '',
		"lg" => '',
		"md" => '',
		"sm" => '',
		"xs" => '',
		), $atts));
	if ($id!='') {$id = " id=\"".$id."\"";}
	if ($lg=='visible') {$class.=' visible-lg';} elseif ($lg=='hidden') {$class.=' hidden-lg';}
	if ($md=='visible') {$class.=' visible-md';} elseif ($md=='hidden') {$class.=' hidden-md';}
	if ($sm=='visible') {$class.=' visible-sm';} elseif ($sm=='hidden') {$class.=' hidden-sm';}
	if ($xs=='visible') {$class.=' visible-xs';} elseif ($xs=='hidden') {$class.=' hidden-xs';}
	$ouput = '<div'.$id.' class="well '.$class.'">'.do_shortcode($content).'</div>';
	return $ouput;
}

//20
add_shortcode( 'p-lead', 'echo_plead' );
function echo_plead($atts, $content = null) {
	extract(shortcode_atts(array(
		"id" => '',
		"class" => '',
		"lg" => '',
		"md" => '',
		"sm" => '',
		"xs" => '',
		), $atts));
	if ($id!='') {$id = " id=\"".$id."\"";}
	if ($lg=='visible') {$class.=' visible-lg';} elseif ($lg=='hidden') {$class.=' hidden-lg';}
	if ($md=='visible') {$class.=' visible-md';} elseif ($md=='hidden') {$class.=' hidden-md';}
	if ($sm=='visible') {$class.=' visible-sm';} elseif ($sm=='hidden') {$class.=' hidden-sm';}
	if ($xs=='visible') {$class.=' visible-xs';} elseif ($xs=='hidden') {$class.=' hidden-xs';}
	$peadig_plead = "<p".$id." class=\"lead ".$class."\">".do_shortcode($content)."</p>";
	return $peadig_plead;
}

add_shortcode( 'emph', 'echo_emph' );
function echo_emph($atts, $content = null) {
	extract(shortcode_atts(array(
		"id" => '',
		"class" => '',
		"lg" => '',
		"md" => '',
		"sm" => '',
		"xs" => '',
		"type" => '',
		), $atts));
	if ($id!='') {$id = " id=\"".$id."\"";}
	if ($lg=='visible') {$class.=' visible-lg';} elseif ($lg=='hidden') {$class.=' hidden-lg';}
	if ($md=='visible') {$class.=' visible-md';} elseif ($md=='hidden') {$class.=' hidden-md';}
	if ($sm=='visible') {$class.=' visible-sm';} elseif ($sm=='hidden') {$class.=' hidden-sm';}
	if ($xs=='visible') {$class.=' visible-xs';} elseif ($xs=='hidden') {$class.=' hidden-xs';}
	if ($type!='') {$class.=' text-'.$type;}
	$output = '<span'.$id.' class="'.$class.'">'.do_shortcode($content).'</span>';
	return $output;
}

add_shortcode( 'abbr', 'echo_abbr' );
function echo_abbr($atts, $content = null) {
	extract(shortcode_atts(array(
		"id" => '',
		"class" => '',
		"lg" => '',
		"md" => '',
		"sm" => '',
		"xs" => '',
		"title" => '',
		"initialism" => '',
		), $atts));
	if ($id!='') {$id = " id=\"".$id."\"";}
	if ($lg=='visible') {$class.=' visible-lg';} elseif ($lg=='hidden') {$class.=' hidden-lg';}
	if ($md=='visible') {$class.=' visible-md';} elseif ($md=='hidden') {$class.=' hidden-md';}
	if ($sm=='visible') {$class.=' visible-sm';} elseif ($sm=='hidden') {$class.=' hidden-sm';}
	if ($xs=='visible') {$class.=' visible-xs';} elseif ($xs=='hidden') {$class.=' hidden-xs';}
	if ($initialism=='yes') {$class.=' initialism';}
	$output = '<abbr'.$id.' title="'.$title.'" class="'.$class.'">'.do_shortcode($content).'</abbr>';
	return $output;
}

add_shortcode( 'quote', 'echo_quote' );
function echo_quote($atts, $content = null) {
	extract(shortcode_atts(array(
		"id" => '',
		"class" => '',
		"lg" => '',
		"md" => '',
		"sm" => '',
		"xs" => '',
		"source" => '',
		"right" => '',
		), $atts));
	if ($id!='') {$id = " id=\"".$id."\"";}
	if ($right=='yes') {$class.=' pull-right col-lg-12';}
	if ($lg=='visible') {$class.=' visible-lg';} elseif ($lg=='hidden') {$class.=' hidden-lg';}
	if ($md=='visible') {$class.=' visible-md';} elseif ($md=='hidden') {$class.=' hidden-md';}
	if ($sm=='visible') {$class.=' visible-sm';} elseif ($sm=='hidden') {$class.=' hidden-sm';}
	if ($xs=='visible') {$class.=' visible-xs';} elseif ($xs=='hidden') {$class.=' hidden-xs';}
	$output = '<div><blockquote'.$id.' class="'.$class.'"><p>'.do_shortcode($content).'</p>';
	if ($source!='') {$output.='<small>'.$source.'</small>';}
	$output .= '</blockquote></div>';
	return $output;
}

add_shortcode( 'list', 'echo_list' );
function echo_list($atts, $content = null) {
	extract(shortcode_atts(array(
		"id" => '',
		"class" => '',
		"lg" => '',
		"md" => '',
		"sm" => '',
		"xs" => '',
		"ordered" => '',
		"unstyled" => '',
		"inline" => '',
		), $atts));
	if ($id!='') {$id = " id=\"".$id."\"";}
	if ($ordered=='yes') {$type='ol';} else {$type='ul';}
	if ($unstyled=='yes') {$class.=' list-unstyled';}
	if ($inline=='yes') {$class.=' list-inline';}
	if ($lg=='visible') {$class.=' visible-lg';} elseif ($lg=='hidden') {$class.=' hidden-lg';}
	if ($md=='visible') {$class.=' visible-md';} elseif ($md=='hidden') {$class.=' hidden-md';}
	if ($sm=='visible') {$class.=' visible-sm';} elseif ($sm=='hidden') {$class.=' hidden-sm';}
	if ($xs=='visible') {$class.=' visible-xs';} elseif ($xs=='hidden') {$class.=' hidden-xs';}
	$output = '<'.$type.' class="'.$class.'">'.do_shortcode($content).'</'.$type.'>';
	return $output;
}

add_shortcode( 'item', 'echo_item' );
function echo_item($atts, $content = null) {
	extract(shortcode_atts(array(
		"id" => '',
		"class" => '',
		"lg" => '',
		"md" => '',
		"sm" => '',
		"xs" => '',
		), $atts));
	if ($id!='') {$id = " id=\"".$id."\"";}
	if ($lg=='visible') {$class.=' visible-lg';} elseif ($lg=='hidden') {$class.=' hidden-lg';}
	if ($md=='visible') {$class.=' visible-md';} elseif ($md=='hidden') {$class.=' hidden-md';}
	if ($sm=='visible') {$class.=' visible-sm';} elseif ($sm=='hidden') {$class.=' hidden-sm';}
	if ($xs=='visible') {$class.=' visible-xs';} elseif ($xs=='hidden') {$class.=' hidden-xs';}
	$output = '<li class="'.$class.'">'.do_shortcode($content).'</li>';
	return $output;
}


add_shortcode( 'list-group', 'echo_list_group' );
function echo_list_group($atts, $content = null) {
	extract(shortcode_atts(array(
		"id" => '',
		"class" => '',
		"lg" => '',
		"md" => '',
		"sm" => '',
		"xs" => '',
		"type_lg" => '',
		), $atts));
	if ($id!='') {$id = " id=\"".$id."\"";}
	if ($lg=='visible') {$class.=' visible-lg';} elseif ($lg=='hidden') {$class.=' hidden-lg';}
	if ($md=='visible') {$class.=' visible-md';} elseif ($md=='hidden') {$class.=' hidden-md';}
	if ($sm=='visible') {$class.=' visible-sm';} elseif ($sm=='hidden') {$class.=' hidden-sm';}
	if ($xs=='visible') {$class.=' visible-xs';} elseif ($xs=='hidden') {$class.=' hidden-xs';}
	if ($type_lg=='linked') {$type_lg='div';} else {$type_lg='ul';}
	$output = '<'.$type_lg.' class="list-group '.$class.'">'.do_shortcode($content).'</'.$type_lg.'>';
	return $output;
}

add_shortcode( 'list-group-item', 'echo_list_group_item' );
function echo_list_group_item($atts, $content = null) {
	extract(shortcode_atts(array(
		"id" => '',
		"class" => '',
		"lg" => '',
		"md" => '',
		"sm" => '',
		"xs" => '',
		"active" => '',
		"title" => '',
		"type_lg" => '',
		), $atts));
	if ($id!='') {$id = " id=\"".$id."\"";}
	if ($title!='') {$title = '<h4 class="list-group-item-heading">'.$title.'</h4>';}
	if ($active=='yes') {$class.=' active';}
	if ($lg=='visible') {$class.=' visible-lg';} elseif ($lg=='hidden') {$class.=' hidden-lg';}
	if ($md=='visible') {$class.=' visible-md';} elseif ($md=='hidden') {$class.=' hidden-md';}
	if ($sm=='visible') {$class.=' visible-sm';} elseif ($sm=='hidden') {$class.=' hidden-sm';}
	if ($xs=='visible') {$class.=' visible-xs';} elseif ($xs=='hidden') {$class.=' hidden-xs';}
	if ($type_lg=='linked') {$type_lg='a';} else {$type_lg='li';}
	$output = '<'.$type_lg.' class="list-group-item '.$class.'">'.$title.do_shortcode($content).'</'.$type_lg.'>';
	return $output;
}



add_shortcode( 'def-list', 'echo_deflist' );
function echo_deflist($atts, $content = null) {
	extract(shortcode_atts(array(
		"id" => '',
		"class" => '',
		"lg" => '',
		"md" => '',
		"sm" => '',
		"xs" => '',
		"horizontal" => '',
		), $atts));
	if ($id!='') {$id = " id=\"".$id."\"";}
	if ($horizontal=='yes') {$class.=' dl-horizontal';}
	if ($lg=='visible') {$class.=' visible-lg';} elseif ($lg=='hidden') {$class.=' hidden-lg';}
	if ($md=='visible') {$class.=' visible-md';} elseif ($md=='hidden') {$class.=' hidden-md';}
	if ($sm=='visible') {$class.=' visible-sm';} elseif ($sm=='hidden') {$class.=' hidden-sm';}
	if ($xs=='visible') {$class.=' visible-xs';} elseif ($xs=='hidden') {$class.=' hidden-xs';}
	$output = '<dl class="'.$class.'">'.do_shortcode($content).'</dl>';
	return $output;
}

add_shortcode( 'def-item', 'echo_defitem' );
function echo_defitem($atts, $content = null) {
	extract(shortcode_atts(array(
		"id" => '',
		"class" => '',
		"lg" => '',
		"md" => '',
		"sm" => '',
		"xs" => '',
		"title" => '',
		), $atts));
	if ($id!='') {$id = " id=\"".$id."\"";}
	if ($lg=='visible') {$class.=' visible-lg';} elseif ($lg=='hidden') {$class.=' hidden-lg';}
	if ($md=='visible') {$class.=' visible-md';} elseif ($md=='hidden') {$class.=' hidden-md';}
	if ($sm=='visible') {$class.=' visible-sm';} elseif ($sm=='hidden') {$class.=' hidden-sm';}
	if ($xs=='visible') {$class.=' visible-xs';} elseif ($xs=='hidden') {$class.=' hidden-xs';}
	$output = '<dt>'.$title.'</dt><dd class="'.$class.'">'.do_shortcode($content).'</dd>';
	return $output;
}

add_shortcode( 'p-code', 'echo_pcode' );
function echo_pcode($atts, $content = null) {
	extract(shortcode_atts(array(
		"id" => '',
		"class" => '',
		"lg" => '',
		"md" => '',
		"sm" => '',
		"xs" => '',
		"block" => '',
		), $atts));
	if ($id!='') {$id = " id=\"".$id."\"";}
	if ($block=='yes') {$type='pre';} else {$type='code';}
	if ($lg=='visible') {$class.=' visible-lg';} elseif ($lg=='hidden') {$class.=' hidden-lg';}
	if ($md=='visible') {$class.=' visible-md';} elseif ($md=='hidden') {$class.=' hidden-md';}
	if ($sm=='visible') {$class.=' visible-sm';} elseif ($sm=='hidden') {$class.=' hidden-sm';}
	if ($xs=='visible') {$class.=' visible-xs';} elseif ($xs=='hidden') {$class.=' hidden-xs';}
	$output = '<'.$type.' class="'.$class.'">'.do_shortcode($content).'</'.$type.'>';
	return $output;
}



add_shortcode( 'panel', 'echo_panel' );
function echo_panel($atts, $content = null) {
	extract(shortcode_atts(array(
		"id" => '',
		"class" => '',
		"lg" => '',
		"md" => '',
		"sm" => '',
		"xs" => '',
		"header" => '',
		"footer" => '',
		"color" => '',
		), $atts));
	if ($id!='') {$id = " id=\"".$id."\"";}
	if ($lg=='visible') {$class.=' visible-lg';} elseif ($lg=='hidden') {$class.=' hidden-lg';}
	if ($md=='visible') {$class.=' visible-md';} elseif ($md=='hidden') {$class.=' hidden-md';}
	if ($sm=='visible') {$class.=' visible-sm';} elseif ($sm=='hidden') {$class.=' hidden-sm';}
	if ($xs=='visible') {$class.=' visible-xs';} elseif ($xs=='hidden') {$class.=' hidden-xs';}
	if ($color!='') {$class.=' panel-'.$color;} else {$class.=' panel-default';}
	if ($header!='') {$header = '<div class="panel-heading"><h3 class="panel-title">'.$header.'</h3></div>';}
	if ($footer!='') {$footer = '<div class="panel-footer">'.$footer.'</div>';}
	$output = '<div class="panel'.$class.'">'.$header.'<div class="panel-body">'.do_shortcode($content).'</div>'.$footer.'</div>';
	return $output;
}




/**
 *
 * TABLES
 */

add_shortcode( 'table', 'echo_table' );
function echo_table($atts, $content = null) {
	extract(shortcode_atts(array(
		"id" => '',
		"class" => '',
		"lg" => '',
		"md" => '',
		"sm" => '',
		"xs" => '',
		"striped" => '',
		"bordered" => '',
		"hover" => '',
		"condensed" => '',
		"caption" => '',
		), $atts));
	if ($id!='') {$id = " id=\"".$id."\"";}
	if ($caption!='') {$caption = '<caption>'.$caption.'</caption>';}
	if ($striped=='yes') {$class.=' table-striped';}
	if ($bordered=='yes') {$class.=' table-bordered';}
	if ($hover=='yes') {$class.=' table-hover';}
	if ($condensed=='yes') {$class.=' table-condensed';}
	if ($lg=='visible') {$class.=' visible-lg';} elseif ($lg=='hidden') {$class.=' hidden-lg';}
	if ($md=='visible') {$class.=' visible-md';} elseif ($md=='hidden') {$class.=' hidden-md';}
	if ($sm=='visible') {$class.=' visible-sm';} elseif ($sm=='hidden') {$class.=' hidden-sm';}
	if ($xs=='visible') {$class.=' visible-xs';} elseif ($xs=='hidden') {$class.=' hidden-xs';}
	$output = '<div class="table-responsive"><table class="table '.$class.'">'.$caption.do_shortcode($content).'</table></div>';
	return $output;
}

add_shortcode( 'thead', 'echo_thead' );
function echo_thead($atts, $content = null) {
	extract(shortcode_atts(array(
		"id" => '',
		"class" => '',
		"lg" => '',
		"md" => '',
		"sm" => '',
		"xs" => '',
		"striped" => '',
		"bordered" => '',
		"hover" => '',
		"condensed" => '',
		), $atts));
	if ($id!='') {$id = " id=\"".$id."\"";}
	if ($lg=='visible') {$class.=' visible-lg';} elseif ($lg=='hidden') {$class.=' hidden-lg';}
	if ($md=='visible') {$class.=' visible-md';} elseif ($md=='hidden') {$class.=' hidden-md';}
	if ($sm=='visible') {$class.=' visible-sm';} elseif ($sm=='hidden') {$class.=' hidden-sm';}
	if ($xs=='visible') {$class.=' visible-xs';} elseif ($xs=='hidden') {$class.=' hidden-xs';}
	$output = '<thead class="'.$class.'"><tr>'.do_shortcode($content).'</tr></thead>';
	return $output;
}

add_shortcode( 'thcell', 'echo_thcell' );
function echo_thcell($atts, $content = null) {
	extract(shortcode_atts(array(
		"id" => '',
		"class" => '',
		"lg" => '',
		"md" => '',
		"sm" => '',
		"xs" => '',
		"striped" => '',
		"bordered" => '',
		"hover" => '',
		"condensed" => '',
		), $atts));
	if ($id!='') {$id = " id=\"".$id."\"";}
	if ($lg=='visible') {$class.=' visible-lg';} elseif ($lg=='hidden') {$class.=' hidden-lg';}
	if ($md=='visible') {$class.=' visible-md';} elseif ($md=='hidden') {$class.=' hidden-md';}
	if ($sm=='visible') {$class.=' visible-sm';} elseif ($sm=='hidden') {$class.=' hidden-sm';}
	if ($xs=='visible') {$class.=' visible-xs';} elseif ($xs=='hidden') {$class.=' hidden-xs';}
	$output = '<th class="'.$class.'">'.do_shortcode($content).'</th>';
	return $output;
}

add_shortcode( 'tbody', 'echo_tbody' );
function echo_tbody($atts, $content = null) {
	$output = '<tbody>'.do_shortcode($content).'</tbody>';
	return $output;
}

add_shortcode( 'tr', 'echo_tr' );
function echo_tr($atts, $content = null) {
	extract(shortcode_atts(array(
		"id" => '',
		"class" => '',
		"lg" => '',
		"md" => '',
		"sm" => '',
		"xs" => '',
		"type" => '',
		"rowspan" => '',
		), $atts));
	if ($id!='') {$id = " id=\"".$id."\"";}
	if ($rowspan!='') {$rowspan = " rowspan=\"".$rowspan."\"";}
	if ($type!='') {$class.=' '.$type;}
	if ($lg=='visible') {$class.=' visible-lg';} elseif ($lg=='hidden') {$class.=' hidden-lg';}
	if ($md=='visible') {$class.=' visible-md';} elseif ($md=='hidden') {$class.=' hidden-md';}
	if ($sm=='visible') {$class.=' visible-sm';} elseif ($sm=='hidden') {$class.=' hidden-sm';}
	if ($xs=='visible') {$class.=' visible-xs';} elseif ($xs=='hidden') {$class.=' hidden-xs';}
	$output = '<tr class="'.$class.'"'.$rowspan.'>'.do_shortcode($content).'</tr>';
	return $output;
}

add_shortcode( 'td', 'echo_td' );
function echo_td($atts, $content = null) {
	extract(shortcode_atts(array(
		"id" => '',
		"class" => '',
		"colspan" => '',
		"type" => '',
		), $atts));
	if ($id!='') {$id = " id=\"".$id."\"";}
	if ($type!='') {$class.=' '.$type;}
	if ($colspan!='') {$colspan = " colspan=\"".$colspan."\"";}
	$output = '<td class="'.$class.'"'.$colspan.'>'.do_shortcode($content).'</td>';
	return $output;
}






/**
 * SEARCH
 */

add_shortcode('p-search', 'echo_search');
function echo_search( ) {
	ob_start();
	get_search_form(true);
	$ret = ob_get_contents();
	ob_end_clean();
	return $ret;
}


add_shortcode('wpbsearch', 'get_search_form');







/**
 * BUTTONS
 */

add_shortcode( 'peadig-button', 'echo_peadig_button' );
function echo_peadig_button($atts, $content = null) {
	extract(shortcode_atts(array(
		"id" => '',
		"class" => '',
		"lg" => '',
		"md" => '',
		"sm" => '',
		"xs" => '',
		"type" => '',
		"link" => '',
		"size" => '',
		"block" => '',
		"disabled" => '',
		), $atts));
	if ($id!='') {$id = " id=\"".$id."\"";}
	if ($type!='') {$class .= ' btn-'.$type;} else {$class .= ' btn-default';}
	if ($size!='') {
		if ($size=='btn-lg') {$class .= ' btn-lg';}
		if ($size=='btn-sm') {$class .= ' btn-sm';}
		if ($size=='btn-xs') {$class .= ' btn-xs';}
	}
	if ($block=='yes') {$class .= ' btn-block';}
	if ($lg=='visible') {$class.=' visible-lg';} elseif ($lg=='hidden') {$class.=' hidden-lg';}
	if ($md=='visible') {$class.=' visible-md';} elseif ($md=='hidden') {$class.=' hidden-md';}
	if ($sm=='visible') {$class.=' visible-sm';} elseif ($sm=='hidden') {$class.=' hidden-sm';}
	if ($xs=='visible') {$class.=' visible-xs';} elseif ($xs=='hidden') {$class.=' hidden-xs';}
	if ($disabled=='yes') {$disabled = " disabled";}
	if ($link!='') {
		$shortcode = '<a href="'.$link.'"'.$id.' class="btn '.$class.'"'.$disabled.'>'.do_shortcode($content).'</a>';
	} else {
		$shortcode = '<button'.$id.' class="btn '.$class.'"'.$disabled.'>'.do_shortcode($content).'</button>';
	}
	return $shortcode;
}

add_shortcode( 'button-group', 'echo_peadig_button_group' );
function echo_peadig_button_group($atts, $content = null) {
	extract(shortcode_atts(array(
		"id" => '',
		"class" => '',
		"size" => '',
		"lg" => '',
		"md" => '',
		"sm" => '',
		"xs" => '',
		"vertical" => '',
		"justified" => '',
		), $atts));
	if ($id!='') {$id = " id=\"".$id."\"";}
	if ($vertical=='yes') {$class .= "btn-group-vertical";} else {$class .= "btn-group";}
	if ($justified=='yes') {$class .= " btn-group-justified";}
	if ($size!='') {
		if ($size=='large') {$class .= ' btn-group-lg';}
		if ($size=='small') {$class .= ' btn-group-sm';}
		if ($size=='extra small') {$class .= ' btn-group-xs';}
	}
	if ($lg=='visible') {$class.=' visible-lg';} elseif ($lg=='hidden') {$class.=' hidden-lg';}
	if ($md=='visible') {$class.=' visible-md';} elseif ($md=='hidden') {$class.=' hidden-md';}
	if ($sm=='visible') {$class.=' visible-sm';} elseif ($sm=='hidden') {$class.=' hidden-sm';}
	if ($xs=='visible') {$class.=' visible-xs';} elseif ($xs=='hidden') {$class.=' hidden-xs';}
	$shortcode = '<div'.$id.' class="'.$class.'">'.do_shortcode($content).'</div>';
	return $shortcode;
}

add_shortcode( 'button-toolbar', 'echo_peadig_button_toolbar' );
function echo_peadig_button_toolbar($atts, $content = null) {
	extract(shortcode_atts(array(
		"id" => '',
		"class" => '',
		"lg" => '',
		"md" => '',
		"sm" => '',
		"xs" => '',
		"vertical" => '',
		), $atts));
	if ($id!='') {$id = " id=\"".$id."\"";}
	if ($vertical=='yes') {$class .= " btn-group-vertical";}
	if ($lg=='visible') {$class.=' visible-lg';} elseif ($lg=='hidden') {$class.=' hidden-lg';}
	if ($md=='visible') {$class.=' visible-md';} elseif ($md=='hidden') {$class.=' hidden-md';}
	if ($sm=='visible') {$class.=' visible-sm';} elseif ($sm=='hidden') {$class.=' hidden-sm';}
	if ($xs=='visible') {$class.=' visible-xs';} elseif ($xs=='hidden') {$class.=' hidden-xs';}
	$shortcode = '<div'.$id.' class="btn-toolbar '.$class.'">'.do_shortcode($content).'</div>';
	return $shortcode;
}









/**
 * ICONS
 */

add_shortcode( 'peadig-i', 'echo_peadig_icon' );
function echo_peadig_icon($atts) {
	extract(shortcode_atts(array(
		"id" => '',
		"class" => '',
		"size" => '',
		"lg" => '',
		"md" => '',
		"sm" => '',
		"xs" => '',
		"type" => '',
		), $atts));
	if ($id!='') {$id=" id=\"".$id."\"";}
	if ($lg=='visible') {$class.=' visible-lg';} elseif ($lg=='hidden') {$class.=' hidden-lg';}
	if ($md=='visible') {$class.=' visible-md';} elseif ($md=='hidden') {$class.=' hidden-md';}
	if ($sm=='visible') {$class.=' visible-sm';} elseif ($sm=='hidden') {$class.=' hidden-sm';}
	if ($xs=='visible') {$class.=' visible-xs';} elseif ($xs=='hidden') {$class.=' hidden-xs';}
	if ($size!='') {$size=' style="font-size:'.$size.';"';}
	$shortcode = "<span".$id." class=\"glyphicon glyphicon-".$type. " " . $class."\"".$size."></span>";
	return $shortcode;
}

add_shortcode( 'fontawesome', 'echo_peadig_fontawesome' );
function echo_peadig_fontawesome($atts) {
	extract(shortcode_atts(array(
		"id" => '',
		"class" => '',
		"size" => '',
		"lg" => '',
		"md" => '',
		"sm" => '',
		"xs" => '',
		"type" => '',
		), $atts));
	if ($id!='') {$id=" id=\"".$id."\"";}
	if ($lg=='visible') {$class.=' visible-lg';} elseif ($lg=='hidden') {$class.=' hidden-lg';}
	if ($md=='visible') {$class.=' visible-md';} elseif ($md=='hidden') {$class.=' hidden-md';}
	if ($sm=='visible') {$class.=' visible-sm';} elseif ($sm=='hidden') {$class.=' hidden-sm';}
	if ($xs=='visible') {$class.=' visible-xs';} elseif ($xs=='hidden') {$class.=' hidden-xs';}
	if ($size!='') {$size=' style="font-size:'.$size.';"';}
	$shortcode = "<i".$id." class=\"fa fa-".$type. " " . $class."\"".$size."></i>";
	return $shortcode;
}







/**
 * LABELS
 */

add_shortcode( 'peadig-label', 'echo_peadig_label' );
function echo_peadig_label($atts, $content = null) {
	extract(shortcode_atts(array(
		"id" => '',
		"class" => '',
		"type" => '',
		), $atts));
	if ($id!='') {$id=" id=\"".$id."\"";}
	if ($type=='badge') {$class.=' badge ';} elseif ($type!='') {$class.=' label label-'.$type;} else {$class.='label label-default';}
	$shortcode = "<span".$id." class=\"".$class."\">".do_shortcode($content)."</span>";
	return $shortcode;
}




/**
 * ALERTS
 */


add_shortcode( 'peadig-alert', 'echo_peadig_alert' );
function echo_peadig_alert($atts, $content = null) {
	extract(shortcode_atts(array(
		"id" => '',
		"class" => '',
		"lg" => '',
		"md" => '',
		"sm" => '',
		"xs" => '',
		"type" => '',
		"block" => '',
		"dismissbutton" => '',
		), $atts));
	if ($id!='') {$id=" id=\"".$id."\"";}
	if ($type!='') {$class.=' alert-'.$type;} else {$class.=' alert-info';}
	if ($block=='yes') {$class.=' alert-block';}
	if ($dismissbutton!='no') {$dismiss="<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>";}
	if ($lg=='visible') {$class.=' visible-lg';} elseif ($lg=='hidden') {$class.=' hidden-lg';}
	if ($md=='visible') {$class.=' visible-md';} elseif ($md=='hidden') {$class.=' hidden-md';}
	if ($sm=='visible') {$class.=' visible-sm';} elseif ($sm=='hidden') {$class.=' hidden-sm';}
	if ($xs=='visible') {$class.=' visible-xs';} elseif ($xs=='hidden') {$class.=' hidden-xs';}
	$shortcode = "<div".$id." class=\"alert alert-dismissable ".$class." fade in\">".$dismiss.do_shortcode($content)."</div>";
	return $shortcode;
}






/**
 * PROGRESS BARS
 */


add_shortcode( 'progress-bar', 'echo_peadig_progress' );
function echo_peadig_progress($atts, $content = null) {
	extract(shortcode_atts(array(
		"id" => '',
		"class" => '',
		"lg" => '',
		"md" => '',
		"sm" => '',
		"xs" => '',
		"type" => '',
		"striped" => '',
		"animated" => '',
		"amount" => '',
		), $atts));
	if ($id!='') {$id=" id=\"".$id."\"";}
	if ($type!='') {$color=' progress-bar-'.$type;}
	if ($striped=='yes') {
		$class.=' progress-striped';
		if ($animated=='yes') {$class.=' active';}
	}
	if ($lg=='visible') {$class.=' visible-lg';} elseif ($lg=='hidden') {$class.=' hidden-lg';}
	if ($md=='visible') {$class.=' visible-md';} elseif ($md=='hidden') {$class.=' hidden-md';}
	if ($sm=='visible') {$class.=' visible-sm';} elseif ($sm=='hidden') {$class.=' hidden-sm';}
	if ($xs=='visible') {$class.=' visible-xs';} elseif ($xs=='hidden') {$class.=' hidden-xs';}
	$shortcode = '<div'.$id.' class="progress'.$class.'"><div class="progress-bar'.$color.'"  role="progressbar" aria-valuenow="'.$amount.'" aria-valuemin="0" aria-valuemax="100" style="width: '.$amount.'%;"></div></div>';
	return $shortcode;
}





/**
 * TABBED CONTENT
 */

add_shortcode( 'tab-headings', 'echo_peadig_tab_headings' );
function echo_peadig_tab_headings($atts, $content = null) {
	extract(shortcode_atts(array(
		"id" => '',
		"class" => '',
		"lg" => '',
		"md" => '',
		"sm" => '',
		"xs" => '',
		), $atts));
	if ($lg=='visible') {$class.=' visible-lg';} elseif ($lg=='hidden') {$class.=' hidden-lg';}
	if ($md=='visible') {$class.=' visible-md';} elseif ($md=='hidden') {$class.=' hidden-md';}
	if ($sm=='visible') {$class.=' visible-sm';} elseif ($sm=='hidden') {$class.=' hidden-sm';}
	if ($xs=='visible') {$class.=' visible-xs';} elseif ($xs=='hidden') {$class.=' hidden-xs';}
	$shortcode = '<ul id="'.$id.'" class="nav nav-tabs">'.do_shortcode($content).'</ul>';
	return $shortcode;
}

add_shortcode( 'tab-title', 'echo_peadig_tab_title' );
function echo_peadig_tab_title($atts) {
	extract(shortcode_atts(array(
		"id" => '',
		"class" => '',
		"lg" => '',
		"md" => '',
		"sm" => '',
		"xs" => '',
		"title" => '',
		"active" => '',
		), $atts));
	if ($active=='yes') {$class.=' class="active"';}
	$shortcode = '<li'.$class.' ><a href="#'.$id.'" data-toggle="tab">'.$title.'</a></li>';
	return $shortcode;
}

add_shortcode( 'tabs', 'echo_peadig_tabs' );
function echo_peadig_tabs($atts, $content = null) {
	$shortcode = '<div id="'.$id.'Content" class="tab-content">'.do_shortcode($content).'</div>';
	return $shortcode;
}

add_shortcode( 'tab-pane', 'echo_peadig_tab_pane' );
function echo_peadig_tab_pane($atts, $content = null) {
	extract(shortcode_atts(array(
		"id" => '',
		"active" => '',
		), $atts));
	if ($active=='yes') {$active=' in active';}
	$shortcode = '<div class="tab-pane fade'.$active.'" id="'.$id.'" data-toggle="tab">'.do_shortcode($content).'</div>';
	return $shortcode;
}







/**
 * MODAL/POPUP
 */

add_shortcode( 'modal-launch', 'echo_peadig_modal_launch' );
function echo_peadig_modal_launch($atts, $content = null) {
	extract(shortcode_atts(array(
		"id" => '',
		"text" => 'Click to open popup',
		"class" => '',
		"lg" => '',
		"md" => '',
		"sm" => '',
		"xs" => '',
		), $atts));
	if ($lg=='visible') {$class.=' visible-lg';} elseif ($lg=='hidden') {$class.=' hidden-lg';}
	if ($md=='visible') {$class.=' visible-md';} elseif ($md=='hidden') {$class.=' hidden-md';}
	if ($sm=='visible') {$class.=' visible-sm';} elseif ($sm=='hidden') {$class.=' hidden-sm';}
	if ($xs=='visible') {$class.=' visible-xs';} elseif ($xs=='hidden') {$class.=' hidden-xs';}
	$shortcode = '<a href="#'.$id.'" data-toggle="modal">'.$text.'</a>';
	return $shortcode;
}

add_shortcode( 'modal', 'echo_peadig_tab_modal' );
function echo_peadig_tab_modal($atts, $content = null) {
	extract(shortcode_atts(array(
		"id" => '',
		"title" => '',
		"closebutton" => '',
		"closetext" => 'Close',
		"xbutton" => '',
		"class" => '',
		"lg" => '',
		"md" => '',
		"sm" => '',
		"xs" => '',
		), $atts));
	if ($lg=='visible') {$class.=' visible-lg';} elseif ($lg=='hidden') {$class.=' hidden-lg';}
	if ($md=='visible') {$class.=' visible-md';} elseif ($md=='hidden') {$class.=' hidden-md';}
	if ($sm=='visible') {$class.=' visible-sm';} elseif ($sm=='hidden') {$class.=' hidden-sm';}
	if ($xs=='visible') {$class.=' visible-xs';} elseif ($xs=='hidden') {$class.=' hidden-xs';}
	$shortcode = '<div id="'.$id.'" class="modal fade'.$class.'" tabindex="-1" role="dialog" aria-labelledby="'.$id.'Label" aria-hidden="true"><div class="modal-dialog"><div class="modal-content"><div class="modal-header">';
	if ($xbutton!='no') {$shortcode .= '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';}
	$shortcode .= '<h4 id="'.$id.'Label" class="modal-title">'.$title.'</h4></div><div class="modal-body">'.do_shortcode($content).'</div>';
	if ($closebutton!='no') {$shortcode .= '<div class="modal-footer"><button class="btn btn-default" data-dismiss="modal">'.$closetext.'</button></div></div></div>';}
	$shortcode .= '</div>';
	return $shortcode;
}






/**
 * ACCORDIAN
 */

add_shortcode( 'accordian', 'echo_peadig_accordian' );
function echo_peadig_accordian($atts, $content = null) {
	extract(shortcode_atts(array(
		"id" => '',
		"class" => '',
		"lg" => '',
		"md" => '',
		"sm" => '',
		"xs" => '',
		), $atts));
	if ($lg=='visible') {$class.=' visible-lg';} elseif ($lg=='hidden') {$class.=' hidden-lg';}
	if ($md=='visible') {$class.=' visible-md';} elseif ($md=='hidden') {$class.=' hidden-md';}
	if ($sm=='visible') {$class.=' visible-sm';} elseif ($sm=='hidden') {$class.=' hidden-sm';}
	if ($xs=='visible') {$class.=' visible-xs';} elseif ($xs=='hidden') {$class.=' hidden-xs';}
	$shortcode = '<div class="panel-group '.$class.'" id="'.$id.'">'.do_shortcode($content).'</div>';
	return $shortcode;
}

add_shortcode( 'accordian-item', 'echo_peadig_accordian_item' );
function echo_peadig_accordian_item($atts, $content = null) {
	extract(shortcode_atts(array(
		"id" => '',
		"class" => '',
		"accordianid" => '',
		"title" => '',
		"active" => '',
		), $atts));
	if ($active=='yes') {$act=' in';}
	$shortcode = '<div class="panel panel-default"><div class="panel-heading"><h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#'.$accordianid.'" href="#'.$id.'">'.$title.'</a></h4></div><div id="'.$id.'" class="panel-collapse collapse'.$act.' '.$class.'"><div class="panel-body">'.do_shortcode($content).'</div></div></div>';
	return $shortcode;
}

/**
 * TOOLTIP & POPOVER
 */

add_shortcode( 'tooltip', 'echo_peadig_tooltip' );
function echo_peadig_tooltip($atts, $content = null) {
	extract(shortcode_atts(array(
		"title" => '',
		"placement" => 'top',
		"allowhtml" => '',
		"class" => '',
		), $atts));
	if ($allowhtml=='yes') {$html=' data-html="true"';}
	if ($html=='yes') {$usehtml=' data-html="true"';}
	$shortcode = '<a href="#" data-toggle="tooltip" data-placement="'.$placement.'"'.$html.' title="'.$title.'" class="'.$class.'" rel="tooltip">'.do_shortcode($content).'</a>';
	return $shortcode;
}


add_shortcode( 'popover', 'echo_peadig_popover' );
function echo_peadig_popover($atts, $content = null) {
	extract(shortcode_atts(array(
		"title" => '',
		"placement" => 'top',
		"allowhtml" => '',
		"class" => '',
		"text" => '',
		"trigger" => 'click',
		), $atts));
	if ($allowhtml=='yes') {$html=' data-html="true"';}
	if ($trigger!='click') {$trig=' data-trigger="'.$trigger.'"';}
	$shortcode = '<a href="#" data-toggle="popover" data-placement="'.$placement.'"'.$html.$trig.' data-content="'.do_shortcode($content).'" data-original-title="Popover on top" class="'.$class.'" rel="popover">'.$text.'</a>';
	return $shortcode;
}


/**
 * OVERRIDE DEFAULT GALLERY
 */

add_filter( 'post_gallery', 'my_post_gallery', 10, 2 );
function my_post_gallery( $output, $attr) {
	$post = get_post();

	static $instance = 0;
	$instance++;

	if ( ! empty( $attr['ids'] ) ) {
		// 'ids' is explicitly ordered, unless you specify otherwise.
		if ( empty( $attr['orderby'] ) ) {
			$attr['orderby'] = 'post__in';
		}
		$attr['include'] = $attr['ids'];
	}

	/**
	 * Filter the default gallery shortcode output.
	 *
	 * If the filtered output isn't empty, it will be used instead of generating
	 * the default gallery template.
	 *
	 * @since 2.5.0
	 * @since 4.2.0 The `$instance` parameter was added.
	 *
	 * @see gallery_shortcode()
	 *
	 * @param string $output   The gallery output. Default empty.
	 * @param array  $attr     Attributes of the gallery shortcode.
	 * @param int    $instance Unique numeric ID of this gallery shortcode instance.
	 */
	//$output = apply_filters( 'post_gallery', '', $attr, $instance );
	if ( $output != '' ) {
		return $output;
	}

	$html5 = current_theme_supports( 'html5', 'gallery' );
	$atts = shortcode_atts( array(
		'order'      => 'ASC',
		'orderby'    => 'menu_order ID',
		'id'         => $post ? $post->ID : 0,
		'itemtag'    => $html5 ? 'figure'     : 'dl',
		'icontag'    => $html5 ? 'div'        : 'dt',
		'captiontag' => $html5 ? 'figcaption' : 'dd',
		'columns'    => 3,
		'size'       => 'thumbnail',
		'include'    => '',
		'exclude'    => '',
		'link'       => ''
	), $attr, 'gallery' );

	$id = intval( $atts['id'] );

	if ( ! empty( $atts['include'] ) ) {
		$_attachments = get_posts( array( 'include' => $atts['include'], 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );

		$attachments = array();
		foreach ( $_attachments as $key => $val ) {
			$attachments[$val->ID] = $_attachments[$key];
		}
	} elseif ( ! empty( $atts['exclude'] ) ) {
		$attachments = get_children( array( 'post_parent' => $id, 'exclude' => $atts['exclude'], 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );
	} else {
		$attachments = get_children( array( 'post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $atts['order'], 'orderby' => $atts['orderby'] ) );
	}

	if ( empty( $attachments ) ) {
		return '';
	}

	if ( is_feed() ) {
		$output = "\n";
		foreach ( $attachments as $att_id => $attachment ) {
			$output .= wp_get_attachment_link( $att_id, $atts['size'], true ) . "\n";
		}
		return $output;
	}

	$itemtag = tag_escape( $atts['itemtag'] );
	$captiontag = tag_escape( $atts['captiontag'] );
	$icontag = tag_escape( $atts['icontag'] );
	$valid_tags = wp_kses_allowed_html( 'post' );
	if ( ! isset( $valid_tags[ $itemtag ] ) ) {
		$itemtag = 'dl';
	}
	if ( ! isset( $valid_tags[ $captiontag ] ) ) {
		$captiontag = 'dd';
	}
	if ( ! isset( $valid_tags[ $icontag ] ) ) {
		$icontag = 'dt';
	}

	$columns = intval( $atts['columns'] );
	$itemwidth = $columns > 0 ? floor(100/$columns) : 100;
	$float = is_rtl() ? 'right' : 'left';

	$selector = "gallery-{$instance}";

	$gallery_style = '';

	$size_class = sanitize_html_class( $atts['size'] );
	
if ( apply_filters( 'use_default_gallery_style', ! $html5 ) ) {
		$gallery_style = "
		<style type='text/css'>
			#{$selector} {
				margin: auto;
			}
			#{$selector} .gallery-item {
				float: {$float};
				margin-top: 10px;
				text-align: center;
				width: {$itemwidth}%;
			}
			#{$selector} img {
				border: 2px solid #cfcfcf;
			}
			#{$selector} .gallery-caption {
				margin-left: 0;
			}
			/* see gallery_shortcode() in wp-includes/media.php */
		</style>\n\t\t";
	}


	if ($columns == '1' || $columns == '0') {
    $i = 0;
	$gallery_div = "<div id='{$selector}' class='gallery galleryid-{$id}' data-ride='carousel' data-interval='false' data-pause='hover'>
      <div class='carousel-inner' role='listbox'>";
      $output = apply_filters( 'gallery_style', $gallery_style . $gallery_div );
    foreach ( $attachments as $id => $attachment ) {
    	$attr = ( trim( $attachment->post_excerpt ) ) ? array( 'aria-describedby' => "$selector-$id" ) : '';
		if ( ! empty( $atts['link'] ) && 'file' === $atts['link'] ) {
			$image_output = wp_get_attachment_link( $id, $atts['size'], false, false, false, $attr );
		} elseif ( ! empty( $atts['link'] ) && 'none' === $atts['link'] ) {
			$image_output = wp_get_attachment_image( $id, $atts['size'], false, $attr );
		} else {
			$image_output = wp_get_attachment_link( $id, $atts['size'], true, false, false, $attr );
		}
		$image_meta  = wp_get_attachment_metadata( $id );

		$orientation = '';
		if ( isset( $image_meta['height'], $image_meta['width'] ) ) {
			$orientation = ( $image_meta['height'] > $image_meta['width'] ) ? 'portrait' : 'landscape';
		}
        $int++;
		if ($int!='1') {$add='';} else {$add=' active';}
		$output .= "<div class='item".$add."'>$image_output</div>";
    }
    $output .= "</div><a class='left carousel-control' href='#{$selector}' role='button' data-slide='prev'>
        <span class='glyphicon glyphicon-chevron-left' aria-hidden='true'></span>
        <span class='sr-only'>Previous</span>
      </a><a class='right carousel-control' href='#{$selector}' role='button' data-slide='next'>
        <span class='glyphicon glyphicon-chevron-right' aria-hidden='true'></span>
        <span class='sr-only'>Next</span>
      </a>
    </div>";

    return $output;
} else {
		$i = 0;
		$gallery_div = "<div id='{$selector}'><div class='row'>";
		$output = apply_filters( 'gallery_style', $gallery_style . $gallery_div );
		if ($columns>'4') {$span="col-sm-2 col-xs-6";}
		elseif ($columns=='4') {$span="col-sm-3 col-xs-6";}
		elseif ($columns=='3') {$span="col-sm-4 col-xs-6";}
		else {$span="col-xs-6";}
		foreach ( $attachments as $id => $attachment ) {
			$attr = ( trim( $attachment->post_excerpt ) ) ? array( 'aria-describedby' => "$selector-$id" ) : '';
		if ( ! empty( $atts['link'] ) && 'file' === $atts['link'] ) {
			$image_output = wp_get_attachment_link( $id, $atts['size'], false, false, false, $attr );
		} elseif ( ! empty( $atts['link'] ) && 'none' === $atts['link'] ) {
			$image_output = wp_get_attachment_image( $id, $atts['size'], false, $attr );
		} else {
			$image_output = wp_get_attachment_link( $id, $atts['size'], true, false, false, $attr );
		}
		$image_meta  = wp_get_attachment_metadata( $id );

		$orientation = '';
		if ( isset( $image_meta['height'], $image_meta['width'] ) ) {
			$orientation = ( $image_meta['height'] > $image_meta['width'] ) ? 'portrait' : 'landscape';
		}
			$output .="<div class='$span {$orientation}'>
			<div class='thumbnail'>$image_output</div>
			</div>";
			if ( $columns > 0 && ++$i % $columns == 0 )
            $output .= '</div><div class="row">';

		}
		$output .= "</div></div>";

		return $output;
	}



}

?>