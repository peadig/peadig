<?php

/*
 * All the functions for the admin side of WordPress live here
 *
 ********************************************************************************/







/*
 * Adds the tinyMCE button to the editor
 *
 */

function pd_add_tinymce_plugin($plugin_array) {
    $plugin_array['peadig_button'] = get_template_directory_uri().'/js/mce_peadig_button.js';
    return $plugin_array;
}

function add_peadig_button() {
    if ( ! current_user_can('edit_posts') && ! current_user_can('edit_pages') )
        return;
    if ( get_user_option('rich_editing') == 'true') {
        add_filter('mce_external_plugins', 'pd_add_tinymce_plugin');
        add_filter('mce_buttons', 'pd_register_button');
    }
}

function pd_register_button($buttons) {
    array_push($buttons, "|", "peadig_button");
    return $buttons;
}

add_action('init', 'add_peadig_button');




/*
 * Checks if a menu is assigned, if not show a message in the admin panel
 * TODO auto assign the menu on activation of theme
 */

function pd_primary_check_menu() {
    $theme_mods = get_theme_mods();

    if(empty($theme_mods['nav_menu_locations'])){
        add_action('admin_notices', 'no_menu_message');
    }

}

function no_menu_message(){
        // Shows as an error message. You could add a link to the right page if you wanted.
    echo '<div id="message" class="error"><p>';
    echo '<strong>Theme Menu Issue</strong> - You do not have a menu assigned. Ensure you select your menu\'s <strong>Theme Location</strong> from your <a href="nav-menus.php?action=edit"> Menu Settings</a>';
    echo '</p></div>';
}

add_action('init', 'pd_primary_check_menu');








/*
 * Peadig favicon for the admin
 *
 */


function pd_admin_area_favicon() {
    $pd_options = get_option('peadig'); ?>

    <link rel="shortcut icon" href="<?php
    if(empty($pd_options['favicon']['url'])){
        echo get_template_directory_uri().'/img/favicon.png';
    } else {
        echo $pd_options['favicon']['url'];
    }
    ?>">
    <?php
}

add_action('admin_head', 'pd_admin_area_favicon');









/*
 * Adds additional fields to the autor bio area
 *
 */

function pd_social_profiles( $contactmethods ) {
    $contactmethods['googleplus']   = 'Google+ Profile URL';
    $contactmethods['twitter']      = 'Twitter username (without @)';
    $contactmethods['facebook']     = 'Facebook Profile URL';
    $contactmethods['linkedin']     = 'Linkedin Profile URL';
    unset($contactmethods['yim']);
    unset($contactmethods['aim']);
    unset($contactmethods['jabber']);
    return $contactmethods;
}
add_filter( 'user_contactmethods', 'pd_social_profiles', 10, 1);





/*
 * Adds Peadig attribute to WordPress backend
 *
 */

function footer_admin () {
    echo '&copy; '.date("Y").' - Powered by <a href="http://wordpress.org/">WordPress</a> using <a href="http://peadig.com/">Peadig</a>. ';
}
add_filter('admin_footer_text', 'footer_admin');








/*
 * Peadig Social meta box, until the end of the file
 * TODO OOP this stuff
 * TODO add inline previews into the meta box
 * TODO use default admin styling
 */

function pd_add_custom_box() {
    $post_types = get_post_types( '', 'names' ); 
    foreach ( $post_types as $post_type ) {
        if ( "post" == $post_type ) {
            add_meta_box(
                'peadig_sectionid',
                __( 'Post Social Settings - Peadig', 'peadig_singlemeta' ),
                'pd_social_metabox',
                $post_type,
                'advanced',
                'core'
                );
        } else {
            add_meta_box(
                'peadig_sectionid',
                __( 'Peadig', 'peadig_singlemeta' ),
                'pd_social_metabox',
                $post_type,
                'advanced',
                'high'
                );
        }
    }
}
add_action( 'add_meta_boxes', 'pd_add_custom_box' );

function pd_save_postdata( $post_id ) {
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ){
        return;
    }
    if ( !isset( $_POST['peadig_noncename'] ) ) {
        return;
    }
    if ( isset( $_POST['peadig_noncename'] ) && !wp_verify_nonce( $_POST['peadig_noncename'], plugin_basename( __FILE__ ) ) ){
        return;
    }
    if ( 'page' == $_POST['post_type'] ){
        if ( !current_user_can( 'edit_page', $post_id ) ){
            return;
        }
    } else {

        if ( !current_user_can( 'edit_post', $post_id ) ){
            return;
        }
    }    

    $_peadig_twitter_text_data = sanitize_text_field( $_POST['_peadig_twitter_text'] );
    add_post_meta($post_id, '_peadig_twitter_text', $_peadig_twitter_text_data, true) or
    update_post_meta($post_id, '_peadig_twitter_text', $_peadig_twitter_text_data);

    $_peadig_twitter_title_data = sanitize_text_field( $_POST['_peadig_twitter_title'] );
    add_post_meta($post_id, '_peadig_twitter_title', $_peadig_twitter_title_data, true) or
    update_post_meta($post_id, '_peadig_twitter_title', $_peadig_twitter_title_data);

    $_peadig_twitter_desc_data = sanitize_text_field( $_POST['_peadig_twitter_desc'] );
    add_post_meta($post_id, '_peadig_twitter_desc', $_peadig_twitter_desc_data, true) or
    update_post_meta($post_id, '_peadig_twitter_desc', $_peadig_twitter_desc_data);

    $_peadig_twitter_img_1_data = sanitize_text_field( $_POST['_peadig_twitter_image_1'] );
    add_post_meta($post_id, '_peadig_twitter_image_1', $_peadig_twitter_img_1_data, true) or
    update_post_meta($post_id, '_peadig_twitter_image_1', $_peadig_twitter_img_1_data);

    $_peadig_fbog_title_data = sanitize_text_field( $_POST['_peadig_fbog_title'] );
    add_post_meta($post_id, '_peadig_fbog_title', $_peadig_fbog_title_data, true) or
    update_post_meta($post_id, '_peadig_fbog_title', $_peadig_fbog_title_data);

    $_peadig_fbog_desc_data = sanitize_text_field( $_POST['_peadig_fbog_desc'] );
    add_post_meta($post_id, '_peadig_fbog_desc', $_peadig_fbog_desc_data, true) or
    update_post_meta($post_id, '_peadig_fbog_desc', $_peadig_fbog_desc_data);

    $_peadig_fbog_image_data = sanitize_text_field( $_POST['_peadig_fbog_image'] );
    add_post_meta($post_id, '_peadig_fbog_image', $_peadig_fbog_image_data, true) or
    update_post_meta($post_id, '_peadig_fbog_image', $_peadig_fbog_image_data);

    $_peadig_gplus_title_data = sanitize_text_field( $_POST['_peadig_gplus_title'] );
    add_post_meta($post_id, '_peadig_gplus_title', $_peadig_gplus_title_data, true) or
    update_post_meta($post_id, '_peadig_gplus_title', $_peadig_gplus_title_data);

    $_peadig_gplus_desc_data = sanitize_text_field( $_POST['_peadig_gplus_desc'] );
    add_post_meta($post_id, '_peadig_gplus_desc', $_peadig_gplus_desc_data, true) or
    update_post_meta($post_id, '_peadig_gplus_desc', $_peadig_gplus_desc_data);

    $_peadig_gplus_type_data = sanitize_text_field( $_POST['_peadig_gplus_type'] );
    add_post_meta($post_id, '_peadig_gplus_type', $_peadig_gplus_type_data, true) or
    update_post_meta($post_id, '_peadig_gplus_type', $_peadig_gplus_type_data);

    $_peadig_gplus_image_data = sanitize_text_field( $_POST['_peadig_gplus_image'] );
    add_post_meta($post_id, '_peadig_gplus_image', $_peadig_gplus_image_data, true) or
    update_post_meta($post_id, '_peadig_gplus_image', $_peadig_gplus_image_data);


    $_peadig_header_js_data = sanitize_text_field( $_POST['_peadig_header_js'] );
    add_post_meta($post_id, '_peadig_header_js', $_peadig_header_js_data, true) or
    update_post_meta($post_id, '_peadig_header_js', $_peadig_header_js_data);

    $_peadig_footer_js_data = sanitize_text_field( $_POST['_peadig_footer_js'] );
    add_post_meta($post_id, '_peadig_footer_js', $_peadig_footer_js_data, true) or
    update_post_meta($post_id, '_peadig_footer_js', $_peadig_footer_js_data);

}


add_action( 'save_post', 'pd_save_postdata' );

function pd_social_metabox( $post ) {

  wp_nonce_field( plugin_basename( __FILE__ ), 'peadig_noncename' );

  $_peadig_twitter_title         = get_post_meta( get_the_ID(), $key = '_peadig_twitter_title', $single = true );
  $_peadig_twitter_text          = get_post_meta( get_the_ID(), $key = '_peadig_twitter_text', $single = true );
  $_peadig_twitter_desc          = get_post_meta( get_the_ID(), $key = '_peadig_twitter_desc', $single = true );
  $_peadig_twitter_image_1       = get_post_meta( get_the_ID(), $key = '_peadig_twitter_image_1', $single = true );

  $_peadig_fbog_title           = get_post_meta( get_the_ID(), $key = '_peadig_fbog_title', $single = true );
  $_peadig_fbog_desc            = get_post_meta( get_the_ID(), $key = '_peadig_fbog_desc', $single = true );
  $_peadig_fbog_image           = get_post_meta( get_the_ID(), $key = '_peadig_fbog_image', $single = true );

  $_peadig_gplus_title          = get_post_meta( get_the_ID(), $key = '_peadig_gplus_title', $single = true );
  $_peadig_gplus_desc           = get_post_meta( get_the_ID(), $key = '_peadig_gplus_desc', $single = true );
  $_peadig_gplus_type           = get_post_meta( get_the_ID(), $key = '_peadig_gplus_type', $single = true );
  $_peadig_gplus_image          = get_post_meta( get_the_ID(), $key = '_peadig_gplus_image', $single = true );

  $_peadig_header_js            = get_post_meta( get_the_ID(), $key = '_peadig_header_js', $single = true );
  $_peadig_footer_js            = get_post_meta( get_the_ID(), $key = '_peadig_footer_js', $single = true );


?>

    <div class="peadig_meta_tabs">

        <div class="peadig_tab_buttons">
            <div class="peadig_tab_button active" id="peadig_twitter_button">Twitter</div>
            <div class="peadig_tab_button" id="peadig_facebook_button">Facebook</div>
            <div class="peadig_tab_button" id="peadig_google_button">Google+</div>
        </div>
        <div class="peadig_tabs">
            <div class="peadig_tab active" id="peadig_twitter_tab">
                <table class="form-table">
                    <tbody>
                    <tr>
                        <td scope="row"><label for="_peadig_twitter_text">Tweet button text</label></td>
                        <td>
                            <input type="text" placeholder="" id="_peadig_twitter_text" autocomplete="off" name="_peadig_twitter_text" value="<?php echo $_peadig_twitter_text; ?>" class="large-text">
                            <p id="_peadig_twitter_text_limit"></p>
                        </td>
                    </tr>
                    <tr>
                        <td scope="row"><label for="_peadig_twitter_title">Twitter Title</label></td>
                        <td>
                            <input type="text" placeholder="" id="_peadig_twitter_title" autocomplete="off" name="_peadig_twitter_title" value="<?php echo $_peadig_twitter_title; ?>" class="large-text">
                            <p id="_peadig_twitter_title_limit"></p>
                        </td>
                    </tr>
                    <tr>
                        <td scope="row"><label for="_peadig_twitter_desc">Twitter Description</label></td>
                        <td>
                            <textarea class="large-text" rows="3" id="_peadig_twitter_desc" name="_peadig_twitter_desc"><?php echo
                            $_peadig_twitter_desc; ?></textarea>
                            <p id="_peadig_twitter_desc_limit"></p>
                        </td>
                    </tr>
                    <tr>
                        <td scope="row"><label for="_peadig_twitter_image_1">Twitter Image</label></td>
                        <td>
                            <div class="_peadig_twitter_image">
                                <div class="_peadig_twitter_uploader wp-core-ui">

                                    <input type="hidden" name="_peadig_twitter_image_1" id="_peadig_twitter_image_1" value="<?php echo $_peadig_twitter_image_1;?>" />
                                    <input class="_peadig_upload_button button" name="_peadig_twitter_image_1_button" id="_peadig_twitter_image_1_button" value="Upload Image" />
                                </div>
                                <div class="_peadig_twitter_preview">
                                    <span id="_peadig_twitter_image_1_preview"><?php if(isset($_peadig_twitter_image_1)){ ?>
                                    <img src="<?php echo $_peadig_twitter_image_1; ?>" height="50px"/>
                                    <?php } ?></span>
                                </div>
                            
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>



            </div>

            <div class="peadig_tab" id="peadig_facebook_tab">
                <table class="form-table">

                <tbody>
                <tr>
                    <td scope="row"><label for="_peadig_fbog_title">Facebook Open Graph Title</label></td>
                    <td>
                        <input type="text" placeholder="" id="_peadig_fbog_title" autocomplete="off" name="_peadig_fbog_title" value="<?php echo $_peadig_fbog_title; ?>" class="large-text">
                        <p id="_peadig_fbog_title_limit"></p>
                    </td>
                </tr>
                <tr>
                    <td scope="row"><label for="_peadig_fbog_desc">Facebook Open Graph Description</label></td>
                    <td>
                        <textarea class="large-text" rows="3" id="_peadig_fbog_desc" name="_peadig_fbog_desc"><?php echo
                        $_peadig_fbog_desc; ?></textarea>
                        <p id="_peadig_fbog_desc_limit"></p>
                    </td>
                </tr>
                <tr>
                    <td scope="row"><label for="_peadig_fbog_image">Facebook Open Graph Image</label></td>
                    <td>
                        <div class="uploader wp-core-ui">
                            <input type="hidden" name="_peadig_fbog_image" id="_peadig_fbog_image" value="<?php echo $_peadig_fbog_image;?>" />
                            <input class="_peadig_upload_button button" name="_peadig_fbog_image_button" id="_peadig_fbog_image_button" value="Upload Image" />
                            <p id="_peadig_fbog_image_preview">
                            <?php if(isset($_peadig_fbog_image)){ ?>
                            <img src="<?php echo $_peadig_fbog_image?>" height="100px"/>
                            <?php } ?>
                            </p>
                        </div>
                    </td>
                </tr>
                </table>
            </div>

            <div class="peadig_tab" id="peadig_google_tab">
                <table class="form-table">
                    <tbody>
                    <tr>
                        <td scope="row"><label for="_peadig_gplus_title">Google Plus Title</label></td>
                        <td>
                            <input type="text" placeholder="" id="_peadig_gplus_title" autocomplete="off" name="_peadig_gplus_title" value="<?php echo $_peadig_gplus_title; ?>" class="large-text">
                            <p id="_peadig_gplus_title_limit"></p>
                        </td>
                    </tr>

                    <tr>
                        <td scope="row"><label for="_peadig_gplus_desc">Google+ Post Description</label></td>
                        <td>
                            <textarea class="large-text" rows="3" id="_peadig_gplus_desc" name="_peadig_gplus_desc"><?php echo
                            $_peadig_gplus_desc; ?></textarea>
                            <p id="_peadig_gplus_desc_limit"></p>
                        </td>
                    </tr>


                    <tr>
                        <td scope="row"><label for="_peadig_gplus_image">Google+ Image</label></td>
                        <td>
                            <div class="uploader wp-core-ui">
                                <input type="hidden" name="_peadig_gplus_image" id="_peadig_gplus_image" value="<?php echo $_peadig_gplus_image;?>">
                                <input class="_peadig_upload_button button" name="_peadig_gplus_image_button" id="_peadig_gplus_image_button" value="Upload Image" />
                                <p id="_peadig_gplus_image_preview">
                                <?php if(isset($_peadig_gplus_image)){ ?>
                                <img src="<?php echo $_peadig_gplus_image?>" height="100px"/>
                                <?php } ?>

                                </p>
                            </div>
                        </td>
                    </tr>
                    <tr>
                            <td scope="row"><label for="_peadig_gplus_type">Google+ Type</label></td>
                            <td>
                                <?php
                                $gp_types = array("Article", "Blog", "Book", "Event", "Local Business", "Organisation", "Person", "Product", "Review", "Other")
                                ?>
                                <select type="text" id="_peadig_gplus_type" name="_peadig_gplus_type" class="large-text">
                                <?php
                                foreach($gp_types as $card){
                                ?>
                                <option value="<?php echo $card?>"<?php if($_peadig_gplus_type==$card){ echo " selected";} ?>><?php echo $card?></option>
                                <?php
                                }
                                ?>
                                </select>
                                <p id="_peadig_gplus_type_limit"></p>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>

    </div>


    <script type="text/javascript">
    jQuery(document).ready(function($){
        var _custom_media = true,
        _orig_send_attachment = wp.media.editor.send.attachment;

        $('._peadig_upload_button').click(function(e) {


            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = $(this);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;
            wp.media.editor.send.attachment = function(props, attachment){
                if ( _custom_media ) {
                    $("#"+id).val(attachment.url);
                    $("#"+id+"_preview").html('<img src="'+attachment.url+'" width="150">');
                } else {
                    return _orig_send_attachment.apply( this, [props, attachment] );
                };
            }

            wp.media.editor.open(button);
            return false;
        });

        $('.add_media').on('click', function(){
            _custom_media = false;
        });

        <?php
        $limiting = array(
            array("name" => "_peadig_twitter_text", "limit" => 75),
            array("name" => "_peadig_twitter_title", "limit" => 160),
            array("name" => "_peadig_twitter_desc", "limit" => 200),
            array("name" => "_peadig_fbog_title", "limit" => 160),
            array("name" => "_peadig_fbog_desc", "limit" => 300),
            array("name" => "_peadig_gplus_title", "limit" => 160),
            array("name" => "_peadig_gplus_desc", "limit" => 200),
            );


        foreach ($limiting as $value){ ?>
            $("#<?php echo $value['name']?>").keyup(function(){
                var val = $(this).val();
                var remaining = parseFloat(<?php echo $value['limit'] ?>)-(val.length+1);
                $("#<?php echo $value['name']?>_limit").text(remaining + " of <?php echo $value['limit'] ?> characters remaining");
            });
            <?php } ?>


            $(".peadig_tab_button").on("click", function(){
                var clicked_tab = $(this).attr('id');

                clicked_tab = clicked_tab.replace("peadig_","");
                clicked_tab = clicked_tab.replace("_button","");

                $('.peadig_tab_button').removeClass('active');
                $('.peadig_tab').removeClass('active');
                $('#peadig_'+clicked_tab+'_button').addClass('active');
                $('#peadig_'+clicked_tab+'_tab').addClass('active');


            });

        });

    </script>


<?php
}
