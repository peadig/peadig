<?php
/*
 * All the functions from previous versions will be in here along with their removal version.
 *
 ********************************************************************************/



/**
 * OLD POST NOTIFICATION
 *
 * Scheduled for Removal in version 1.4
 *
 * Calls need replacing with DefaultHook::old_post_notifications()
 *
 */
function old_post_notif () {
    $pd_options = get_option('peadig');
    if (!empty($pd_options['old_post_notif'])) {
        $ageunix = get_the_time('U');
        $days_old_in_seconds = ((time() - $ageunix));
        $days_old = (($days_old_in_seconds/86400));
        if ($days_old > $pd_options['how_many_days']) : ?>
            <div class="alert alert-info">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                This post was written <strong><?php echo human_time_diff( get_the_time('U') ) . ' ago'; ?></strong> and therefore may not be as accurate as more recent posts.
            </div>
        <?php endif;
    }
}






/**
 * SOCIAL SHARING BUTTONS
 *
 * Scheduled for Removal in version 1.4
 *
 * Calls need replacing with DefaultHook::social_buttons()
 *
 */
function peadig_social_buttons()
{
    if (is_singular()) {
        $custom_fields = get_post_custom();
        if (!empty($custom_fields)) {
            foreach ($custom_fields as $field_key => $field_values) {
                foreach ($field_values as $key => $value)
                    $post_meta[$field_key] = $value; // builds array
            }
        }
    }
    $pd_options = get_option('peadig');
    ?><div class="peadigsocial"><?php
    if (!empty($pd_options['twitter_tweet'])) {
        $twitter_profile = get_the_author_meta('twitter');
        if (!empty($twitter_profile)) {
            $byauthor = ' by @' . $twitter_profile;
        }
        if (!empty($post_meta['_peadig_twitter_text'])) {
            $tweetext = $post_meta['_peadig_twitter_text'];
        } else {
            $tweetext = get_the_title();
        }
        echo '<div class="tweet"><a rel="nofollow" href="https://twitter.com/share" class="twitter-share-button" data-url="' . get_permalink() . '" data-text="' . $tweetext . $byauthor . '" data-via="' . $pd_options['twitter_username'] . '" data-related="' . $twitter_profile . '">Tweet</a>
        </div>';
    }
    if (!empty($pd_options['facebook_like'])) {
        if (!empty($pd_options['facebook_share'])) {$share = ' data-share="true"';}
        echo '<div class="fb-like" data-href="' . get_permalink() . '" data-width="150" data-layout="button_count" data-action="like" data-show-faces="false"'.$share.'></div>';
    }
    if (empty($pd_options['facebook_like']) && !empty($pd_options['facebook_share'])) {
        echo '<div class="fb-share-button" data-href="' . get_permalink() . '" data-type="button_count"></div>';
    }
    if (!empty($pd_options['google_plusone'])) {
        echo '<div class="plusone">
        <div class="g-plusone" data-size="medium" callback="plusone" data-href="' . get_permalink() . '"></div>
        </div>';
    }
    if (!empty($pd_options['buffer_button'])) {
        $twitter_profile = get_the_author_meta('twitter');
        if (!empty($twitter_profile)) {
            $byauthor = ' by @' . $twitter_profile;
        }
        if (!empty($post_meta['_peadig_twitter_text'])) {
            $tweetext = $post_meta['_peadig_twitter_text'];
        } else {
            $tweetext = get_the_title();
        }
        echo '<div class="buffer">
        <a href="http://bufferapp.com/add" class="buffer-add-button" data-text="' . $tweetext . $byauthor . '" data-count="horizontal" data-via="' . $pd_options['twitter_username'] . '">Buffer</a></div>';
    }
    if (!empty($pd_options['linkedin_share'])) {
        echo '<div class="inshare"><script type="IN/Share" data-counter="right"></script></div>';
    }
    if (!empty($pd_options['pocket_button'])) {
        echo '<div class="pocket"><a data-pocket-label="pocket" data-pocket-count="none" class="pocket-btn" data-lang="en"></a></div>';
    }
    if (!empty($pd_options['pinterest_pinit'])) {
        echo '<div class="pinterest-btn">';
        ?><a href="//www.pinterest.com/pin/create/button/" data-pin-do="buttonBookmark" ><img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_gray_20.png" /></a>
        </div><?php
    }
    if (!empty($pd_options['stumble_upon'])) {
        echo '<div class="stumble"><su:badge layout="2"></su:badge></div>';
    }
    if (!empty($pd_options['reddit_share'])) {
        ?>
        <div class="reddit">
            <script type="text/javascript">
                reddit_url = "<?php
            the_permalink();
            ?>";
                reddit_title = "<?php
            the_title();
            ?>";
            </script>
            <script type="text/javascript" src="http://www.reddit.com/static/button/button1.js"></script></div>
    <?php
    }
    echo '</div>';
}