<?php
/**
Peadig Carousel Template for NexGen Gallery

Follow variables are useable :

	$gallery     : Contain all about the gallery
	$images      : Contain all images, path, title
	$pagination  : Contain the pagination content

 You can check the content when you insert the tag <?php var_dump($variable) ?>
 If you would like to show the timestamp of the image ,you can use <?php echo $exif['created_timestamp'] ?>
**/
?>
<?php if (!defined ('ABSPATH')) die ('No direct access allowed'); ?><?php if (!empty ($gallery)) : ?>

        <div id="<?php echo $gallery->anchor ?>" class="carousel slide">
	<ol class="carousel-indicators">
	<?php $activenum="0"; foreach ( $images as $image ) : ?>
		<li data-target="#<?php echo $gallery->anchor ?>" data-slide-to="<?php echo $activenum; ?>"<?php if ($activenum=="0") {echo ' class="active"'; } $activenum++; ?></li>
 	<?php endforeach; ?>
	</ol>

            <div class="carousel-inner">
	<?php $activenum="1"; foreach ( $images as $image ) : ?>
            <div id="ngg-image-<?php echo $image->pid ?>" class="item<?php if ($activenum=="1") {echo " active"; } $activenum++; ?>" <?php echo $image->style ?>>
<img title="<?php echo $image->alttext; ?>" alt="<?php echo $image->alttext; ?>" src="<?php echo $image->imageURL; ?>"  />
<div class="carousel-caption"><h3><?php echo $image->alttext; ?></h3><?php if (!empty($image->description)) { echo '<p>'.$image->description.'</p>'; } ?></div>
              </div>
 	<?php endforeach; ?>
            </div>
            <a class="left carousel-control" href="#<?php echo $gallery->anchor ?>" data-slide="prev"><span class="icon-prev"></span></a>
            <a class="right carousel-control" href="#<?php echo $gallery->anchor ?>" data-slide="next"><span class="icon-next"></span></a>
          </div>
<?php endif; ?>