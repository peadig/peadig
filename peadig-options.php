<?php

if (!class_exists('Redux_Framework_sample_config')) {

	class Redux_Framework_sample_config {

		public $args        = array();
		public $sections    = array();
		public $theme;
		public $ReduxFramework;

		public function __construct() {

			if (!class_exists('ReduxFramework')) {
				return;
			}

            // This is needed. Bah WordPress bugs.  ;)
			if (  true == Redux_Helpers::isTheme(__FILE__) ) {
				$this->initSettings();
			} else {
				add_action('plugins_loaded', array($this, 'initSettings'), 10);
			}

		}

		public function initSettings() {

            // Just for demo purposes. Not needed per say.
			$this->theme = wp_get_theme();

            // Set the default arguments
			$this->setArguments();

            // Set a few help tabs so you can see how it's done
			$this->setHelpTabs();

            // Create the sections and fields
			$this->setSections();

            if (!isset($this->args['opt_name'])) { // No errors please
            	return;
            }

            // If Redux is running as a plugin, this will remove the demo notice and links
            //add_action( 'redux/loaded', array( $this, 'remove_demo' ) );
            
            // Function to test the compiler hook and demo CSS output.
            // Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
            //add_filter('redux/options/'.$this->args['opt_name'].'/compiler', array( $this, 'compiler_action' ), 10, 2);
            
            // Change the arguments after they've been declared, but before the panel is created
            //add_filter('redux/options/'.$this->args['opt_name'].'/args', array( $this, 'change_arguments' ) );
            
            // Change the default value of a field after it's been set, but before it's been useds
            //add_filter('redux/options/'.$this->args['opt_name'].'/defaults', array( $this,'change_defaults' ) );
            
            // Dynamically add a section. Can be also used to modify sections/fields
            //add_filter('redux/options/' . $this->args['opt_name'] . '/sections', array($this, 'dynamic_section'));

            $this->ReduxFramework = new ReduxFramework($this->sections, $this->args);
          }

        /**

          Custom function for filtering the sections array. Good for child themes to override or add to the sections.
          Simply include this function in the child themes functions.php file.

          NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
          so you must use get_template_directory_uri() if you want to use any of the built in icons

         * */
          function dynamic_section($sections) {
            //$sections = array();
          	$this->sections[] = array(
          		'title' => __('Section via hook', 'redux-framework-demo'),
          		'desc' => __('<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'redux-framework-demo'),
          		'icon' => 'el el-icon-paper-clip',
                // Leave this as a blank section, no options just some intro text set above.
          		'fields' => array()
          		);

          	return $sections;
          }

        /**

          Filter hook for filtering the default value of any given field. Very useful in development mode.

         * */
          function change_defaults($defaults) {
          	$defaults['str_replace'] = 'Testing filter hook!';

          	return $defaults;
          }

        // Remove the demo link and the notice of integrated demo from the redux-framework plugin
          function remove_demo() {

            // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
          	if (class_exists('ReduxFrameworkPlugin')) {
          		remove_filter('plugin_row_meta', array(ReduxFrameworkPlugin::instance(), 'plugin_metalinks'), null, 2);

                // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
          		remove_action('admin_notices', array(ReduxFrameworkPlugin::instance(), 'admin_notices'));
          	}
          }

          public function setSections() {

            /**
              Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
             * */
            // Background Patterns Reader
              $sample_patterns_path   = ReduxFramework::$_dir . '../sample/patterns/';
              $sample_patterns_url    = ReduxFramework::$_url . '../sample/patterns/';
              $sample_patterns        = array();

              if (is_dir($sample_patterns_path)) :

              	if ($sample_patterns_dir = opendir($sample_patterns_path)) :
              		$sample_patterns = array();

              	while (( $sample_patterns_file = readdir($sample_patterns_dir) ) !== false) {

              		if (stristr($sample_patterns_file, '.png') !== false || stristr($sample_patterns_file, '.jpg') !== false) {
              			$name = explode('.', $sample_patterns_file);
              			$name = str_replace('.' . end($name), '', $sample_patterns_file);
              			$sample_patterns[]  = array('alt' => $name, 'img' => $sample_patterns_url . $sample_patterns_file);
              		}
              	}
              	endif;
              	endif;

              	ob_start();

              	$ct             = wp_get_theme();
              	$this->theme    = $ct;
              	$item_name      = $this->theme->get('Name');
              	$tags           = $this->theme->Tags;
              	$screenshot     = $this->theme->get_screenshot();
              	$class          = $screenshot ? 'has-screenshot' : '';

              	$customize_title = sprintf(__('Customize &#8220;%s&#8221;', 'redux-framework-demo'), $this->theme->display('Name'));
              	
              	?>
              	<div id="current-theme" class="<?php echo esc_attr($class); ?>">
              		<?php if ($screenshot) : ?>
              		<?php if (current_user_can('edit_theme_options')) : ?>
              		<a href="<?php echo wp_customize_url(); ?>" class="load-customize hide-if-no-customize" title="<?php echo esc_attr($customize_title); ?>">
              			<img src="<?php echo esc_url($screenshot); ?>" alt="<?php esc_attr_e('Current theme preview'); ?>" />
              		</a>
              	<?php endif; ?>
              	<img class="hide-if-customize" src="<?php echo esc_url($screenshot); ?>" alt="<?php _e('Current theme preview', 'peadig-framework'); ?>" />
              <?php endif; ?>

              <h4><?php echo $this->theme->display('Name'); ?></h4>

              <div>
              	<ul class="theme-info">
              		<li><?php printf(__('By %s', 'redux-framework-demo'), $this->theme->display('Author')); ?></li>
              		<li><?php printf(__('Version %s', 'redux-framework-demo'), $this->theme->display('Version')); ?></li>
              		<li><?php echo '<strong>' . __('Tags', 'redux-framework-demo') . ':</strong> '; ?><?php printf($this->theme->display('Tags')); ?></li>
              	</ul>
              	<p class="theme-description"><?php echo $this->theme->display('Description'); ?></p>
              	<?php
              	if ($this->theme->parent()) {
              		printf(' <p class="howto">' . __('This <a href="%1$s">child theme</a> requires its parent theme, %2$s.', 'peadig-framework') . '</p>', __('http://codex.wordpress.org/Child_Themes', 'redux-framework-demo'), $this->theme->parent()->display('Name'));
              	}
              	?>

              </div>
            </div>

            <?php
            $item_info = ob_get_contents();

            ob_end_clean();



# #
# # #
# # # # # # SITE OPTIONS
# # #
# #


            $this->sections[] = array(
             'icon' => 'el el-icon-cog',
             'icon_class' => 'icon-large',
             'title' => __('Site Options', 'peadig-framework'),
             'fields' => array(
              array(
               'id'     => 'contact_heading',
               'type'   => 'info',
               'notice' => true,
               'style'  => 'info',
               'icon'   => 'el el-icon-envelope',
               'title'  => __( 'Contact Details', 'peadig-framework' ),
               'desc'   => __( 'Add in your Email and Phone Number', 'peadig-framework' )
               ),
              array(
               'id'       => 'address',
               'type'     => 'textarea',
               'title'    => __( '<span class="el el-icon-map-marker"></span> Address', 'redux-framework-demo' ),
               'desc'     => __( 'Note: HTML is disabled.', 'redux-framework-demo' ),
               'validate' => 'no_html',
               'default'  => 'No HTML is allowed in here.'
               ),
              array(
               'id'        => 'email',
               'type'      => 'text',
               'title'     => __('<span class="el el-icon-envelope"></span> Email Address', 'peadig-framework'),
               'validate'  => 'email',
               'desc'      => __('This is the email address used in the header as well as the contact form.', 'peadig-framework')
               ),
              array(
               'id'        => 'phone',
               'type'      => 'text',
               'title'     => __('<span class="el el-icon-phone"></span> Telephone', 'peadig-framework'),
               'desc'      => __('This will show the phone number in the header inclusive of a click to call link.', 'peadig-framework')
               ),
              array(
               'id'     => 'logo_heading',
               'type'   => 'info',
               'notice' => true,
               'style'  => 'info',
               'icon'   => 'el el-icon-globe',
               'title'  => __( 'Logo Options', 'peadig-framework' ),
               'desc'   => __( 'You can upload your own logo image, or use your Site Name and Tagline in the header.', 'peadig-framework' )
               ),
              array(
               'id'        => 'use_logo_image',
               'type'      => 'switch',
               'on'        => 'Yes',
               'off'       => 'No',
               'title'     => __('Use Logo Image', 'peadig-framework'),
               'desc'      => '<p>Turning this option off will use Site Name and Tagline instead of image logo below</p>',
               'switch'    => true,
               'default'   => '1'
               ),
              array(
               'id'        => 'logo',
               'type'      => 'media',
               'url'=> true,
               'compiler' => 'true',
               'required' => array('use_logo_image', '=' , '1'),
               'title'         => __('Logo', 'peadig-framework'),
               'desc'      => '<p>Uploading your own logo here will add it to the header. The logo can be any pixel dimension and will be cropped to fit into the template.</p>'
               ),
              array(
               'id'        => 'favicon',
               'type'      => 'media',
               'url'=> true,
               'compiler' => 'true',
               'title'     => __('Favicon', 'peadig-framework'),
               'desc'      => '<p>Upload a PNG or ICO file that is <strong>16x16 pixels</strong>. This will appear as the favicon for both the frontend and backend of the site.</p>'
               ),
              array(
               'id'        => 'apple_touch',
               'type'      => 'media',
               'url'=> true,
               'compiler' => 'true',
               'title'     => __('Apple Touch Icon', 'peadig-framework'),
               'desc'      => '<p>Upload a PNG or ICO file that is <strong>32x32 pixels</strong>. This will appear as the touch icon for Apple devices.</p>'
               ),
              array(
               'id'        => 'admin_login_logo',
               'type'      => 'media',
               'url'=> true,
               'compiler' => 'true',
               'title'     => __('Admin Login Logo', 'peadig-framework'),
               'desc'      => '<p>When logging into the site, you can change the logo from the default WordPress logo. You should make this image <strong>80x80 pixels</strong> or 274x63 pixels if using not using Wordpress 3.8 or above.</p>'
               ),
              )
);

$this->sections[] = array(
	'icon' => 'el el-icon-tint',
	'icon_class' => 'icon-large',
	'title' => __('Color and Typography', 'peadig-framework'),
	'subsection' => true,
	'fields' => array(
		array(
			'id'=>'h_font',
			'type' => 'typography', 
			'title' => __('Heading Options', 'peadig-framework'),
				'compiler'=>true, // Use if you want to hook in your own CSS compiler
				'google'=>true, // Disable google fonts. Won't work if you haven't defined your google api key
				//'font-style'=>false, // Includes font-style and weight. Can use font-style or font-weight to declare
				'subsets'=>false, // Only appears if google is true and subsets not set to false
				'line-height'=>false,
				'font-size'=>false,
				'all_styles' => false, // Enable all Google Font style/weight variations to be added to the page
				'output' => array('body'), // An array of CSS selectors to apply this font style to dynamically
				'units'=>'px', // Defaults to px
				'default'=> array(
					'color'=>"#333", 
					'font-family'=>'Arial,Helvetica,Arial,sans-serif', 
					'font-size'=>'33px', 
					'google' => true),
				),
		array(
			'id'=>'text_font',
			'type' => 'typography', 
			'title' => __('Text Options', 'peadig-framework'),
				'compiler'=>true, // Use if you want to hook in your own CSS compiler
				'google'=>true, // Disable google fonts. Won't work if you haven't defined your google api key
				//'font-style'=>false, // Includes font-style and weight. Can use font-style or font-weight to declare
				'subsets'=>false, // Only appears if google is true and subsets not set to false
				'line-height'=>false,
				'all_styles' => false, // Enable all Google Font style/weight variations to be added to the page
				'output' => array('body'), // An array of CSS selectors to apply this font style to dynamically
				'units'=>'px', // Defaults to px
				'default'=> array(
					'color'=>"#333", 
					'font-family'=>'Arial,Helvetica,Arial,sans-serif', 
					'google' => true,
					'font-size'=>'14px'),
				),
		array(
			'id'=>'link_color',
			'type' => 'link_color',
			'title' => __('Link Colors', 'peadig-framework'),
			'subtitle' => __('Only color validation can be done on this field type', 'peadig-framework'),
			'desc' => __('This is the description field, again good for additional info.', 'peadig-framework'),
			'default' => array(
				'regular' => '#428bca',
				'hover' => '#2a6496',
				'active' => '#2a6496',
				)
			),
		array(
			'id'        => 'bg_color',
			'type'      => 'color',
			'title'     => __('Background Color', 'peadig-framework'),
			'default'   => '#ffffff'
			),
		array(
			'id'        => 'container_color',
			'type'      => 'color',
			'title'     => __('Container Background Color', 'peadig-framework'),
			'default'   => 'transparent'
			),
		array(
			'id'        => 'nav_bg',
			'type'      => 'color',
			'title'     => __('Nav Background Color', 'peadig-framework'),
			'default'   => '#f8f8f8'
			),
		array(
			'id'        => 'nav_border',
			'type'      => 'color',
			'title'     => __('Nav Border Color', 'peadig-framework'),
			'default'   => '#e7e7e7'
			),
		array(
			'id'        =>'custom_css',
			'type' => 'ace_editor',
			'mode' => 'css',
			'theme' => 'chrome',
			'title'     => __('Custom CSS', 'peadig-framework'),
			'desc'      => __('Quickly add some CSS to your theme by adding it to this block.', 'peadig-framework'),
			'validate' => 'css',
			),

    array(
  'id'        => 'mobile_bar',
  'type'      => 'color',
  'title'     => __('Mobile Color Theme', 'peadig-framework'),
  'desc'     => __('You can choose any color, but Google have also provided preset color palletes <a href="http://www.google.com/design/spec/style/color.html#color-color-palette" target="_blank">here</a>', 'peadig-framework'),
  'transparent'     => false
  ),

		)
);

$this->sections[] = array(
	'icon' => 'el el-icon-user',
	'title' => __('Social Media Profiles', 'peadig-framework'),
	'subsection' => true,
	'fields' => array(
		array(
			'id'        => 'twitter_username',
			'type'      => 'text',
			'title'     => __('<span class="el el-icon-twitter"></span> Twitter Username', 'peadig-framework'),
			'default'   => '',
			'class'     => 'medium-text',
			'validate'  => 'str_replace',
			'str'       => array('search' => '@', 'replacement' => ''),
			'desc'      => __('<p>No need to include the @</p>', 'peadig-framework')
			),
		array(
			'id'        => 'facebook_page_url',
			'type'      => 'text',
			'title'     => __('<span class="el el-icon-facebook"></span> Facebook Page URL', 'peadig-framework'),
			'validate'  => 'url'
			),
		array(
			'id'        => 'facebook_page_id',
			'type'      => 'text',
			'title'     => __('<span class="el el-icon-facebook"></span> Facebook Page ID', 'peadig-framework'),
			'validate'  => 'numeric',
			'class'     => 'medium-text',
			'desc'      => __('<p>You can find this by performing a Graph Search. Click <a href="http://graph.facebook.com/peadig" target="_blank">here</a> to see a Graph search for Peadig.</p>', 'peadig-framework')
			),
		array(
			'id'        => 'facebook_app_id',
			'type'      => 'text',
			'title'     => __('<span class="el el-icon-facebook"></span> Facebook App ID', 'peadig-framework'),
			'validate'  => 'numeric',
			'class'     => 'medium-text',
			'desc'      => __('<p>You can find out your Facebook App ID by visiting the <a href="https://developers.facebook.com/apps" target="_blank">Facebook Developer Area</a>.</p>', 'peadig-framework')
			),
    array(
      'id'        => 'facebook_lang',
      'type'     => 'select',
      'title'     => __('<span class="el el-icon-facebook"></span> Facebook Language', 'peadig-framework'),
      'desc'      => __('<p>Facebook is offered in many languages. Please choose a language as this is separate to WordPress default languages.</p>', 'peadig-framework'),
                            //Must provide key => value pairs for select options
      'options'  => array(
        'af_ZA' => 'Afrikaans',
        'ar_AR' => 'Arabic',
        'az_AZ' => 'Azerbaijani',
        'be_BY' => 'Belarusian',
        'bg_BG' => 'Bulgarian',
        'bn_IN' => 'Bengali',
        'bs_BA' => 'Bosnian',
        'ca_ES' => 'Catalan',
        'cs_CZ' => 'Czech',
        'cy_GB' => 'Welsh',
        'da_DK' => 'Danish',
        'de_DE' => 'German',
        'el_GR' => 'Greek',
        'en_GB' => 'English (UK)',
        'en_PI' => 'English (Pirate)',
        'en_UD' => 'English (Upside Down)',
        'en_US' => 'English (US)',
        'eo_EO' => 'Esperanto',
        'es_ES' => 'Spanish (Spain)',
        'es_LA' => 'Spanish',
        'et_EE' => 'Estonian',
        'eu_ES' => 'Basque',
        'fa_IR' => 'Persian',
        'fb_LT' => 'Leet Speak',
        'fi_FI' => 'Finnish',
        'fo_FO' => 'Faroese',
        'fr_CA' => 'French (Canada)',
        'fr_FR' => 'French (France)',
        'fy_NL' => 'Frisian',
        'ga_IE' => 'Irish',
        'gl_ES' => 'Galician',
        'he_IL' => 'Hebrew',
        'hi_IN' => 'Hindi',
        'hr_HR' => 'Croatian',
        'hu_HU' => 'Hungarian',
        'hy_AM' => 'Armenian',
        'id_ID' => 'Indonesian',
        'is_IS' => 'Icelandic',
        'it_IT' => 'Italian',
        'ja_JP' => 'Japanese',
        'ka_GE' => 'Georgian',
        'km_KH' => 'Khmer',
        'ko_KR' => 'Korean',
        'ku_TR' => 'Kurdish',
        'la_VA' => 'Latin',
        'lt_LT' => 'Lithuanian',
        'lv_LV' => 'Latvian',
        'mk_MK' => 'Macedonian',
        'ml_IN' => 'Malayalam',
        'ms_MY' => 'Malay',
        'nb_NO' => 'Norwegian (bokmal)',
        'ne_NP' => 'Nepali',
        'nl_NL' => 'Dutch',
        'nn_NO' => 'Norwegian (nynorsk)',
        'pa_IN' => 'Punjabi',
        'pl_PL' => 'Polish',
        'ps_AF' => 'Pashto',
        'pt_BR' => 'Portuguese (Brazil)',
        'pt_PT' => 'Portuguese (Portugal)',
        'ro_RO' => 'Romanian',
        'ru_RU' => 'Russian',
        'sk_SK' => 'Slovak',
        'sl_SI' => 'Slovenian',
        'sq_AL' => 'Albanian',
        'sr_RS' => 'Serbian',
        'sv_SE' => 'Swedish',
        'sw_KE' => 'Swahili',
        'ta_IN' => 'Tamil',
        'te_IN' => 'Telugu',
        'th_TH' => 'Thai',
        'tl_PH' => 'Filipino',
        'tr_TR' => 'Turkish',
        'uk_UA' => 'Ukrainian',
        'vi_VN' => 'Vietnamese',
        'zh_CN' => 'Simplified Chinese (China)',
        'zh_HK' => 'Traditional Chinese (Hong Kong)',
        'zh_TW' => 'Traditional Chinese (Taiwan)',
        ),
'default'  => 'en_US'
),
array(
 'id'        => 'google_plus_page_id',
 'type'      => 'text',
 'title'     => __('<span class="el el-icon-googleplus"></span> Google+ Page URL', 'peadig-framework'),
 'validate'  => 'url',
 'desc'      => __('<p>URL of your Google+ page. This will also be used as the <a href="https://support.google.com/webmasters/answer/1708844?hl=en" target="_blank">rel=publisher</a> to link your Google+ page to your site.</p>', 'peadig-framework')
 ),
array(
 'id'        => 'linkedin_url',
 'type'      => 'text',
 'title'     => __('<span class="el el-icon-linkedin"></span> Linkedin URL', 'peadig-framework'),
 'validate'  => 'url'
 ),
array(
 'id'        => 'foursquare_url',
 'type'      => 'text',
 'title'     => __('<span class="el el-icon-foursquare"></span> Foursquare URL', 'peadig-framework'),
 'validate'  => 'url'
 ),
array(
 'id'        => 'flickr_profile_url',
 'type'      => 'text',
 'title'     => __('<span class="el el-icon-flickr"></span> Flickr Profile URL', 'peadig-framework'),
 'validate'  => 'url'
 ),
array(
 'id'        => 'instagram',
 'type'      => 'text',
 'title'     => __('<span class="el el-icon-instagram"></span> Instagram User ID', 'peadig-framework'),
 'desc'      => __('Your username, not your full URL', 'peadig-framework')
 ),
array(
 'id'        => 'pinterest',
 'type'      => 'text',
 'title'     => __('<span class="el el-pinterest"></span> Pinterest User ID', 'peadig-framework'),
 'desc'      => __('Your username, not your full URL', 'peadig-framework')
 ),
array(
 'id'        => 'youtube_profile_url',
 'type'      => 'text',
 'title'     => __('<span class="el el-icon-youtube"></span> Youtube Profile URL', 'peadig-framework'),
 'validate'  => 'url'
 ),
array(
 'id'        => 'vimeo',
 'type'      => 'text',
 'title'     => __('<span class="el el-icon-vimeo"></span> Vimeo User ID', 'peadig-framework'),
 'desc'      => __('Your username, not your full URL=', 'peadig-framework')
 ),
array(
 'id'        => 'use_rss_icon',
 'title'     => __('<span class="el el-icon-rss"></span> RSS Icon', 'peadig-framework'),
 'desc'      => 'RSS Icon',
 'type'      => 'switch',
 'default'   => '1'
 ),
array(
 'id'        => 'rss_feed',
 'type'      => 'text',
 'required' => array('use_rss_icon', '=' , '1'),
 'title'     => __('<span class="el el-icon-rss"></span> RSS Feed URL', 'peadig-framework'),
 'validate'  => 'url',
 'desc'      => __('You may be using a third party for your RSS feed such as Feedburner. If you want to override the default WordPress RSS feed link, enter the URL above.', 'peadig-framework')
 )
)
);

# #
# # #
# # # # # # TEMPLATE OPTIONS
# # #
# #

$this->sections[] = array(
	'icon'          => 'el el-icon-adjust-alt',
	'title'         => __('Layout Options', 'peadig-framework'),
	'fields' => array(

		)
	);
$this->sections[] = array(
	'icon'          => 'el el-icon-screen',
	'icon_class'    => 'icon-large',
	'subsection' => true,
	'title'         => __('Header and Footer', 'peadig-framework'),
	'fields' => array(

		array(
			'id'     => 'header_heading',
			'type'   => 'info',
			'notice' => true,
			'style'  => 'info',
			'title'  => __( 'Header', 'peadig-framework' )
			),
		array(
			'id'        => 'use_email',
			'type'      => 'switch',
			'on'        => 'Yes',
			'off'       => 'No',
			'title'     => __('Use Email in Header', 'peadig-framework'),
			'default'   => '1'
			),
       array(
    'id'       => 'email_link',
    'type'     => 'button_set',
    'title'    => __( 'Email Link Behavior', 'redux-framework-demo' ),
    'desc'     => __( 'Choose how the link for the contact email address behaves', 'redux-framework-demo' ),
                //Must provide key => value pairs for radio options
    'options'  => array(
      'mailto' => 'mailto: Link',
      'page' => 'Page'
      ),
    'required' => array('use_email','=','1'),
    'default'  => 'mailto'
    ),
       array(
     'id'        => 'email_page',
     'required'  => array('email_link','equals','page'),
     'type'      => 'select', 'data' => 'pages',
     'title'     => __('Link target page', 'peadig-framework'),
     ),
		array(
			'id'        => 'use_phone',
			'type'      => 'switch',
			'on'        => 'Yes',
			'off'       => 'No',
			'title'     => __('Use Phone Number in Header', 'peadig-framework'),
			'default'   => ''
			),
		array(
			'id'        => 'use_header_social_icons',
			'type'      => 'switch',
			'on'        => 'Yes',
			'off'       => 'No',
			'title'     => __('Use Social Icons in Header', 'peadig-framework'),
			'desc'      => __('<p>Turning this option off will remove the social icons from the header and center your logo across the whole row.</p>', 'peadig-framework'),
			'default'   => '1'
			),
		array(
			'id'        => 'use_breadcrumbs',
			'type'      => 'switch',
			'title'     => __('Breadcrumbs', 'peadig-framework'),
			'desc'      => '<p>Add breadcrumbs. If you\'re using Yoast SEO Plugin and have breadcrumbs enabled, Yoast will be used.</p>',
			'default'   => '1'
			),
		array(
			'id'        => 'sidebar_above_content',
			'type'      => 'switch',
			'on'        => 'Enabled',
			'off'       => 'Disabled',
			'title'     => __('Widget area above content', 'peadig-framework'),
			'desc'      => 'Adds a sidebar at the top of the main content container, below the header and above the breadcrumbs. You can add widgets to the sidebar <a href="'.get_admin_url('widgets.php').'">here</a>.',
			),
		array(
			'id'     => 'footer_heading',
			'type'   => 'info',
			'notice' => true,
			'style'  => 'info',
			'title'  => __( 'Footer', 'peadig-framework' )
			),
		array(
			'id'        => 'sidebar_below_content',
			'type'      => 'switch',
			'on'        => 'Enabled',
			'off'       => 'Disabled',
			'title'     => __('Widget area below content', 'peadig-framework'),
			'desc'      => 'Adds a sidebar at the bottom of the main content container, above the footer. You can add widgets to the sidebar <a href="'.get_admin_url().'widgets.php">here</a>.',
			),
		array(
			'id'=>'footer_text',
			'type' => 'editor',
			'title' => __('Footer Text', 'peadig-framework'), 
			'subtitle' => __('You can use Peadig shortcodes here too.', 'peadig-framework'),
			'default' => '&copy; '.date('Y').' [wp-url].',
			),


		array(
			'id'        => 'even_footer_cols',
			'type'      => 'switch',
			'title'     => __('Equal Widget Column Span', 'peadig-framework'),
			'desc'      => 'Enabling this option will force all widgets in the container above content, below content as well as the footer to fit into one row. <strong>Ensure you have not set a column span for all widgets</strong>.',
			'default'   => '1'
			),
		array(
			'id'        => 'footer_well',
			'type'      => 'switch',
			'on'        => 'Enabled',
			'off'       => 'Disabled',
			'title'     => __('Footer Wells', 'peadig-framework'),
			'desc'      => 'adds a well to all footer widgets',
			),
		)
);

$this->sections[] = array(
	'icon' => 'el el-icon-website',
	'title' => __('Columns & Sidebars', 'peadig-framework'),
	'subsection' => true,
	'fields' => array(
		array(
			'id'        => 'fixed_container_width',
			'type'      => 'switch',
			'title'     => __('Fixed Container Width', 'peadig-framework'),
      'default'     => '0',
			),
		array(
			'id'=>'max_container_width',
			'required' => array('fixed_container_width', '=' , '1'),
			'type' => 'dimensions',
			'height' => 'false',
				'units' => array('px','%'), // You can specify a unit value. Possible: px, em, %
				'units_extended' => 'false', // Allow users to select any type of unit
				'title' => __('Max Container Width', 'peadig-framework'),
				'default' => array('width' => 960, 'units'=>'px', )
				),						
		array(
			'id'        =>'main_span',
			'type'      => 'slider',
			'title'     => __('Main Column Span', 'peadig-framework'),
			"default" 	=> "8",
			"min" 		=> "6",
			"step"		=> "1",
			"max" 		=> "10",
			),
		array(
			'id'        => 'second_sidebar',
			'type'      => 'switch',
			'title'     => __('Use Second Sidebar', 'peadig-framework'),
			'desc'      => '<p>When the second sidebar is used, both sidebars share the same column span</p>',
			'default'   => ''
			),

		array(
			'id'        =>'taxonomy_sidebar',
			'type'      => 'image_select',
			'title'     => __('Taxonomy Sidebar Layout', 'peadig-framework'),
			'desc'      => __('This option helps you customise sidebars in all taxonomy templates. This includes all category, tag, date, archive and all custom taxonomies.', 'peadig-framework'),
			'options'   => array(
				'1' => array('alt' => '1 Column', 'img' => ReduxFramework::$_url.'assets/img/1col.png'),
				'2' => array('alt' => '2 Column Left', 'img' => ReduxFramework::$_url.'assets/img/2cl.png'),
				'3' => array('alt' => '2 Column Right', 'img' => ReduxFramework::$_url.'assets/img/2cr.png'),
				'4' => array('alt' => '3 Column Middle', 'img' => ReduxFramework::$_url.'assets/img/3cm.png'),
				/*'5' => array('alt' => '3 Column Left', 'img' => ReduxFramework::$_url.'assets/img/3cl.png'),
				'6' => array('alt' => '3 Column Right', 'img' => ReduxFramework::$_url.'assets/img/3cr.png')*/
				),//Must provide key => value(array:title|img) pairs for radio options
			'default'   => '3'
			),

array(
	'id'        => 'sidebar_well',
	'type'      => 'switch',
	'on'        => 'Enabled',
	'off'       => 'Disabled',
	'title'     => __('Sidebar Wells', 'peadig-framework'),
	'desc'      => 'adds a well to all sidebar widgets',
	),
)
);

$this->sections[] = array(
	'icon' => 'el el-icon-list',
	'title' => __('Navigation Menu', 'peadig-framework'),
	'subsection' => true,
	'fields' => array(
		array(
			'id'        => 'fixed_nav',
			'type'      => 'switch',
			'title'     => __('Fixed Navigation Bar', 'peadig-framework'),
			'default' 	=> 0
			),
		array(
			'id'        => 'fixed_nav_pos',
			'required' => array('fixed_nav', '=' , '1'),
			'type'      => 'button_set',
			'title'     => __('Fixed Nav Position', 'peadig-framework'),
			'options'   => array(
				'top' => __('Top', 'peadig-framework'),
				'bottom' => __('Bottom', 'peadig-framework')
				),
			'default'   => 'top'
			),
		array(
			'id'        => 'inverse_nav',
			'type'      => 'switch',
			'title'     => __('Inverse Navbar', 'peadig-framework'),
			),
		array(
			'id'        => 'justified_nav',
			'type'      => 'switch',
			'title'     => __('Justified Nav Items', 'peadig-framework'),
			'desc'      => __('Enabling this will stretch menu items to fit the width of the container.', 'peadig-framework'),
			'default' 	=> 0
			),
		array(
			'id'        => 'nav_login',
			'type'      => 'switch',
			'title'     => __('Navbar Login Form', 'peadig-framework'),
			'desc'      => __('<p>Turning this on will show the WordPress login form at the right of the navbar.</p>', 'peadig-framework'),
			),
		array(
			'id'        => 'nav_mobile_text',
			'type'      => 'text',
			'title'     => __('Navbar Mobile Text', 'peadig-framework'),
			'desc'      => __('<p>When viewing the site on a mobile device, this is the text to display next to the toggle button.</p>', 'peadig-framework'),
			'default'   => get_bloginfo('name')
			),
		array(
			'id'        => 'nav_css',
			'type'      => 'text',
			'desc'      => 'additional CSS classes for the nav menu',
			'title'     => __('Nav CSS class', 'peadig-framework'),
			'default'   => '',
			'validate'  => 'no_special_chars'
			),
		)
);


# #
# # #
# # # # # # HOMEPAGE OPTION BLOCK
# # #
# #


$this->sections[] = array(
	'icon'          => 'el el-icon-website',
	'icon_class'    => 'icon-large',
	'title'         => __('Template Options', 'peadig-framework'),
	'fields' => array(

/*
			array(
				'id'=>"group",
				'type' => 'group',//doesn't need to be called for callback fields
				'title' => __('Group - BETA', 'peadig-framework'), 
				'subtitle' => __('Group any items together. Experimental. Use at your own risk.', 'peadig-framework'),
				'groupname' => __('Group', 'peadig-framework'), // Group name
				'subfields' => 
					array(
						array(
							'id'=>'switch-fold',
							'type' => 'switch', 
							'title' => __('testing fold with Group', 'peadig-framework'),
							'subtitle'=> __('Look, it\'s on!', 'peadig-framework'),
							"default" 		=> 1,
							),	
						array(
	                        'id'=>'text-group',
	                        'type' => 'text',
	                        'title' => __('Text', 'peadig-framework'), 
	                        'subtitle' => __('Here you put your subtitle', 'peadig-framework'),
	                        'required' => array('switch-fold', '=' , '1'),
							),
						array(
							'id'=>'select-group',
							'type' => 'select',
							'title' => __('Testing select', 'peadig-framework'), 
							'subtitle' => __('Select your themes alternative color scheme.', 'peadig-framework'),
							'options' => array('default.css'=>'default.css', 'color1.css'=>'color1.css'),
							'default' => 'default.css',
							),
						),
				),		
				*/
)

);

$this->sections[] = array(
	'icon' => 'el el-icon-home',
	'title' => __('Homepage', 'peadig-framework'),
	'subsection' => true,
	'fields' => array(
/*
                        array(
                            'id'       => 'opt-homepage-layout-2',
                            'type'     => 'sorter',
                            'title'    => 'Homepage Layout Manager',
                            'desc'     => 'Organize how you want the layout to appear on the homepage',
                            'compiler' => 'true',
                            'options'  => array(
                                'enabled' => array(
                                    'highlights' => 'Highlights',
                                    'slider'     => 'Slider',
                                ),
                                'disabled'  => array(
                                    'staticpage' => 'Static Page',
                                    'services'   => 'Services'
                                ),
                            ),
                        ),

                    array(
                            'id'       => 'opt-media',
                            'required' => array( 'enabled', "=", 'highlights' ),
                            //'required' => array( 'enabled', "=", 'highlights' ),
                            'type'     => 'media',
                            'url'      => true,
                            'title'    => __( 'Media w/ URL', 'redux-framework-demo' ),
                            'compiler' => 'true',
                            //'mode'      => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                            'desc'     => __( 'Basic media uploader with disabled URL input field.', 'redux-framework-demo' ),
                            'subtitle' => __( 'Upload any media using the WordPress native uploader', 'redux-framework-demo' ),
                            'default'  => array( 'url' => 'http://s.wordpress.org/style/images/codeispoetry.png' ),
                            //'hint'      => array(
                            //    'title'     => 'Hint Title',
                            //    'content'   => 'This is a <b>hint</b> for the media field with a Title.',
                            //)
                        ),
*/
array(
	'id'     => 'slider_heading',
	'type'   => 'info',
	'notice' => true,
	'style'  => 'info',
	'icon'   => 'el el-icon-picture',
	'title'  => __( 'Homepage Slider', 'peadig-framework' ),
	'desc'   => __( 'You can use an image slider on the top of your homepage to display your latest posts, a selection of pages or even a NextGEN image gallery.', 'peadig-framework' )
	),
array(
	'id'        => 'use_homepage_slider',
	'type'      => 'switch',
	'on'        => 'Enabled',
	'off'       => 'Disabled',
	'title'     => __('Slider on Homepage', 'peadig-framework'),
	'default'   => '1',
	),
array(
	'id'        => 'full_width_slider',
//	'required' => array('homepage_blocks[slider]', '=' , 'enabled'),
	'required' => array('use_homepage_slider', '=' , '1'),
//	'required' => array('homepage_blocks', '=' , 'enabled'),
//	'required' => array(array('homepage_blocks','slider'),'equals','enabled'),	
	'type'      => 'switch',
	'title'     => __('Full Width Slider', 'peadig-framework')
	),


array(
	'id'        => 'slider_type',
	'required' => array('use_homepage_slider', '=' , '1'),
	'type'      => 'button_set',
	'title'     => __('Type of Slider', 'peadig-framework'),
	'options'   => array(
		'posts' => __('Posts', 'peadig-framework'),
		'pages' => __('Pages', 'peadig-framework'),
		'nextgen' => __('NextGEN', 'peadig-framework'),
		'custom' => __('Custom', 'peadig-framework')
		),
	'desc'      => '<p>Choose what you want to show in the homepage slider.</p>',
	'default'   => 'posts'
	),
array(
	'id'        => 'nextgen_id',
	'required'  => array('slider_type','equals','nextgen'),
	'type'      => 'text',
	'title'     => __('Gallery ID', 'peadig-framework'),
	'desc'      => '<p>This is your numeric NextGEN gallery ID. It will also use the title and description of the images in that gallery to output the title and caption for each image in the slider.</p>',
	'validate'  =>	 'numeric'
	),
array(
	'id'        => 'number_of_slider_items',
	'required'  => array('slider_type','equals','Posts'),
	'type'      => 'slider',
	'title'     => __('Number of Post Items', 'peadig-framework'),
	'desc'      => '<p>If you selected posts above, please choose the number of posts you want in the slider.</p>',
	"default" 	=> "4",
	"min" 		=> "1",
	"step"		=> "1",
	"max" 		=> "10",
	),
array(
	'id'=>'slides',
	'type' => 'slides',
	'required'  => array('slider_type','equals','custom'),
	'title' => __('Homepage Slider', 'peadig-framework'),
	'subtitle'=> __('NOT YET IMPLEMENTED...', 'peadig-framework'),
	'desc' => __('Customize the homepage slider here!', 'peadig-framework')
	),  
array(
	'id'        => 'post_categories',
	'required'  => array('slider_type','equals','posts'),
	'type'      => 'select',	'data' => 'categories',	'multi' => true,
	'title'     => __('Post Categories', 'peadig-framework'),
	'desc'      => '<p>If you selected posts above, please choose the categories you\'d like to be included in the slider.</p>',
	),
array(
	'id'        => 'select_pages',
	'required'  => array('slider_type','equals','pages'),
	'type'      => 'select',	'data' => 'pages',	'multi' => true,
	'title'     => __('Pages Select', 'peadig-framework'),
	'desc'      => '<p>If you selected pages above, please choose the pages you\'d like to be included in the slider.</p>',
	),
array(
	'id'        => 'slider_fixed_height',
	'type'      => 'switch',
	'required' => array('use_homepage_slider', '=' , '1'),
	'title'     => __('Slider Fixed Height', 'peadig-framework'),
	'default'   => '1'
	),
array(
	'id'        => 'slider_height',
	'required' => array('slider_fixed_height', '=' , '1'),
	'type'      => 'slider',
	'title'     => __('Slider Height', 'peadig-framework'),
	'desc'      => 'enter max height in pixels',
	"default" 	=> "400",
	"min" 		=> "100",
	"step"		=> "5",
	"max" 		=> "800",
	),
array(
	'id'        => 'animation',
	'required' => array('use_homepage_slider', '=' , '1'),
	'type'      => 'switch',
	'title'     => __('Animation', 'peadig-framework'),
	'desc'      => '<p>When enabled, the slider will automatically animate by moving to the next slide.</p>',
	'default'   => '1'
	),
array(
	'id'        => 'animation_interval',
	'required' => array('animation', '=' , '1'),
	'type'      => 'slider',
	'title'     => __('Animation Interval', 'peadig-framework'),
	'desc'      => __('enter number in milliseconds.', 'peadig-framework'),
	"default" 	=> "7000",
	"min" 		=> "1000",
	"step"		=> "250",
	"max" 		=> "15000",
	),
array(
	'id'        => 'use_prev_next_buttons',
	'required' => array('use_homepage_slider', '=' , '1'),
	'type'      => 'switch',
	'title'     => __('Prev/Next buttons', 'peadig-framework'),
	'default'   => '1'
	),
array(
	'id'        => 'slider_navigation',
	'required' => array('use_homepage_slider', '=' , '1'),
	'type'      => 'switch',
	'title'     => __('Slider Navigation Bullets', 'peadig-framework'),
	'default'   => '1'
	),
array(
	'id'        => 'additional_slider_css',
	'required' => array('use_homepage_slider', '=' , '1'),
	'type'      => 'text',
	'validate'  => 'no_special_chars',
	'title'     => __('Additional Slider CSS', 'peadig-framework'),
	),
array(
	'id'     => 'jumbotron_heading',
	'type'   => 'info',
	'notice' => true,
	'style'  => 'info',
	'icon'   => 'el el-icon-bullhorn',
	'title'  => __( 'Jumbotron', 'peadig-framework' ),
	'desc'   => __( 'A Jumbotron lets you sell your site "elevator pitch" style in its own well with louder text.', 'peadig-framework' )
	),
array(
	'id'        => 'jumbotron',
	'type'      => 'switch',
	'on'        => 'Enabled',
	'off'       => 'Disabled',
	'title'     => __('Jumbotron', 'peadig-framework'),
	'default'   => '1'
	),
array(
	'id'        => 'full_width_jumbotron',
	'type'      => 'switch',
	'title'     => __('Full Width Jumbotron', 'peadig-framework'),
	'required' => array('jumbotron', '=' , '1'),
	'default'   => ''
	),
array(
	'id'        => 'jumbotron_title',
	'type'      => 'text',
	'title'     => __('Jumbotron Title', 'peadig-framework'),
	'validate'  => 'html',
	'required' => array('jumbotron', '=' , '1'),
	'default'   => 'Welcome to '.get_bloginfo('name')
	),
array(
	'id'        => 'jumbotron_tagline',
	'type'      => 'editor',
	'required' => array('jumbotron', '=' , '1'),
	'title'     => __('Jumbotron Tagline', 'peadig-framework'),
	'default'   => ''
	),
array(
	'id'        => 'use_jumbotron_button',
	'title'     => __('Use Jumbotron Button', 'peadig-framework'),
	'type'      => 'switch',
	'required' => array('jumbotron', '=' , '1'),
	'default'   => '1'
	),
array(
	'id'        => 'jumbotron_button_link',
	'type'      => 'select',	'data' => 'pages',
	'title'     => __('Jumbotron Button Link', 'peadig-framework'),
	'required' => array('jumbotron', '=' , '1'),
	'default'   => ''
	),
array(
	'id'        => 'jumbotron_button_text_override',
	'type'      => 'text',
	'validate'  => 'html',
	'title'     => __('Jumbotron Button Text Override', 'peadig-framework'),
	'required' => array('jumbotron', '=' , '1'),
	'default'   => ''
	),
array(
	'id'        => 'jumbotron_button_css',
	'type'      => 'text',
	'validate'  => 'no_special_chars',
	'title'     => __('Jumbotron Button CSS', 'peadig-framework'),
	'required' => array('jumbotron', '=' , '1'),
	'default'   => 'btn btn-default'
	),
array(
	'id'     => 'services_heading',
	'type'   => 'info',
	'notice' => true,
	'style'  => 'info',
	'icon'   => 'el el-icon-info-sign',
	'title'  => __( 'Services Columns', 'peadig-framework' ),
	'desc'   => __( 'Here you can use 3 columns to link to 3 pages on your site. You can use this to detail 3 main areas of your site.', 'peadig-framework' )
	),
array(
	'id'        => 'services_unit',
	'type'      => 'switch',
	'on'        => 'Enabled',
	'off'       => 'Disabled',
	'title'     => __('Services Columns', 'peadig-framework'),
	'default'   => '1'
	),
array(
	'id'        => 'services_unit_imgsize',
	'required' => array('services_unit', '=' , '1'),
	'type'      => 'text',
	'title'     => __('Services Unit Image Size', 'peadig-framework'),
	'desc'      => 'enter your image size in pixels',
	'validate'  => 'numeric',
	'class'     => 'small-text',
	'default'   => '250'
	),
array(
	'id'        => 'services_unit_imgclass',
	'required' => array('services_unit', '=' , '1'),
	'type'      => 'text',
	'title'     => __('Services Unit Image Size', 'peadig-framework'),
	'validate'  => 'no_special_chars',
	'default'   => 'img-thumbnail',
	'class'     => 'medium-text',
	'desc'      => 'add CSS classes separated by spaces'
	),
array(
	'id'        => 'services_page_1',
	'required' => array('services_unit', '=' , '1'),
	'type'      => 'select',
	'data'      => 'pages',
	'title'     => __('Services Page 1', 'peadig-framework'),
	),
array(
	'id'        => 'services_desc_1',
	'required' => array('services_unit', '=' , '1'),
	'type'      => 'editor',
	'title'     => __('Services 1 Description', 'peadig-framework'),
	'default'   => '',
	'sub_desc'  => 'Leave this blank to use the page\'s excerpt (if one is set)'
	),
array(
	'id'        => 'services_button_1',
	'required' => array('services_unit', '=' , '1'),
	'type'      => 'text',
	'validate'  => 'html',
	'title'     => __('Services 1 Button Text', 'peadig-framework'),
	'default'   => 'Learn More'
	),
array(
	'id'        => 'services_page_2',
	'required' => array('services_unit', '=' , '1'),
	'type'      => 'select',
	'data'      => 'pages',
	'title'     => __('Services Page 2', 'peadig-framework'),
	),
array(
	'id'        => 'services_desc_2',
	'required' => array('services_unit', '=' , '1'),
	'type'      => 'editor',
	'title'     => __('Services 2 Description', 'peadig-framework'),
	'sub_desc'  => 'Leave this blank to use the page\'s excerpt (if one is set)',
	'default'   => ''
	),
array(
	'id'        => 'services_button_2',
	'required' => array('services_unit', '=' , '1'),
	'type'      => 'text',
	'validate'  => 'html',
	'title'     => __('Services 2 Button Text', 'peadig-framework'),
	'default'   => 'Learn More'
	),
array(
	'id'        => 'services_page_3',
	'required' => array('services_unit', '=' , '1'),
	'type'      => 'select',
	'data'      => 'pages',
	'title'     => __('Services Page 3', 'peadig-framework'),
	),
array(
	'id'        => 'services_desc_3',
	'required' => array('services_unit', '=' , '1'),
	'type'      => 'editor',
	'title'     => __('Services 3 Description', 'peadig-framework'),
	'sub_desc'  => 'Leave this blank to use the page\'s excerpt (if one is set)',
	'default'   => ''
	),
array(
	'id'        => 'services_button_3',
	'required' => array('services_unit', '=' , '1'),
	'type'      => 'text',
	'validate'  => 'html',
	'title'     => __('Services 3 Button Text', 'peadig-framework'),
	'default'   => 'Learn More'
	),
array(
	'id'     => 'quote_heading',
	'type'   => 'info',
	'notice' => true,
	'style'  => 'info',
	'icon'   => 'el el-icon-quotes',
	'title'  => __( 'Quote', 'peadig-framework' ),
	'desc'   => __( 'Someone got something to say about your site? Why not show it off with a quote?' )
	),
array(
	'id'        => 'quote',
	'type'      => 'switch',
	'on'        => 'Enabled',
	'off'       => 'Disabled',
	'title'     => __('Quote', 'peadig-framework'),
	'default'   => '1'
	),
array(
	'id'        => 'quote_line_1',
	'required' => array('quote', '=' , '1'),
	'type'      => 'text',
	'validate'  => 'html',
	'title'     => __('Quote Line 1', 'peadig-framework'),
	'default'   => 'Welcome to '.get_bloginfo('name')
	),
array(
	'id'        => 'quote_line_2',
	'required' => array('quote', '=' , '1'),
	'type'      => 'text',
	'validate'  => 'html',
	'title'     => __('Quote Line 2', 'peadig-framework'),
	'default'   => 'Welcome to '.get_bloginfo('description')
	),
array(
	'id'     => 'cont_heading',
	'type'   => 'info',
	'notice' => true,
	'style'  => 'info',
	'icon'   => 'el el-icon-file-edit',
	'title'  => __( 'Content', 'peadig-framework' ),
                           // 'desc'   => __( 'Someone got something to say about your site? Why not show it off with a quote?' )
	),
array(
	'id'        => 'content_on_homepage',
	'type'      => 'switch',
	'on'        => 'Enabled',
	'off'       => 'Disabled',
	'title'     => __('Content on Homepage', 'peadig-framework'),
	'desc'      => '<p>If selected, your front page will display as per your options in the <a href="'.get_admin_url().'options-reading.php" target="_blank">Reading Settings</a></p>',
	'default'   => '1'
	),
array(
	'id'        =>'homepage_sidebar',
	'type'      => 'image_select',
	'required' => array('content_on_homepage', '=' , '1'),
	'title'     => __('Homepage Sidebar Layout', 'peadig-framework'),
	'desc'      => __('Select whether you want sidebars on the homepage and if so, what layout you want this in.', 'peadig-framework'),
	'options'   => array(
		'1' => array('alt' => '1 Column', 'img' => ReduxFramework::$_url.'assets/img/1col.png'),
		'2' => array('alt' => '2 Column Left', 'img' => ReduxFramework::$_url.'assets/img/2cl.png'),
		'3' => array('alt' => '2 Column Right', 'img' => ReduxFramework::$_url.'assets/img/2cr.png'),
		'4' => array('alt' => '3 Column Middle', 'img' => ReduxFramework::$_url.'assets/img/3cm.png'),
		/*'5' => array('alt' => '3 Column Left', 'img' => ReduxFramework::$_url.'assets/img/3cl.png'),
		'6' => array('alt' => '3 Column Right', 'img' => ReduxFramework::$_url.'assets/img/3cr.png')*/
											),//Must provide key => value(array:title|img) pairs for radio options
	'default'   => '3'
	),

)
);


$this->sections[] = array(
	'icon'          => 'el el-icon-file-edit',
	'subsection' => true,
	'title'         => __('Posts & Pages', 'peadig-framework'),
	'desc'          => __('Below are settings within posts and pages. Note that post elements will mirror settings onto any singular template such as custom post types.', 'peadig-framework'),
	'fields' => array(

		array(
			'id'     => 'pages_heading',
			'type'   => 'info',
			'notice' => true,
			'style'  => 'info',
			'icon'   => 'el el-icon-file',
			'title'  => __( 'Pages', 'peadig-framework' ),
			'desc'  => __( 'For social sharing options, please refer to its own subsection', 'peadig-framework' )
			),





		array(
			'id'     => 'posts_heading',
			'type'   => 'info',
			'notice' => true,
			'style'  => 'info',
			'icon'   => 'el el-icon-file-edit',
			'title'  => __( 'Single Posts and Custom Post Types', 'peadig-framework' ),
			'desc'  => __( 'For social sharing options, please refer to its own subsection', 'peadig-framework' )
			),


		array(
			'id'        =>'post_sidebar',
			'type'      => 'image_select',
			'title'     => __('Post Sidebar Layout', 'peadig-framework'),
			'desc'      => __('Although pages can use page templates, posts cannot. This option helps you customise sidebars in posts.', 'peadig-framework'),
			'options'   => array(
				'1' => array('alt' => '1 Column', 'img' => ReduxFramework::$_url.'assets/img/1col.png'),
				'2' => array('alt' => '2 Column Left', 'img' => ReduxFramework::$_url.'assets/img/2cl.png'),
				'3' => array('alt' => '2 Column Right', 'img' => ReduxFramework::$_url.'assets/img/2cr.png'),
				'4' => array('alt' => '3 Column Middle', 'img' => ReduxFramework::$_url.'assets/img/3cm.png'),
				/*'5' => array('alt' => '3 Column Left', 'img' => ReduxFramework::$_url.'assets/img/3cl.png'),
				'6' => array('alt' => '3 Column Right', 'img' => ReduxFramework::$_url.'assets/img/3cr.png')*/
											),//Must provide key => value(array:title|img) pairs for radio options
			'default'   => '3'
			),
array(
	'id'        => 'readmore',
	'type'      => 'text',
	'desc'      => 'When using excerpts, you can use a link continuing into the post. Set that text here...',
	'title'     => __('"Read More" text', 'peadig-framework'),
	'default'   => '[read more...]'
	),
array(
	'id'        => 'featimg',
	'type'      => 'switch',
	'desc'      => 'Show featured image before post content',
	'title'     => __('Show Featured Image', 'peadig-framework'),
	'default'   => '1'
	),
array(
	'id'        => 'old_post_notif',
	'type'      => 'switch',
	'title'     => __('Old post notification', 'peadig-framework'),
	'desc'      => '<p>Turning this on shows an alert at the top of a post older than XX days old</p>',
	'default'   => ''
	),
array(
	'id'        => 'how_many_days',
	'required' => array('old_post_notif', '=' , '1'),
	'type'      => 'slider',
	'title'     => __('Days to Trigger Alert', 'peadig-framework'),
	"default" 	=> "182",
	"min" 		=> "14",
	"step"		=> "7",
	"max" 		=> "364",
	),
array(
	'id'        => 'featured_image_class',
	'type'      => 'text',
	'desc'      => 'adds CSS to the all featured images',
	'title'     => __('Featured Image CSS Class', 'peadig-framework'),
	'default'   => 'img-thumbnail',
	'validate'  => 'no_special_chars'
	),
)
);



# #
# # #
# # # # # # COMMENTS OPTION BLOCK
# # #
# #
$this->sections[] = array(
	'icon'          => 'el el-icon-comment',
	'subsection' => true,
	'title'         => __('Comments', 'peadig-framework'),
	'desc'          => __('<p class="description">WordPress may not be the commenting system you want to use, or you may want to use more than one. This area will help you set up not only WordPress comments, but also Facebook, Google+ and Disqus.</p>', 'peadig-framework'),
	'fields' => array(
		array(
			'id'        => 'comments_pages',
			'type'      => 'switch',
			'title'     => __('Show Comments on Pages', 'peadig-framework'),
			'default'   => '0'
			),



/*
		array(
			'id' => 'comment_selection',
			'type' => 'sortable',
		        'mode' => 'checkbox', // checkbox or text
		        'title' => __('Commenting Services', 'peadig-framework'),
		        'desc' => __('Choose which commenting systems you want to use, and which order you want them to appear in tabs. The option at the top will act as the active commenting service.', 'peadig-framework'),
		        'options' => array(
		        	'wp' => 'WordPress',
		        	'fb' => 'Facebook',
		        	'gplus' => 'Google+',
		        	'disqus' => 'Disqus',
		        	),
		        'default' => array('1' => '1', '2' => '1', '3' => '1', '4' => '0')
		        ),	        
*/






array(
	'id'        => 'pdwp_comments',
	'type'      => 'switch',
	'title'     => __('WordPress Comments', 'peadig-framework'),
	'default'   => '1'
	),
array(
	'id'        => 'pdfb_comments',
	'type'      => 'switch',
	'title'     => __('Facebook Comments', 'peadig-framework'),
	'default'   => '1'
	),
array(
	'id'        => 'pdfb_comnum',
	'required' => array('pdfb_comments', '=' , '1'),
	'type'      => 'button_set',
	'title'     => __('Facebook Comments Number', 'peadig-framework'),
	'options'   => array(
		'1' => __('1', 'peadig-framework'),
		'2' => __('2', 'peadig-framework'),
		'3' => __('3', 'peadig-framework'),
		'4' => __('4', 'peadig-framework'),
		'5' => __('5', 'peadig-framework'),
		'10' => __('10', 'peadig-framework'),
		'15' => __('15', 'peadig-framework'),
		'20' => __('20', 'peadig-framework')
		),
	'default'   => '5'
	),
array(
	'id'        => 'fbcom_color',
	'required' => array('pdfb_comments', '=' , '1'),
	'type'      => 'button_set',
	'title'     => __('Facebook Comments Color Scheme', 'peadig-framework'),
	'sub_desc'  => __('', 'peadig-framework'),
	'options'   => array(
		'light' => __('Light', 'peadig-framework'),
		'dark' => __('Dark', 'peadig-framework')
		),
	'default'   => 'light'
	),
array(
	'id'        => 'pdgplus_comments',
	'type'      => 'switch',
	'title'     => __('Google+ Comments', 'peadig-framework'),
	'default'   => '1'
	),
array(
	'id'        => 'disqus_comments',
	'type'      => 'switch',
	'title'     => __('Disqus', 'peadig-framework'),
	'default'   => ''
	),
array(
	'id'        => 'disqus_shortname',
	'required' => array('disqus_comments', '=' , '1'),
	'type'      => 'text',
	'title'     => __('Disqus Shortname', 'peadig-framework'),
	'switch'    => true,
	'default'   => '',
	'class'     => 'medium-text'
	),
array(
	'id'        => 'comments_tabs',
	'type'      => 'switch',
	'title'     => __('Use Tabs to split Comment Systems', 'peadig-framework'),
	'desc'      => '<p>Instead of outputting comment systems underneath each other you can put each of them in their own tab.</p>',
	'default'   => '1'
	),
array(
	'id'        => 'active_comment_tab',
	'required' => array('comments_tabs', '=' , '1'),
	'type'      => 'button_set',
	'title'     => __('Active Comments Tab', 'peadig-framework'),
	'options'   => array(
		'wpcom' => __('WordPress', 'peadig-framework'),
		'fbcom' => __('Facebook', 'peadig-framework'),
		'gplus' => __('Google+', 'peadig-framework'),
		'disqus' => __('Disqus', 'peadig-framework')
		),
	'desc'      => '<p>Choose which commenting system you want to appear by default in the content tabs as well as being cited in archive and summary info. Note that Google+ will not show the comment count as a link or with any formatting.</p>',
	'default'   => 'wpcom'
	),
array(
	'id'        => 'pdwp_com_title',
	'required' => array('comments_tabs', '=' , '1'),
	'type'      => 'text',
	'title'     => __('Tab Title', 'peadig-framework'),
	'desc'      => '<p>When using comment tabs, if you want to change the title from WordPress to somthing else, you can set that here.</p>',
	'switch'    => true,
	'default'   => 'WordPress',
	),
array(
	'id'        => 'comments_counters',
	'type'      => 'switch',
	'title'     => __('Use Comment Counters in Tabs', 'peadig-framework'),
	'desc'      => '<p>This option outputs the number of comments for each comment system in brackets. Note that we have excluded Google+ comments here as it breaks the tab layout.</p>',
	'default'   => '1'
	),
)
);

$this->sections[] = array(
	'type' => 'divide',
	);

# #
# # #
# # # # # # AUTHOR INFO
# # #
# #

$this->sections[] = array(
	'icon'          => 'el el-icon-user',
	'icon_class'    => 'icon-large',
	'title'         => __('Author Info', 'peadig-framework'),
	'desc'          => __('<p class="description">Each post is written by an author. This section lets you set whether you want author info included within posts.</p>', 'peadig-framework'),
	'fields' => array(
		array(
			'id'        => 'post_meta_top',
			'type'      => 'switch',
			'on'        => 'Enabled',
			'off'       => 'Disabled',
			'title'     => __('Above Posts', 'peadig-framework'),
			'default'   => '1'
			),
		array(
			'id'        => 'meta_top_well',
			'required' => array('post_meta_top', '=' , '1'),
			'type'      => 'switch',
			'on'        => 'Yes',
			'off'       => 'No',
			'title'     => __('Put top author info inside a well', 'peadig-framework'),
			'desc'      => 'adds a well to the author bio info at the top of the post',
			'default'   => '1'
			),
		array(
			'id'        => 'post_meta_bottom',
			'type'      => 'switch',
			'on'        => 'Enabled',
			'off'       => 'Disabled',
			'title'     => __('Below posts', 'peadig-framework'),
			'default'   => '1'
			),
		array(
			'id'        => 'post_meta_archives',
			'type'      => 'switch',
			'on'        => 'Enabled',
			'off'       => 'Disabled',
			'title'     => __('Within category/tag/archive pages', 'peadig-framework'),
			'default'   => '1'
			),
		array(
			'id'        => 'avatar_class',
			'type'      => 'text',
			'desc'      => 'adds CSS to the all avatars, including author and commenters',
			'title'     => __('Avatar CSS Class', 'peadig-framework'),
			'default'   => 'img-thumbnail',
			'validate'  => 'no_special_chars'
			),
		array(
			'id'     => 'authorfollow_heading',
			'type'   => 'info',
			'notice' => true,
			'style'  => 'info',
			'icon'   => 'el el-icon-thumbs-up',
			'title'  => __( 'Follow Buttons', 'peadig-framework' ),
			'desc'   => __( 'These buttons will only appear if an author connects their profile to social networks within the profile settings of WordPress.', 'peadig-framework' )
			),
		array(
			'id'        => 'follow_btns_top',
			'required' => array('post_meta_top', '=' , '1'),
			'type'      => 'switch',
			'on'        => 'Yes',
			'off'       => 'No',
			'title'     => __('Include at top of post/page', 'peadig-framework')
			),
		array(
			'id'        => 'follow_btns_bottom',
			'required' => array('post_meta_bottom', '=' , '1'),
			'type'      => 'switch',
			'on'        => 'Yes',
			'off'       => 'No',
			'title'     => __('Include at bottom of post/page', 'peadig-framework'),
			'default'   => '1'
			),
		array(
			'id'        => 'meta_twitter_follow',
			'type'      => 'switch',
			'on'        => 'Yes',
			'off'       => 'No',
			'title'     => __('<span class="el el-icon-twitter"></span> Twitter Follow Button', 'peadig-framework'),
			'default'   => '1'
			),
		array(
			'id'        => 'meta_facebook_follow',
			'type'      => 'switch',
			'on'        => 'Yes',
			'off'       => 'No',
			'title'     => __('<span class="el el-icon-facebook"></span> Facebook Follow Button', 'peadig-framework'),
			'default'   => '1'
			),
		array(
			'id'        => 'meta_gplus_follow',
			'type'      => 'switch',
			'on'        => 'Yes',
			'off'       => 'No',
			'title'     => __('<span class="el el-icon-googleplus"></span> Google+ Circles Button', 'peadig-framework'),
			'default'   => '1'
			),
		array(
			'id'        => 'meta_linkedin_follow',
			'type'      => 'switch',
			'on'        => 'Yes',
			'off'       => 'No',
			'title'     => __('<span class="el el-icon-linkedin"></span> Linkedin Profile Icon', 'peadig-framework'),
			'default'   => '1'
			),
		)
);


# #
# # #
# # # # # # SOCIAL DATA & SHARING
# # #
# #


$this->sections[] = array(
	'icon'          => 'el el-icon-group',
	'icon_class'    => 'icon-large',
	'title'         => __('Social Settings', 'peadig-framework'),
	'fields' => array(
		)

	);

$this->sections[] = array(
	'icon'          => 'el el-icon-info-circle',
	'icon_class'    => 'icon-large',
	'subsection'	=> 'true',
	'title'         => __('Social Structured Data', 'peadig-framework'),
	'fields' => array(
		array(
			'id'        => 'social_structured_data',
			'type'      => 'switch',
			'on'        => 'Yes',
			'off'       => 'No',
			'title'     => __('Use Social Structured Data', 'peadig-framework'),
			'desc'      => __('Social structured data helps you provide more detail to social networking sites for when a post or page is shared with them. Enabling this option will fill out all that data for you. You can set social structured data up within the Peadig META when editing pages or posts.', 'peadig-framework'),
			'default'   => '1'
			),
		array(
			'id'        => 'default_thumbnail_image',
			'required' => array('social_structured_data', '=' , '1'),
			'type'      => 'media',
      'title'     => __('Homepage / Default Thumbnail Image', 'peadig-framework'),
      'desc'      => __('This image is used if no image thumbnail is set and there is no featured image.  If none is set here your site logo will be used.', 'peadig-framework')
			),
    array(
      'id'        => 'homepage_ssd_title',
      'required' => array('social_structured_data', '=' , '1'),
      'type'      => 'text',
      'desc'      => __('When you share the homepage on social networks, this is the title the link would show', 'peadig-framework'),
      'title'     => __('Homepage Social Title', 'peadig-framework'),
      'default'   => get_bloginfo('name'),
      'validate'  => 'no_special_chars'
      ),
    array(
      'id'        => 'homepage_ssd_desc',
      'required' => array('social_structured_data', '=' , '1'),
      'type'      => 'text',
      'desc'      => __('When you share the homepage on social networks, this is the description the link would show', 'peadig-framework'),
      'title'     => __('Homepage Social Description', 'peadig-framework'),
      'default'   => get_bloginfo('description'),
      'validate'  => 'no_special_chars'
      ),
		array(
			'id'        => 'sharing_posts',
			'type'      => 'switch',
			'on'        => 'Yes',
			'off'       => 'No',
			'title'     => __('Share buttons in posts', 'peadig-framework'),
			'default'   => '1'
			),
		array(
			'id'        => 'sharing_pages',
			'type'      => 'switch',
			'on'        => 'Yes',
			'off'       => 'No',
			'title'     => __('Share buttons in pages', 'peadig-framework'),
			),
		array(
			'id'        => 'share_before',
			'type'      => 'switch',
			'on'        => 'Yes',
			'off'       => 'No',
			'title'     => __('Share buttons before content', 'peadig-framework'),
			'default'   => '1'
			),
		array(
			'id'        => 'share_after',
			'type'      => 'switch',
			'on'        => 'Yes',
			'off'       => 'No',
			'title'     => __('Share buttons after content', 'peadig-framework'),
			'default'   => '1'
			),
		array(
			'id'        => 'sharing_archives',
			'type'      => 'switch',
			'on'        => 'Yes',
			'off'       => 'No',
			'title'     => __('Share buttons in categeory/tag/author/archive pages', 'peadig-framework'),
			'default'   => '1'
			),
		)

);

$this->sections[] = array(
	'icon'          => 'el el-icon-share',
	'icon_class'    => 'icon-large',
	'subsection'	=> 'true',
	'title'         => __('Sharing Buttons', 'peadig-framework'),
	'desc'          => __('<p class="description">Social Sharing Buttons</h3><p class="description">Just toggle which social sharing buttons you want to use for posts and/or pages.</p>', 'peadig-framework'),
	'fields' => array(
		array(
			'id'        => 'twitter_tweet',
			'type'      => 'switch',
			'on'        => 'On',
			'off'       => 'Off',
			'title'     => __('<span class="el el-icon-twitter"></span> Twitter Tweet', 'peadig-framework'),
			'default'   => '1'
			),
		array(
			'id'        => 'facebook_like',
			'type'      => 'switch',
			'on'        => 'On',
			'off'       => 'Off',
			'title'     => __('<span class="el el-icon-facebook"></span> Facebook Like', 'peadig-framework'),
			'default'   => '1'
			),
		array(
			'id'        => 'facebook_share',
			'type'      => 'switch',
			'on'        => 'On',
			'off'       => 'Off',
			'title'     => __('<span class="el el-icon-facebook"></span> Facebook Share', 'peadig-framework'),
			'default'   => '1'
			),
		array(
			'id'        => 'google_plusone',
			'type'      => 'switch',
			'on'        => 'On',
			'off'       => 'Off',
			'title'     => __('<span class="el el-icon-googleplus"></span> Google +1', 'peadig-framework'),
			'default'   => '1'
			),
		array(
			'id'        => 'buffer_button',
			'type'      => 'switch',
			'on'        => 'On',
			'off'       => 'Off',
			'title'     => __('<span class="el el-icon-share"></span> Buffer', 'peadig-framework'),
			'default'   => '1'
			),
		array(
			'id'        => 'linkedin_share',
			'type'      => 'switch',
			'on'        => 'On',
			'off'       => 'Off',
			'title'     => __('<span class="el el-icon-linkedin"></span> Linkedin Share', 'peadig-framework'),
			'default'   => '1'
			),
		array(
			'id'        => 'pocket_button',
			'type'      => 'switch',
			'on'        => 'On',
			'off'       => 'Off',
			'title'     => __('<span class="el el-icon-share"></span> Pocket (Read it Later)', 'peadig-framework'),
			'default'   => '1'
			),
		array(
			'id'        => 'pinterest_pinit',
			'type'      => 'switch',
			'on'        => 'On',
			'off'       => 'Off',
			'title'     => __('<span class="el el-icon-pinterest"></span> Pinterest', 'peadig-framework'),
			),
		array(
			'id'        => 'stumble_upon',
			'type'      => 'switch',
			'on'        => 'On',
			'off'       => 'Off',
			'title'     => __('<span class="el el-icon-stumbleupon"></span> Stumbleupon', 'peadig-framework'),
			),
		array(
			'id'        => 'reddit_share',
			'type'      => 'switch',
			'on'        => 'On',
			'off'       => 'Off',
			'title'     => __('<span class="el el-icon-reddit"></span> Reddit', 'peadig-framework'),
			),
		)
);



$this->sections[] = array(
	'type' => 'divide',
	);


# #
# # #
# # # # # # DEVELOPER OPTION BLOCK
# # #
# #
$this->sections[] = array(
	'icon'          => ' el-icon-fork',
	'icon_class'    => 'icon-large',
	'title'         => __('Developer Options', 'peadig-framework'),
	'fields' => array(
		array(
			'id'        =>'tracking_code',
			'type' => 'ace_editor',
			'mode' => 'javascript',
			'theme' => 'chrome',
			'title'     => __('Tracking Code', 'peadig-framework'),
			'desc'      => __('Paste your Google Analytics (or other) tracking code here. This will be added into the footer template of your theme.', 'peadig-framework'),
			),
		array(
			'id'        =>'header_js',
			'type' => 'ace_editor',
			'mode' => 'javascript',
			'theme' => 'chrome',
			'title'     => __('Global Header JS', 'peadig-framework'),
			'desc'      => __('Add your sitewide header Javascript here', 'peadig-framework'),
			),
		array(
			'id'        =>'footer_js',
			'type' => 'ace_editor',
			'mode' => 'javascript',
			'theme' => 'chrome',
			'title'     => __('Global Footer JS', 'peadig-framework'),
			'desc'      => __('Add your sitewide footer Javascript here', 'peadig-framework'),
			),
		array(
			'id'        => 'bootstrap_css',
			'type'      => 'switch',
			'title'     => __('Bootstrap CSS', 'peadig-framework'),
			'desc'      => 'Leave this on to output the Bootstrap CSS file',
			'default'   => '1',
			),
		array(
			'id'        => 'bootstrap_js',
			'type'      => 'switch',
			'title'     => __('Bootstrap JS', 'peadig-framework'),
			'desc'      => 'Leave this on to output the Bootstrap JS file',
			'default'   => '1',
			),
		array(
			'id'        => 'use_fontawesome',
			'type'      => 'switch',
			'title'     => __('Font Awesome', 'peadig-framework'),
			'desc'      => '<p><a href="http://fontawesome.io" target="_blank">Font Awesome</a> is the iconic font designed for Bootstrap, offering 361+ icons via a CSS file (and an additional shortcode to use).</p>',
			'default'   => '1',
			),
		array(
			'id'        => 'bootstrap_cdn',
			'type'      => 'switch',
			'title'     => __('Use CDN Networks', 'peadig-framework'),
			'desc'      => '<p>Bootstrap files by default are loaded from your own servers. <a href="http://www.bootstrapcdn.com/" target="_blank">Bootstrap CDN</a> provide free CDN support for Bootstrap\'s CSS and JavaScript (as well as Font Awesome) should you wish to use it.</p>'
			),
		array(
			'id'        => 'noh1',
			'type'      => 'switch',
			'title'     => __('Disable H1 heading within singular templates', 'peadig-framework'),
			'desc'      => '<p>You may sometimes want to insert the H1 for singular templates (ie posts and pages) elsewhere within your theme. If you do, turn this off</p>',
			'default'   => ''
			),

		array(
			'id'        => 'rel_external',
			'type'      => 'switch',
			'title'     => __('Rel External in New window', 'peadig-framework'),
			'desc'      => '<p>To open new windows, valid HTML asks that you use <code>rel="external"</code> rather than <code>target="_blank"</code>. Keep this on to open link with <code>rel="external"</code> in a new window.</p>',
			'default'   => '1',
			),
		array(
			'id'        => 'footer_att',
			'type'      => 'switch',
			'title'     => __('Attribution Link', 'peadig-framework'),
			'desc'      => '<p>Adds a link to the Peadig homepage at the bottom right of the footer.</p>',
			'default'   => '1',
			),
		array(
			'id'        => 'wpautop',
			'type'      => 'switch',
			'on'        => 'On',
			'off'       => 'Off',
			'title'     => __('Use wpautop()', 'peadig-framework'),
			'desc'      => '<p>Turn this option off to remove the wpautop() fucntion from being used on your site.</p>',
			'default'   => '1',
			),
    )
);

/*
$this->sections[] = array(
	'icon' => 'el el-icon-key',
	'icon_class' => 'icon-large',
	'title' => __('License Keys', 'peadig-framework'),
	'fields' => array(
		array(
			'id'=>'peadig_edd',
			'type' => 'edd_license',
			'title' => __('Peadig License', 'peadig-framework'), 
			'subtitle' => __('Please enter your license key.', 'peadig-framework'),
			),


		)
	);
*/


if(isset($theme_options)){

	$section[] = $theme_options;

}

$tabs = array();

if (function_exists('wp_get_theme')){
	$theme_data = wp_get_theme();
	$theme_uri = $theme_data->get('ThemeURI');
	$description = $theme_data->get('Description');
	$author = $theme_data->get('Author');
	$version = $theme_data->get('Version');
	$tags = $theme_data->get('Tags');
}else{
	$theme_data = wp_get_theme(trailingslashit(get_stylesheet_directory()).'style.css');
	$theme_uri = $theme_data['URI'];
	$description = $theme_data['Description'];
	$author = $theme_data['Author'];
	$version = $theme_data['Version'];
	$tags = $theme_data['Tags'];
}	

$theme_info = '<div class="peadig-framework-section-desc">';
$theme_info .= '<p class="redux-opts-item-data item-description">
<h3>Peadig is developed by:</h3>
<ul>
<li><strong>Alex Moss</strong><br />As well as being the Co-Founder of Peadig, Alex is the Director at <strong><a href="http://firecask.com/" target="_blank">FireCask</a></strong>, a digital agency based in Manchester, and has worked in Search since 2007. He is also known for developing WordPress plugins and developing some wonderful WordPress sites (using Peadig of course). Alex also writes for publications such as The Guardian, Huffington Post, Moz, Econsultancy, and 123reg.</li>
<li><strong>Shane Jones</strong><br />Shane Jones is Co-Founder of Peadig and has over 14 years experience in Web Development. He specialises in WordPress Theme and Plugin Development and Social Media Apps. Shane has built apps for global companies such as Nikon, Tefal, Del Monte, Gumtree and many many more.</li>
</ul>
<h3>Attribution</h3>
<p>As well as being powered by WordPress, Peadig combines a number of things to make sure it\'s perfect. This includes the following:</p>
<ul>
<li><strong><a href="http://getbootstrap.com/" target="_blank">Bootstrap</a> v3.3.7</strong><br />Sleek, intuitive, and powerful mobile first front-end framework for faster and easier web development.</li>
<li><strong><a href="http://fontawesome.io/" target="_blank">Font Awesome</a> v4.7.0</strong><br />Font Awesome gives you scalable vector icons that can instantly be customized &#45; size, color, drop shadow, and anything that can be done with the power of CSS.</li>
<li><strong><a href="http://reduxframework.com/" target="_blank">Redux Framework</a> v3.6.8</strong><br />Redux is a simple, truly extensible options framework for WordPress themes and plugins; and is built on the WordPress Settings API. It also powers this options page :)</li>
</ul>
</p>';
$theme_info .= '</div>';


$this->sections[] = array(
	'icon' => 'el el-info-circle',
	'title' => __('Theme Information', 'peadig-framework'),
	'fields' => array(
		array(
			'id'=>'raw_new_info',
			'type' => 'raw',
			'content' => $theme_info,
			)
		),   
	);
}

public function setHelpTabs() {

            // Custom page help tabs, displayed using the help API. Tabs are shown in order of definition.
	$this->args['help_tabs'][] = array(
		'id'        => 'redux-help-tab-1',
		'title'     => __('Theme Information 1', 'redux-framework-demo'),
		'content'   => __('<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo')
		);

	$this->args['help_tabs'][] = array(
		'id'        => 'redux-help-tab-2',
		'title'     => __('Theme Information 2', 'redux-framework-demo'),
		'content'   => __('<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo')
		);

            // Set the help sidebar
	$this->args['help_sidebar'] = __('<p>This is the sidebar content, HTML is allowed.</p>', 'redux-framework-demo');
}

public function setArguments() {

            $theme = wp_get_theme(); // For use with some settings. Not necessary.

            $this->args = array(
                // TYPICAL -> Change these values as you need/desire
                'opt_name'          => 'peadig',            // This is where your data is stored in the database and also becomes your global variable name.
                'display_name'      => $theme->get('Name'),     // Name that appears at the top of your panel
                'display_version'   => $theme->get('Version'),  // Version that appears at the top of your panel
                'menu_type'         => 'menu',                  //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
                'allow_sub_menu'    => true,                    // Show the sections below the admin menu item or not
                'menu_title'        => __('Peadig', 'peadig-framework'),
                'page_title'        => __('Peadig', 'peadig-framework'),
                
                // You will need to generate a Google API key to use this feature.
                // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
                'google_api_key' => 'AIzaSyAX_2L_UzCDPEnAHTG7zhESRVpMPS4ssII', // Must be defined to add google fonts to the typography module
                
                'async_typography'  => false,                    // Use a asynchronous font on the front end or font string
                'admin_bar'         => true,                    // Show the panel pages on the admin bar
                'global_variable'   => '',                      // Set a different name for your global variable other than the opt_name
                'dev_mode'          => false,                    // Show the time the page took to load, etc
                'customizer'        => true,                    // Enable basic customizer support
                
                // OPTIONAL -> Give you extra features
                'page_priority'     => '31',                    // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
                //'page_parent'       => 'themes.php',            // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
                'page_permissions'  => 'manage_options',        // Permissions needed to access the options panel.
                'menu_icon'         => get_template_directory_uri() . '/img/icon-16x16.png',                      // Specify a custom URL to an icon
                'last_tab'          => '',                      // Force your panel to always open to a specific tab (by id)
                'page_icon'         => 'icon-themes',           // Icon displayed in the admin panel next to your menu_title
                'page_slug'         => 'peadig_options',              // Page slug used to denote the panel
                'save_defaults'     => true,                    // On load save the defaults to DB before user clicks save or not
                'default_show'      => false,                   // If true, shows the default value next to each field that is not the default value.
                'default_mark'      => '',                      // What to print by the field's title if the value shown is default. Suggested: *
                'show_import_export' => true,                   // Shows the Import/Export panel when not used as a field.
                
                // CAREFUL -> These options are for advanced use only
                'transient_time'    => 60 * MINUTE_IN_SECONDS,
                'output'            => true,                    // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
                'output_tag'        => true,                    // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
                // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.
                
                // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
                'database'              => '', // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
                'system_info'           => false, // REMOVE

                // HINTS
                'hints' => array(
                	'icon'          => 'icon-question-sign',
                	'icon_position' => 'right',
                	'icon_color'    => 'lightgray',
                	'icon_size'     => 'normal',
                	'tip_style'     => array(
                		'color'         => 'light',
                		'shadow'        => true,
                		'rounded'       => false,
                		'style'         => '',
                		),
                	'tip_position'  => array(
                		'my' => 'top left',
                		'at' => 'bottom right',
                		),
                	'tip_effect'    => array(
                		'show'          => array(
                			'effect'        => 'slide',
                			'duration'      => '500',
                			'event'         => 'mouseover',
                			),
                		'hide'      => array(
                			'effect'    => 'slide',
                			'duration'  => '500',
                			'event'     => 'click mouseleave',
                			),
                		),
                	)
);


            // SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
$this->args['share_icons'][] = array(
	'url'   => 'http://twitter.com/peadig',
	'title' => 'Follow Us on Twitter',
	'icon'  => 'el el-icon-twitter'
                //'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
	);
$this->args['share_icons'][] = array(
	'url'   => 'http://www.facebook.com/peadig',
	'title' => 'Like us on Facebook',
	'icon'  => 'el el-icon-facebook'
	);
$this->args['share_icons'][] = array(
	'url'   => 'https://plus.google.com/+Peadig',
	'title' => 'Follow us on Google+',
	'icon'  => 'el el-icon-googleplus'
	);
$this->args['share_icons'][] = array(
	'url'   => 'https://www.linkedin.com/company/peadig',
	'title' => 'Find us on LinkedIn',
	'icon'  => 'el el-icon-linkedin'
	);


            // Panel Intro text -> before the form
if (!isset($this->args['global_variable']) || $this->args['global_variable'] !== false) {
	if (!empty($this->args['global_variable'])) {
		$v = $this->args['global_variable'];
	} else {
		$v = str_replace('-', '_', $this->args['opt_name']);
	}
	$this->args['intro_text'] = sprintf(__('<p>Welcome to the Peadig options panel. <a href="'.home_url().'/wp-admin/themes.php?page=peadig-license">Click here to view the separate license admin panel.</a></p>', 'peadig-framework'), $v);
} else {
	$this->args['intro_text'] = __('<p>This text is displayed above the options panel. It isn\'t required, but more info is always better! The intro_text field accepts all HTML.</p>', 'peadig-framework');
}

            // Add content after the form.
$this->args['footer_text'] = __('<p>Do you develop with Peadig? Why not <a href="http://peadig.com/developers-required-wordpress-child-themes/" target="_blank">develop child themes</a> and make some money? You can also join our <a href="http://peadig.com/affiliates/" target="_blank">affiliate program</a>.</p>', 'peadig-framework');
}

}

global $reduxConfig;
$reduxConfig = new Redux_Framework_sample_config();
}

/**
  Custom function for the callback referenced above
 */
  if (!function_exists('redux_my_custom_field')):
  	function redux_my_custom_field($field, $value) {
  		print_r($field);
  		echo '<br/>';
  		print_r($value);
  	}
  	endif;

/**
  Custom function for the callback validation referenced above
 * */
  if (!function_exists('redux_validate_callback_function')):
  	function redux_validate_callback_function($field, $value, $existing_value) {
  		$error = false;
  		$value = 'just testing';

        /*
          do your validation

          if(something) {
            $value = $value;
          } elseif(something else) {
            $error = true;
            $value = $existing_value;
            $field['msg'] = 'your custom error message';
          }
         */

          $return['value'] = $value;
          if ($error == true) {
          	$return['error'] = $field;
          }
          return $return;
        }
        endif;
