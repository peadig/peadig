<?php get_header(); ?>
<div class="container">

    <div id="primary" class="col-lg-12">
      <div class="row" role="main">
          <?php CustomHook::search_before_loop() ?>
          <?php if (have_posts()) : ?>


              <?php CustomHook::search_before_header() ?>
              <h2>Search Results - "<?php the_search_query(); ?>"</h2>
              <?php CustomHook::search_after_header() ?>

            <?php DefaultHook::numeric_posts_nav(); ?>

            <?php while (have_posts()) : the_post(); ?>

                <?php CustomHook::leftsidebarright_before_sidebar_1() ?>

                      <article <?php post_class() ?> id="post-<?php the_ID(); ?>">
                          <h2 class="entry-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>

                          <div class="entry">

                              <?php if ( SBUTTONS == "on" ) {

                                  $json = wp_remote_fopen("http://api.sharedcount.com/?url=" . rawurlencode(get_permalink()));

                                  $counts = json_decode($json, true);

                                  echo "This post has " . $counts["Twitter"] ." tweets, " . $counts["Facebook"]["like_count"] . " likes, and ". $counts["GooglePlusOne"] . "+1's<hr>";

                              } ?>

                          </div>

                          <footer class="postmetadata">
                              <?php the_tags('Tags: ', ', ', '<br />'); ?>
                              Posted in <?php the_category(', ') ?> |
                              <?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?>
                          </footer>

                      </article>

                      <hr>

                  <?php endwhile; ?>

                      <?php DefaultHook::numeric_posts_nav(); ?>

                  <?php else : ?>

                      <h1>Not Found</h1>

                  <?php endif; ?>

          <?php CustomHook::search_after_loop() ?>



    </div>
</div>



<?php DefaultHook::footer(); ?>