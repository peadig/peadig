<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
    <div class="row">
        <div class="col-xs-12">
            <div class="input-group"><input type="text" class="form-control" id="appendedInputButtons" placeholder="Search..." name="s"><span class="input-group-btn"><button class="btn btn-primary" type="submit" id="searchsubmit"><?php
            	$pd_options = get_option('peadig');
            	if (!empty($pd_options['use_fontawesome'])) {
            		echo '<i class="fa fa-search"></i>';
            	} else {
            		echo 'Search';
            	}
            ?></button></span>
            </div>
        </div>
    </div>
</form>