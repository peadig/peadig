<?php
/*
 * If the current post is protected by a password and the visitor has not yet
 * entered the password we will return early without loading the comments.
 */
if ( post_password_required() ) {
    return;
}
?>
<div id="comments" class="comments-area">
    <?php 
    $pd_options = get_option('peadig');
    ?>
    <?php CustomHook::comments_before_heading() ?>
    <h3 class="comments-title">
        <?php
        comment_form_title( 'Comments', 'Comment on %s' );
        ?>
    </h3>
    <?php if ( ! comments_open() ) : ?>
        <p class="no-comments"><?php _e( 'Comments are closed.', 'twentyfourteen' ); ?></p>
    <?php else : ?>
    <?php CustomHook::comments_after_heading() ?>
    <?php CustomHook::comments_before_comment_tab() ?>
    <?php if (!empty($pd_options['comments_tabs'])) { ?>
    <ul id="CommentTab" class="nav nav-tabs">
        <?php if (!empty($pd_options['pdwp_comments'])) { ?>
        <li<?php
        if ($pd_options['active_comment_tab']=='wpcom') {
            echo ' class="active"';
        } ?>><a href="#wpcom" data-toggle="tab"><?php echo $pd_options['pdwp_com_title']; ?><?php
        if (!empty($pd_options['comments_counters'])) {
            ?> (<?php comments_number('0', '1', '%' );?>)<?php
        } ?></a></li>

        <?php }

        if (!empty($pd_options['pdfb_comments'])) { ?>

        <li<?php
        if ($pd_options['active_comment_tab']=='fbcom') {
            echo ' class="active"';
        } ?>><a href="#fbcom" data-toggle="tab">Facebook<?php
        if (!empty($pd_options['comments_counters'])) {
            ?> (<fb:comments-count href="<?php echo get_permalink(); ?>"></fb:comments-count>)<?php
        } ?></a></li>

        <?php }

        if (!empty($pd_options['pdgplus_comments'])) { ?>

        <li<?php
        if ($pd_options['active_comment_tab']=='gplus') {
            echo ' class="active"';
        } ?>><a href="#gpluscom" data-toggle="tab">Google+<?php
        if (!empty($pd_options['comments_counters'])) {
            /* ?> (<div class="g-commentcount" data-href="<?php echo get_permalink(); ?>"></div>)<?php */
        } ?></a></li>
        <?php }

        if (!empty($pd_options['disqus_comments'])) { ?>

                    <li<?php if ($pd_options['active_comment_tab']=='disqus') {
                        echo ' class="active"';
                    } ?>><a href="#disqus" data-toggle="tab">Disqus<?php
                        if (!empty($pd_options['comments_counters'])) {
                            ?> (<script type="text/javascript">
                                var disqus_shortname = 'peadig';
                                var disqus_title = '<?php the_title(); ?>';
                                var disqus_url = '<?php the_permalink(); ?>';

                                (function () {
                                var s = document.createElement('script'); s.async = true;
                                s.type = 'text/javascript';
                                s.src = '//' + disqus_shortname + '.disqus.com/count.js';
                                (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
                                }());
                                </script><a href="#disqus" data-toggle="tab">#disqus_thread">)</a><?php
                        } ?></a></li><?php
                } ?>

        </ul>
        <?php } ?>
        <?php CustomHook::comments_after_comment_tab() ?>
        <?php CustomHook::comments_before_tab_content() ?>
        <?php if (!empty($pd_options['comments_tabs'])) : ?>
 


    <div id="CommentTabs" class="tab-content">
    <?php endif; ?>
    <?php if (!empty($pd_options['pdwp_comments'])) { ?>
    <?php if (!empty($pd_options['comments_tabs'])) {
        ?><div class="tab-pane fade<?php
        if ($pd_options['active_comment_tab']=='wpcom') {
            echo ' in active';
        } ?>" id="wpcom"><?php
    } ?>
<?php if (!empty($pd_options['pdwp_comments'])) { ?>
    <?php
    if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME'])){
        die ('Please do not load this page directly. Thanks!');
    }
    if ( post_password_required() ) { ?>
    This post is password protected. Enter the password to view comments.
    <?php
    return;
}
}
?>
<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
    <div class="navigation">
        <div class="next-posts"><?php previous_comments_link() ?></div>
        <div class="prev-posts"><?php next_comments_link() ?></div>
    </div>
<?php endif; // Check for comment navigation. ?>
<ul class="commentlist">
    <?php wp_list_comments('type=comment&callback=peadig_comment'); ?>
</ul>

<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
    <div class="navigation">
        <div class="next-posts"><?php previous_comments_link() ?></div>
        <div class="prev-posts"><?php next_comments_link() ?></div>
    </div>
<?php endif; // Check for comment navigation. ?>

<?php if ( ! comments_open() ) : ?>
    <p class="no-comments"><?php _e( 'Comments are closed.', 'twentyfourteen' ); ?></p>
<?php endif; ?>


<?php
$commenter = wp_get_current_commenter();
$req = get_option( 'require_name_email' );
$aria_req = ( $req ? " aria-required='true'" : '' );
if (empty($required_text)) {$required_text='';}
$comments_args = array(
    'comment_field' =>  '<div class="form-group">
    <label for="comment">Comment</label>
    <textarea class="form-control" name="comment" id="comment" cols="58" rows="10" tabindex="4"></textarea>
    </div>',
    'comment_notes_before' => '<p class="comment-notes">' .
    __( 'Your email address will not be published.' , 'peadig-framework') . ( $req ? $required_text : '' ) .
    '</p>',

    'comment_notes_after' => '<div class="form-group comment-form-author form-allowed-tags">' .
    sprintf(
      __( 'You may use these <abbr title="HyperText Markup Language">HTML</abbr> tags and attributes: %s' ),
      ' <code>' . allowed_tags() . '</code>'
      ) . '</div>
    <div class="row form-group comment-form-submit"><button type="submit" id="submit-new" class="btn btn-primary pull-right"><span>'.__('Post Comment').'</span></button></div>',


    'fields' => apply_filters( 'comment_form_default_fields', array(

        'author' =>
        '<div class="form-group comment-form-author">' .
        '<label for="author">' . __( 'Name', 'domainreference' ) . '</label> ' .
        ( $req ? '<span class="required">*</span>' : '' ) .
        '<input class="form-control" id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) .
        '" size="22"' . $aria_req . ' /></div>',

        'email' =>
        '<div class="form-group comment-form-email"><label for="email">' . __( 'Email', 'domainreference' ) . '</label> ' .
        ( $req ? '<span class="required">*</span>' : '' ) .
        '<input class="form-control" id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .
        '" size="22"' . $aria_req . ' /></div>',

        'url' =>
        '<div class="form-group comment-form-url"><label for="url">' .
        __( 'Website', 'domainreference' ) . '</label>' .
        '<input class="form-control" id="url" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) .
        '" size="30" /></div>'
        )
),
);
comment_form($comments_args);
?>
<?php if (!empty($pd_options['comments_tabs'])) { echo '</div>'; }
}
?>



        <?php if (!empty($pd_options['pdfb_comments'])) { ?>

            <?php if (!empty($pd_options['comments_tabs'])) { ?><div class="tab-pane fade<?php if ($pd_options['active_comment_tab']=='fbcom') {echo ' in active'; } ?>" id="fbcom"><?php } ?>
            <div class="fb-comments" data-href="<?php the_permalink(); ?>" data-width="100%" data-num-posts="<?php echo $pd_options['pdfb_comnum']; ?>" data-colorscheme="<?php echo $pd_options['fbcom_color']; ?>"></div>
            <?php if (!empty($pd_options['comments_tabs'])) { ?></div><?php } ?>
        <?php } ?>

        <?php if (!empty($pd_options['pdgplus_comments'])) { ?>

            <?php if (!empty($pd_options['comments_tabs'])) { ?><div class="tab-pane fade<?php if ($pd_options['active_comment_tab']=='gplus') {echo ' in active'; } ?>" id="gpluscom"><?php } ?>
            <g:comments href="<?php the_permalink(); ?>" width="600" first_party_property="BLOGGER" view_type="FILTERED_POSTMOD"></g:comments>
            <?php if (!empty($pd_options['comments_tabs'])) { ?></div><?php } ?>

        <?php } ?>


            <?php if (!empty($pd_options['disqus_comments'])) { ?>

                <?php if (!empty($pd_options['comments_tabs'])) { ?>
                    <div class="tab-pane fade<?php if ($pd_options['active_comment_tab']=='disqus') {echo ' in active'; } ?>" id="disqus">
                <?php } ?>

                <div id="disqus_thread"></div>
                <script type="text/javascript">
                    var disqus_shortname = '<?php echo $pd_options['disqus_shortname']; ?>';
                    var disqus_title = '<?php the_title(); ?>';
                    var disqus_url = '<?php the_permalink(); ?>';
                    (function() {
                    var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
                    dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
                    })();
                </script>

                    <?php if (!empty($pd_options['comments_tabs'])) { ?>
                        </div>
                    <?php } ?>
                <?php } ?>

            <?php if (!empty($pd_options['comments_tabs'])) { ?>
            </div>
        <?php } ?>
        <?php CustomHook::comments_after_tab_content() ?>
        <?php endif; ?>
</div>