<?php $pd_options = get_option('peadig'); ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head profile="http://gmpg.org/xfn/11">
        <?php CustomHook::header_inside_head_top() ?>
        <meta charset="<?php bloginfo('charset');   ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <link rel="shortcut icon" href="<?php
        if(empty($pd_options['favicon']['url'])){
            echo get_template_directory_uri().'/img/favicon.png';
        } else {
            echo $pd_options['favicon']['url'];
        }
        ?>">

        <link rel="apple-touch-icon" href="<?php
        if(empty($pd_options['apple_touch']['url'])){
            echo get_template_directory_uri().'/img/apple-touch-icon.png';
        } else {
            echo $pd_options['apple_touch']['url'];
        }
        ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php
        if(!empty($pd_options['mobile_bar'])){
            echo '<meta name="theme-color" content="'.$pd_options['mobile_bar'].'">';
        } ?>

        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
        <![endif]-->
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
        <?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>
        <?php CustomHook::header_inside_head_above_wp_head() ?>
        <?php wp_head(); ?>
        <?php CustomHook::header_inside_head_below_wp_head() ?>
    </head>
    <body <?php body_class(); ?>>
        <?php CustomHook::header_inside_body_top(); ?>
        <?php DefaultHook::facebook_sdk(); ?>
        <?php CustomHook::header_above_header() ?>
        <header>
            <div class="container">
                <div class="row">
                    <?php DefaultHook::header() ?>

                    <?php CustomHook::header_inside_header_top() ?>

                    <?php DefaultHook::header_navigation() ?>

                    <?php CustomHook::header_inside_header_bottom() ?>
                </div>
            </div>
        </header>
        <?php CustomHook::header_below_header() ?>
        <div id="body">
            <?php if(!empty($pd_options['sidebar_above_content'])): ?>
            <div class="container">
                <div class="row">
                    <div id="above-content-widgets" class="row col-sm-12">
                        <?php dynamic_sidebar( 'above-content' ); ?>
                    </div>
                </div>
            </div>
            <?php endif; ?>
            <?php if (!is_front_page() && $pd_options['use_breadcrumbs']=='1'): ?>
                <div class="container">
                    <div class="row">
                        <?php CustomHook::header_above_breadcrumb() ?>

                        <?php DefaultHook::breadcrumbs();?>

                        <?php CustomHook::header_below_breadcrumb(); ?>

                    </div>
                </div>
            <?php endif; ?>