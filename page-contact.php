<?php
/*
Template Name: Contact Page
*/
?>

<?php get_header(); ?>

<?php if(isset($_POST['submit'])){ ?>

    <?php DefaultHook::page_contact_template(1, $_POST); ?>

<?php } else { ?>

    <?php DefaultHook::page_contact_template(0); ?>

<?php } ?>

<?php DefaultHook::footer(); ?>