<div class="peadig_sc_overlay">
    <div class="peadig_sc_inner">
    <h1>PEADIG shortcodes</h1>
    <select id="pd_sc_chooser">
        <option>
            Choose Your Shortcode
        </option>

        <optgroup label="Grid Shortcodes">
            <option value="1">
                Grid Row
            </option>
            <option value="2">
                Grid Span
            </option>
        </optgroup>

        <optgroup label="Typography">
            <option value="18">
                Jumbotron
            </option>
            <option value="19">
                Well
            </option>
            <option value="20">
                Lead Paragraph
            </option>
            <option value="21">
                Emphasis
            </option>
            <option value="22">
                Abbr
            </option>
            <option value="23">
                Quote
            </option>
            <option value="24">
                List
            </option>
            <option value="25">
                List Item
            </option>
            <option value="26">
                List Group
            </option>
            <option value="27">
                List Group Item
            </option>
            <option value="28">
                Definition List
            </option>
            <option value="29">
                Definition Item
            </option>
            <option value="30">
                Pre / Code
            </option>
            <option value="31">
                Panel
            </option>
        </optgroup>

        <optgroup label="Tables">
            <option value="40">
                Table
            </option>
            <option value="41">
                Table Header Row
            </option>
            <option value="42">
                Table Header Cell
            </option>
            <option value="43">
                Table Body
            </option>
            <option value="44">
                Table Row
            </option>
            <option value="45">
                Table Cell
            </option>
        </optgroup>

        <optgroup label="Search">
            <option value="60">
                Search Form
            </option>
        </optgroup>

        <optgroup label="Buttons">
            <option value="80">
                Button
            </option>
            <option value="81">
                Button Group
            </option>
            <option value="82">
                Button Toolbar
            </option>
        </optgroup>


        <optgroup label="Icons">
            <option value="100">
                Icon
            </option>
            <option value="101">
                Icon
            </option>
        </optgroup>


        <optgroup label="Labels">
            <option value="120">
                Label / Badge
            </option>
        </optgroup>


        <optgroup label="Alerts">
            <option value="160">
                Alerts
            </option>
        </optgroup>





        <optgroup label="Progress Bar">
            <option value="180">
                Progress
            </option>
        </optgroup>



        <optgroup label="Tabbed Content">
            <option value="200">
                Tab Heading
            </option>
            <option value="201">
                Tab Title
            </option>
            <option value="202">
                Tabs
            </option>
            <option value="203">
                Tab Pane
            </option>
        </optgroup>



        <optgroup label="Modal / Popups">
            <option value="220">
                Modal Launch
            </option>
            <option value="221">
                Modal
            </option>
        </optgroup>




        <optgroup label="Accordian">
            <option value="240">
                Accordian
            </option>
            <option value="241">
                Accordian Item
            </option>
        </optgroup>




        <optgroup label="Toottip / Popover">
            <option value="260">
                Tooltip
            </option>
            <option value="261">
                Popover
            </option>
        </optgroup>




        <optgroup label="Carousel">
            <option value="280">
                Carousel
            </option>
            <option value="281">
                Carousel Item
            </option>
        </optgroup>

    </select>
        <div class="clear"></div>

    <?php
    include "../../classes/shortcode-mce-builder.php";
    ?>



        <div class="pd_sc_content">

            <?php
            $sc = new ShortcodeMCEBuilder(1, array( "span", "id", "class", "lg", "md", "sm", "fluid", "offset"));
            $sc->build("Grid Row", "Use this to add a new row of content");

            $sc = new ShortcodeMCEBuilder(2, array( "span", "offset", "id", "class", "lg", "md", "sm"));
            $sc->build("Grid Span", "Desc todo");





            $sc = new ShortcodeMCEBuilder(18, array("id", "class", "title", "lg", "md", "sm", "text"));
            $sc->build("Jumbotron", "Adds a jumbotron thingy.");

            $sc = new ShortcodeMCEBuilder(19, array("id", "class", "lg", "md", "sm", "text"));
            $sc->build("Well", "Adds a well.");

            $sc = new ShortcodeMCEBuilder(20, array("id", "class", "lg", "md", "sm", "text"));
            $sc->build("Lead Paragraph", "Add a lead in paragraph that has feature styling.");

            $sc = new ShortcodeMCEBuilder(21, array("id", "class", "lg", "md", "sm", "text", "type_dd"));
            $sc->build("Emphasis", "Desc todo");

            $sc = new ShortcodeMCEBuilder(22, array("id", "class", "lg", "md", "sm", "text", "title", "initialism"));
            $sc->build("Abbreviation", "Desc todo");

            $sc = new ShortcodeMCEBuilder(23, array(  "id", "class", "lg", "md", "sm", "source", "right", "text"));
            $sc->build("Quote", "Wrap your text in a Quote tag");

            $sc = new ShortcodeMCEBuilder(24, array("id" ,"class","lg", "md", "sm", "ordered", "unstyled","inline"));
            $sc->build("List", "A list of items, put list items shortcodes within here.");

            $sc = new ShortcodeMCEBuilder(25, array(  "id", "class", "lg", "md", "sm", "text"));
            $sc->build("List Item", "Items to include within your lists.");

            $sc = new ShortcodeMCEBuilder(26, array("id" ,"class","lg", "md", "sm", "type_lg"));
            $sc->build("List Group", "");

            $sc = new ShortcodeMCEBuilder(27, array("id", "class", "lg", "md", "sm", "active", "title", "type_lg", "text"));
            $sc->build("List Group Item", "");

            $sc = new ShortcodeMCEBuilder(28, array("id", "class", "lg", "md", "sm", "horizontal"));
            $sc->build("Def List", "A definition list.");

            $sc = new ShortcodeMCEBuilder(29, array(  "id", "class", "lg", "md", "sm", "title", "text"));
            $sc->build("Def Item", "An item to include in a definition list.");

            $sc = new ShortcodeMCEBuilder(30, array(  "id", "class", "lg", "md", "sm", "block", "text"));
            $sc->build("Pre / Code", "Pre or code blcoks of text.");

            $sc = new ShortcodeMCEBuilder(31, array(  "id", "class", "lg", "md", "sm", "header", "footer", "panel_color", "text"));
            $sc->build("Panel", "Build a Panel");





            $sc = new ShortcodeMCEBuilder(40, array(  "id", "class", "lg", "md", "sm", "striped", "bordered", "hover", "condensed", "caption"));
            $sc->build("Table", "");

            $sc = new ShortcodeMCEBuilder(41, array(  "id", "class", "lg", "md", "sm", "striped", "bordered", "hover", "condensed", "caption"));
            $sc->build("Table Header", "");

            $sc = new ShortcodeMCEBuilder(42, array(  "id", "class", "lg", "md", "sm", "striped", "bordered", "hover", "condensed", "caption", "text"));
            $sc->build("Table Header Cell", "");

            $sc = new ShortcodeMCEBuilder(43, array());
            $sc->build("Table Body", "");

            $sc = new ShortcodeMCEBuilder(44, array( "id", "class", "lg", "md", "sm", "rowspan"));
            $sc->build("Table Row", "");

            $sc = new ShortcodeMCEBuilder(45, array(  "id", "class", "colspan", "text"));
            $sc->build("Table Cell", "");





            $sc = new ShortcodeMCEBuilder(60, array());
            $sc->build("Search Form", "");





            $sc = new ShortcodeMCEBuilder(80, array("id", "class", "lg", "md", "sm", "type_btn", "link", "btn_size", "block_btn", "disabled", "text"));
            $sc->build("Button", "");

            $sc = new ShortcodeMCEBuilder(81, array("id", "class", "lg", "md", "sm", "vertical", "justified"));
            $sc->build("Button Group", "");

            $sc = new ShortcodeMCEBuilder(82, array("id", "class", "lg", "md", "sm", "vertical", "justified"));
            $sc->build("Button Toolbar", "");





            $sc = new ShortcodeMCEBuilder(100, array("id", "class", "lg", "md", "sm", "type_icon", "white"));
            $sc->build("Glyphicons", "");






            $sc = new ShortcodeMCEBuilder(120, array("id", "class", "type_label", "text"));
            $sc->build("Labels / Badge", "");






            $sc = new ShortcodeMCEBuilder(160, array("id", "class", "lg", "md", "sm", "type_alert", "block_alert", "dismissbutton", "text"));
            $sc->build("Alerts", "");





            $sc = new ShortcodeMCEBuilder(180, array("id", "class", "lg", "md", "sm", "progress_type", "striped", "animated", "amount"));
            $sc->build("Progress", "");




            $sc = new ShortcodeMCEBuilder(200, array("id", "class", "lg", "md", "sm"));
            $sc->build("Tab Headings", "");

            $sc = new ShortcodeMCEBuilder(201, array("id", "class", "lg", "md", "sm", "title", "active"));
            $sc->build("Tab Title", "");

            $sc = new ShortcodeMCEBuilder(202, array("text"));
            $sc->build("Tabs", "");

            $sc = new ShortcodeMCEBuilder(203, array("id", "active", "text"));
            $sc->build("Tab Pane", "");





            $sc = new ShortcodeMCEBuilder(220, array("id", "class", "lg", "md", "sm", "text"));
            $sc->build("Modal Launch", "");

            $sc = new ShortcodeMCEBuilder(221, array("id", "class", "lg", "md", "sm", "title", "closebutton", "closetext", "xbutton", "text"));
            $sc->build("Modal", "");





            $sc = new ShortcodeMCEBuilder(240, array("id", "class", "lg", "md", "sm"));
            $sc->build("Accordian", "");

            $sc = new ShortcodeMCEBuilder(241, array("id", "class", "accordianid", "title", "active", "text"));
            $sc->build("Accordian Item", "");





            $sc = new ShortcodeMCEBuilder(260, array("title", "placement", "allowhtml", "class", "text"));
            $sc->build("Tooltip", "");

            $sc = new ShortcodeMCEBuilder(261, array("title", "placement", "allowhtml", "class", "text", "trigger"));
            $sc->build("Popover", "");





            $sc = new ShortcodeMCEBuilder(280, array("id", "class", "lg", "md", "sm"));
            $sc->build("Carousel", "");

            $sc = new ShortcodeMCEBuilder(281, array("title", "image", "alt", "link", "active", "text"));
            $sc->build("Carousel Item", "");





            ?>


        </div>
    </div>
</div>