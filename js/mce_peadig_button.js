(function() {
    tinymce.create('tinymce.plugins.Peadig', {
        init : function(editor, url) {
            editor.addButton('peadig_button', {
                title : 'peadig_button.custom',
                image : url+'/mce-options/20x20.png',
                onclick : function() {
                jQuery(document).ready(function ($){
                    if($("div").hasClass("peadig_sc_overlay")){
                        $(".peadig_sc_overlay").show();
                    } else {
                        $.get("../wp-content/themes/peadig/js/mce-options/overlay_option.php", function(data){
                            $("body").append(data);

                        });



                        $(".peadig_sc_overlay").show();

                        $(document.body).on("click",".peadig_sc_overlay",function(){
                            $(".peadig_sc_overlay").hide();

                        });

                        $(document.body).on("click",".peadig_sc_inner",function(e){
                            e.stopImmediatePropagation();
                        });

                        $(document).keyup(function(e) {
                            if (e.keyCode == 27) {
                                $(".peadig_sc_overlay").hide();
                            }
                        });

                        $(document.body).on("change","#pd_sc_chooser",function(){
                            var selected = $(this).val();

                            $(".pd_sc_codes").hide(1,function(){
                                $("#pd_sc_"+selected).show()
                            });
                        });

                        //outputting of shortcodes here
                        $(document.body).on("click",".pd_sc_insert",function()
                        {
                            var inserting = $(this).attr("data-value");



                            if(inserting == 1)
                            {
                                var content = "[peadig-row";

                                var span = $(".pd_sc_"+inserting+"_span").val();
                                var offset = $(".pd_sc_"+inserting+"_offset").val();
                                var id = $(".pd_sc_"+inserting+"_id").val();
                                var classname = $(".pd_sc_"+inserting+"_class").val();
                                var fluid = $(".pd_sc_"+inserting+"_fluid").val();
                                var lg = $(".pd_sc_"+inserting+"_lg").val();
                                var sm = $(".pd_sc_"+inserting+"_sm").val();
                                var md = $(".pd_sc_"+inserting+"_md").val();


                                if(span != ""){
                                    content += " span=\""+span+"\""
                                }
                                if(id != ""){
                                    content += " id=\""+id+"\""
                                }
                                if(classname != ""){
                                    content += " class=\""+classname+"\""
                                }
                                if(fluid != ""){
                                    content += " fluid=\""+fluid+"\""
                                }
                                if(lg != ""){
                                    content += " lg=\""+lg+"\""
                                }
                                if(sm != ""){
                                    content += " sm=\""+sm+"\""
                                }
                                if(md != ""){
                                    content += " md=\""+md+"\""
                                }

                                content += "][/peadig-row]";

                            }
                                else if(inserting == 2)
                            {
                                var content = "[peadig-col";

                                var span = $(".pd_sc_"+inserting+"_span").val();
                                var offset = $(".pd_sc_"+inserting+"_offset").val();
                                var id = $(".pd_sc_"+inserting+"_id").val();
                                var classname = $(".pd_sc_"+inserting+"_class").val();
                                var lg = $(".pd_sc_"+inserting+"_lg").val();
                                var sm = $(".pd_sc_"+inserting+"_sm").val();
                                var md = $(".pd_sc_"+inserting+"_md").val();

                                if(span != ""){
                                    content += " span=\""+span+"\""
                                }
                                if(id != ""){
                                    content += " id=\""+id+"\""
                                }
                                if(classname != ""){
                                    content += " class=\""+classname+"\""
                                }
                                if(lg != ""){
                                    content += " lg=\""+lg+"\""
                                }
                                if(sm != ""){
                                    content += " sm=\""+sm+"\""
                                }
                                if(md != ""){
                                    content += " md=\""+md+"\""
                                }

                                content += "][/peadig-col]";

                            }
                                else if(inserting == 18)
                            {
                                var content = "[jumbotron";
                                var text = $(".pd_sc_"+inserting+"_text").val();

                                var id = $(".pd_sc_"+inserting+"_id").val();
                                var classname = $(".pd_sc_"+inserting+"_class").val();
                                var title = $(".pd_sc_"+inserting+"_title").val();
                                var lg = $(".pd_sc_"+inserting+"_lg").val();
                                var sm = $(".pd_sc_"+inserting+"_sm").val();
                                var md = $(".pd_sc_"+inserting+"_md").val();


                                if(id != ""){
                                    content += " id=\""+id+"\""
                                }
                                if(classname != ""){
                                    content += " class=\""+classname+"\""
                                }
                                if(title != ""){
                                    content += " title=\""+title+"\""
                                }
                                if(lg != ""){
                                    content += " lg=\""+lg+"\""
                                }
                                if(sm != ""){
                                    content += " sm=\""+sm+"\""
                                }
                                if(md != ""){
                                    content += " md=\""+md+"\""
                                }

                                content += "]";

                                if(text != ""){
                                    content += text
                                }

                                content += "[/jumbotron]";

                            }
                                else if(inserting == 19)
                            {
                                var content = "[well";
                                var text = $(".pd_sc_"+inserting+"_text").val();

                                var id = $(".pd_sc_"+inserting+"_id").val();
                                var classname = $(".pd_sc_"+inserting+"_class").val();
                                var lg = $(".pd_sc_"+inserting+"_lg").val();
                                var sm = $(".pd_sc_"+inserting+"_sm").val();
                                var md = $(".pd_sc_"+inserting+"_md").val();


                                if(id != ""){
                                    content += " id=\""+id+"\""
                                }
                                if(classname != ""){
                                    content += " class=\""+classname+"\""
                                }
                                if(lg != ""){
                                    content += " lg=\""+lg+"\""
                                }
                                if(sm != ""){
                                    content += " sm=\""+sm+"\""
                                }
                                if(md != ""){
                                    content += " md=\""+md+"\""
                                }

                                content += "]";

                                if(text != ""){
                                    content += text
                                }

                                content += "[/well]";

                            }
                                else if(inserting == 20)
                            {
                                var content = "[p-lead";
                                var text = $(".pd_sc_"+inserting+"_text").val();

                                var id = $(".pd_sc_"+inserting+"_id").val();
                                var classname = $(".pd_sc_"+inserting+"_class").val();
                                var lg = $(".pd_sc_"+inserting+"_lg").val();
                                var sm = $(".pd_sc_"+inserting+"_sm").val();
                                var md = $(".pd_sc_"+inserting+"_md").val();


                                if(id != ""){
                                    content += " id=\""+id+"\""
                                }
                                if(classname != ""){
                                    content += " class=\""+classname+"\""
                                }
                                if(lg != ""){
                                    content += " lg=\""+lg+"\""
                                }
                                if(sm != ""){
                                    content += " sm=\""+sm+"\""
                                }
                                if(md != ""){
                                    content += " md=\""+md+"\""
                                }

                                content += "]";

                                if(text != ""){
                                    content += text
                                }

                                content += "[/p-lead]";

                            }
                                else if(inserting == 21)
                            {
                                var content = "[emph";

                                var id = $(".pd_sc_"+inserting+"_id").val();
                                var classname = $(".pd_sc_"+inserting+"_class").val();
                                var lg = $(".pd_sc_"+inserting+"_lg").val();
                                var sm = $(".pd_sc_"+inserting+"_sm").val();
                                var md = $(".pd_sc_"+inserting+"_md").val();
                                var type = $("select.pd_sc_"+inserting+"_type option:selected").val();

                                text = $(".pd_sc_"+inserting+"_text").val();

                                if(id != ""){
                                    content += " id=\""+id+"\""
                                }
                                if(classname != ""){
                                    content += " class=\""+classname+"\""
                                }
                                if(lg != ""){
                                    content += " lg=\""+lg+"\""
                                }
                                if(sm != ""){
                                    content += " sm=\""+sm+"\""
                                }
                                if(md != ""){
                                    content += " md=\""+md+"\""
                                }
                                if(type != ""){
                                    content += " type=\""+type+"\""
                                }

                                content += "]";

                                if(text != ""){
                                    content += text
                                }

                                content += "[/emph]";


                            }
                                else if(inserting == 22)
                            {
                                var content = "[abbr";

                                var id = $(".pd_sc_"+inserting+"_id").val();
                                var classname = $(".pd_sc_"+inserting+"_class").val();
                                var lg = $(".pd_sc_"+inserting+"_lg").val();
                                var sm = $(".pd_sc_"+inserting+"_sm").val();
                                var md = $(".pd_sc_"+inserting+"_md").val();
                                var initialism = $("select.pd_sc_"+inserting+"_initialism option:selected").val();

                                text = $(".pd_sc_"+inserting+"_text").val();

                                title = $(".pd_sc_"+inserting+"_title").val();

                                if(id != ""){
                                    content += " id=\""+id+"\""
                                }
                                if(classname != ""){
                                    content += " class=\""+classname+"\""
                                }
                                if(lg != ""){
                                    content += " lg=\""+lg+"\""
                                }
                                if(sm != ""){
                                    content += " sm=\""+sm+"\""
                                }
                                if(md != ""){
                                    content += " md=\""+md+"\""
                                }
                                if(type != ""){
                                    content += " type=\""+type+"\""
                                }
                                if(title != ""){
                                    content += " title=\""+title+"\""
                                }
                                if(initialism != ""){
                                    content += " initialism=\""+initialism+"\""
                                }

                                content += "]";

                                if(text != ""){
                                    content += text
                                }

                                content += "[/abbr]";


                            }
                                else if(inserting == 23)
                            {
                                var content = "[quote";

                                var id = $(".pd_sc_"+inserting+"_id").val();
                                var classname = $(".pd_sc_"+inserting+"_class").val();
                                var lg = $(".pd_sc_"+inserting+"_lg").val();
                                var sm = $(".pd_sc_"+inserting+"_sm").val();
                                var md = $(".pd_sc_"+inserting+"_md").val();
                                var type = $(".pd_sc_"+inserting+"_type").val();

                                var text = $(".pd_sc_"+inserting+"_text").val();
                                var source = $(".pd_sc_"+inserting+"_source").val();

                                var right = $(".pd_sc_"+inserting+"_right").val();

                                if(id != ""){
                                    content += " id=\""+id+"\""
                                }
                                if(classname != ""){
                                    content += " class=\""+classname+"\""
                                }
                                if(lg != ""){
                                    content += " lg=\""+lg+"\""
                                }
                                if(sm != ""){
                                    content += " sm=\""+sm+"\""
                                }
                                if(md != ""){
                                    content += " md=\""+md+"\""
                                }
                                if(title != ""){
                                    content += " title=\""+title+"\""
                                }
                                if(right != ""){
                                    content += " right=\""+right+"\""
                                }
                                if(source != ""){
                                    content += " source=\""+source+"\""
                                }


                                content += "]";

                                if(text != ""){
                                    content += text
                                }

                                content += "[/quote]";


                            }
                                else if(inserting == 24)
                            {
                                var content = "[list";

                                var id = $(".pd_sc_"+inserting+"_id").val();
                                var classname = $(".pd_sc_"+inserting+"_class").val();
                                var lg = $(".pd_sc_"+inserting+"_lg").val();
                                var sm = $(".pd_sc_"+inserting+"_sm").val();
                                var md = $(".pd_sc_"+inserting+"_md").val();

                                var ordered = $(".pd_sc_"+inserting+"_ordered").val();
                                var unstyled = $(".pd_sc_"+inserting+"_unstyled").val();
                                var inline = $(".pd_sc_"+inserting+"_inline").val();

                                if(id != ""){
                                    content += " id=\""+id+"\""
                                }
                                if(classname != ""){
                                    content += " class=\""+classname+"\""
                                }
                                if(lg != ""){
                                    content += " lg=\""+lg+"\""
                                }
                                if(md != ""){
                                content += " md=\""+md+"\""
                                }
                                if(sm != ""){
                                    content += " sm=\""+sm+"\""
                                }

                                if(ordered != ""){
                                    content += " ordered=\""+ordered+"\""
                                }
                                if(unstyled != ""){
                                    content += " unstyled=\""+unstyled+"\""
                                }
                                if(inline != ""){
                                    content += " inline=\""+inline+"\""
                                }


                                content += "]";

                                content += "[/list]";

                            }
                                else if(inserting == 25)
                            {
                                var content = "[item";

                                var id = $(".pd_sc_"+inserting+"_id").val();
                                var classname = $(".pd_sc_"+inserting+"_class").val();
                                var lg = $(".pd_sc_"+inserting+"_lg").val();
                                var sm = $(".pd_sc_"+inserting+"_sm").val();
                                var md = $(".pd_sc_"+inserting+"_md").val();

                                text = $(".pd_sc_"+inserting+"_text").val();

                                if(id != ""){
                                    content += " id=\""+id+"\""
                                }
                                if(classname != ""){
                                    content += " class=\""+classname+"\""
                                }
                                if(lg != ""){
                                    content += " lg=\""+lg+"\""
                                }
                                if(md != ""){
                                    content += " md=\""+md+"\""
                                }
                                if(sm != ""){
                                    content += " sm=\""+sm+"\""
                                }


                                content += "]";

                                if(text != ""){
                                    content += text
                                }

                                content += "[/item]";


                            }
                                else if(inserting == 26)
                            {
                                var content = "[list-group";

                                var id = $(".pd_sc_"+inserting+"_id").val();
                                var classname = $(".pd_sc_"+inserting+"_class").val();
                                var lg = $(".pd_sc_"+inserting+"_lg").val();
                                var sm = $(".pd_sc_"+inserting+"_sm").val();
                                var md = $(".pd_sc_"+inserting+"_md").val();

                                var type = $("select.pd_sc_"+inserting+"_type_lg option:selected").val();

                                if(id != ""){
                                    content += " id=\""+id+"\""
                                }
                                if(classname != ""){
                                    content += " class=\""+classname+"\""
                                }
                                if(lg != ""){
                                    content += " lg=\""+lg+"\""
                                }
                                if(md != ""){
                                content += " md=\""+md+"\""
                                }
                                if(sm != ""){
                                    content += " sm=\""+sm+"\""
                                }

                                if(type != ""){
                                    content += " type=\""+type+"\""
                                }

                                content += "]";

                                content += "[/list-group]";

                            }
                                else if(inserting == 27)
                            {
                                var content = "[item";

                                var id = $(".pd_sc_"+inserting+"_id").val();
                                var classname = $(".pd_sc_"+inserting+"_class").val();
                                var lg = $(".pd_sc_"+inserting+"_lg").val();
                                var sm = $(".pd_sc_"+inserting+"_sm").val();
                                var md = $(".pd_sc_"+inserting+"_md").val();

                                var active = $(".pd_sc_"+inserting+"_active").val();
                                var title = $(".pd_sc_"+inserting+"_title").val();
                                var type = $(".pd_sc_"+inserting+"_type_lg").val();
                                var text = $(".pd_sc_"+inserting+"_text").val();

                                if(id != ""){
                                    content += " id=\""+id+"\""
                                }
                                if(classname != ""){
                                    content += " class=\""+classname+"\""
                                }
                                if(lg != ""){
                                    content += " lg=\""+lg+"\""
                                }
                                if(md != ""){
                                    content += " md=\""+md+"\""
                                }
                                if(sm != ""){
                                    content += " sm=\""+sm+"\""
                                }
                                if(active != ""){
                                    content += " active=\""+active+"\""
                                }
                                if(title != ""){
                                    content += " title=\""+title+"\""
                                }
                                if(type != ""){
                                    content += " type=\""+type+"\""
                                }

                                content += "]";

                                if(text != ""){
                                    content += text
                                }

                                content += "[/item]";


                            }
                                else if(inserting == 28)
                            {
                                var content = "[def-list";

                                var id = $(".pd_sc_"+inserting+"_id").val();
                                var classname = $(".pd_sc_"+inserting+"_class").val();
                                var lg = $(".pd_sc_"+inserting+"_lg").val();
                                var sm = $(".pd_sc_"+inserting+"_sm").val();
                                var md = $(".pd_sc_"+inserting+"_md").val();
                                var horizontal = $(".pd_sc_"+inserting+"_horizontal").val();

                                if(id != ""){
                                    content += " id=\""+id+"\""
                                }
                                if(classname != ""){
                                    content += " class=\""+classname+"\""
                                }
                                if(lg != ""){
                                    content += " lg=\""+lg+"\""
                                }
                                if(md != ""){
                                    content += " md=\""+md+"\""
                                }
                                if(sm != ""){
                                    content += " sm=\""+sm+"\""
                                }
                                if(horizontal != ""){
                                    content += " horizontal=\""+horizontal+"\""
                                }

                                content += "]";

                                content += "[/def-list]";


                            }
                                else if(inserting == 29)
                            {
                                var content = "[def-item";

                                var id = $(".pd_sc_"+inserting+"_id").val();
                                var classname = $(".pd_sc_"+inserting+"_class").val();
                                var lg = $(".pd_sc_"+inserting+"_lg").val();
                                var sm = $(".pd_sc_"+inserting+"_sm").val();
                                var md = $(".pd_sc_"+inserting+"_md").val();

                                var title = $(".pd_sc_"+inserting+"_title").val();
                                var text = $(".pd_sc_"+inserting+"_text").val();

                                if(id != ""){
                                    content += " id=\""+id+"\""
                                }
                                if(classname != ""){
                                    content += " class=\""+classname+"\""
                                }
                                if(lg != ""){
                                    content += " lg=\""+lg+"\""
                                }
                                if(md != ""){
                                    content += " md=\""+md+"\""
                                }
                                if(sm != ""){
                                    content += " sm=\""+sm+"\""
                                }
                                if(title != ""){
                                    content += " title=\""+title+"\""
                                }

                                content += "]";

                                if(text != ""){
                                    content += text
                                }

                                content += "[/def-item]";

                            }
                                else if(inserting == 30)
                            {
                                var content = "[p-code";

                                var id = $(".pd_sc_"+inserting+"_id").val();
                                var classname = $(".pd_sc_"+inserting+"_class").val();
                                var lg = $(".pd_sc_"+inserting+"_lg").val();
                                var md = $(".pd_sc_"+inserting+"_md").val();
                                var sm = $(".pd_sc_"+inserting+"_sm").val();

                                var block = $(".pd_sc_"+inserting+"_block").val();
                                var text = $(".pd_sc_"+inserting+"_text").val();

                                if(id != ""){
                                    content += " id=\""+id+"\""
                                }
                                if(classname != ""){
                                    content += " class=\""+classname+"\""
                                }
                                if(lg != ""){
                                    content += " lg=\""+lg+"\""
                                }
                                if(md != ""){
                                    content += " md=\""+md+"\""
                                }
                                if(sm != ""){
                                    content += " sm=\""+sm+"\""
                                }
                                if(block != ""){
                                    content += " block=\""+block+"\""
                                }


                                content += "]";

                                if(text != ""){
                                    content += text
                                }

                                content += "[/p-code]";


                            }
                                else if(inserting == 31)
                            {
                                var content = "[panel";

                                var id = $(".pd_sc_"+inserting+"_id").val();
                                var classname = $(".pd_sc_"+inserting+"_class").val();
                                var lg = $(".pd_sc_"+inserting+"_lg").val();
                                var md = $(".pd_sc_"+inserting+"_md").val();
                                var sm = $(".pd_sc_"+inserting+"_sm").val();

                                var header = $(".pd_sc_"+inserting+"_header").val();
                                var footer = $(".pd_sc_"+inserting+"_footer").val();
                                var color = $(".pd_sc_"+inserting+"_color").val();

                                var text = $(".pd_sc_"+inserting+"_text").val();

                                if(id != ""){
                                    content += " id=\""+id+"\""
                                }
                                if(classname != ""){
                                    content += " class=\""+classname+"\""
                                }
                                if(lg != ""){
                                    content += " lg=\""+lg+"\""
                                }
                                if(md != ""){
                                    content += " md=\""+md+"\""
                                }
                                if(sm != ""){
                                    content += " sm=\""+sm+"\""
                                }
                                if(header != ""){
                                    content += " header=\""+header+"\""
                                }
                                if(footer != ""){
                                    content += " footer=\""+footer+"\""
                                }
                                if(color != ""){
                                    content += " color=\""+color+"\""
                                }


                                content += "]";

                                if(text != ""){
                                    content += text
                                }

                                content += "[/panel]";


                            }
                            else if(inserting == 40)
                            {
                                var content = "[table";

                                var id = $(".pd_sc_"+inserting+"_id").val();
                                var classname = $(".pd_sc_"+inserting+"_class").val();
                                var lg = $(".pd_sc_"+inserting+"_lg").val();
                                var sm = $(".pd_sc_"+inserting+"_sm").val();
                                var md = $(".pd_sc_"+inserting+"_md").val();

                                var striped = $(".pd_sc_"+inserting+"_striped").val();
                                var bordered = $(".pd_sc_"+inserting+"_bordered").val();
                                var hover = $(".pd_sc_"+inserting+"_hover").val();
                                var condensed = $(".pd_sc_"+inserting+"_condensed").val();
                                var caption = $(".pd_sc_"+inserting+"_caption").val();



                                if(id != ""){
                                    content += " id=\""+id+"\""
                                }
                                if(classname != ""){
                                    content += " class=\""+classname+"\""
                                }
                                if(lg != ""){
                                    content += " lg=\""+lg+"\""
                                }
                                if(md != ""){
                                    content += " md=\""+md+"\""
                                }
                                if(sm != ""){
                                    content += " sm=\""+sm+"\""
                                }
                                if(striped != ""){
                                    content += " striped=\""+striped+"\""
                                }
                                if(bordered != ""){
                                    content += " bordered=\""+bordered+"\""
                                }
                                if(hover != ""){
                                    content += " hover=\""+hover+"\""
                                }
                                if(condensed != ""){
                                    content += " condensed=\""+condensed+"\""
                                }
                                if(caption != ""){
                                    content += " caption=\""+caption+"\""
                                }

                                content += "]";



                                content += "[/table]";


                            }
                            else if(inserting == 41)
                            {
                                var content = "[thead";

                                var id = $(".pd_sc_"+inserting+"_id").val();
                                var classname = $(".pd_sc_"+inserting+"_class").val();
                                var lg = $(".pd_sc_"+inserting+"_lg").val();
                                var sm = $(".pd_sc_"+inserting+"_sm").val();
                                var md = $(".pd_sc_"+inserting+"_md").val();

                                var striped = $(".pd_sc_"+inserting+"_striped").val();
                                var bordered = $(".pd_sc_"+inserting+"_bordered").val();
                                var hover = $(".pd_sc_"+inserting+"_hover").val();
                                var condensed = $(".pd_sc_"+inserting+"_condensed").val();
                                var caption = $(".pd_sc_"+inserting+"_caption").val();


                                if(id != ""){
                                    content += " id=\""+id+"\""
                                }
                                if(classname != ""){
                                    content += " class=\""+classname+"\""
                                }
                                if(lg != ""){
                                    content += " lg=\""+lg+"\""
                                }
                                if(md != ""){
                                    content += " md=\""+md+"\""
                                }
                                if(sm != ""){
                                    content += " sm=\""+sm+"\""
                                }
                                if(striped != ""){
                                    content += " striped=\""+striped+"\""
                                }
                                if(bordered != ""){
                                    content += " bordered=\""+bordered+"\""
                                }
                                if(hover != ""){
                                    content += " hover=\""+hover+"\""
                                }
                                if(condensed != ""){
                                    content += " condensed=\""+condensed+"\""
                                }
                                if(caption != ""){
                                    content += " caption=\""+caption+"\""
                                }

                                content += "]";



                                content += "[/thead]";


                            }
                            else if(inserting == 42)
                            {
                                content = "[thcell";

                                var id = $(".pd_sc_"+inserting+"_id").val();
                                var classname = $(".pd_sc_"+inserting+"_class").val();
                                var lg = $(".pd_sc_"+inserting+"_lg").val();
                                var sm = $(".pd_sc_"+inserting+"_sm").val();
                                var md = $(".pd_sc_"+inserting+"_md").val();

                                var striped = $(".pd_sc_"+inserting+"_striped").val();
                                var bordered = $(".pd_sc_"+inserting+"_bordered").val();
                                var hover = $(".pd_sc_"+inserting+"_hover").val();
                                var condensed = $(".pd_sc_"+inserting+"_condensed").val();

                                var caption = $(".pd_sc_"+inserting+"_caption").val();

                                var text = $(".pd_sc_"+inserting+"_text").val();


                                if(id != ""){
                                    content += " id=\""+id+"\""
                                }
                                if(classname != ""){
                                    content += " class=\""+classname+"\""
                                }
                                if(lg != ""){
                                    content += " lg=\""+lg+"\""
                                }
                                if(md != ""){
                                    content += " md=\""+md+"\""
                                }
                                if(sm != ""){
                                    content += " sm=\""+sm+"\""
                                }
                                if(striped != ""){
                                    content += " striped=\""+striped+"\""
                                }
                                if(bordered != ""){
                                    content += " bordered=\""+bordered+"\""
                                }
                                if(hover != ""){
                                    content += " hover=\""+hover+"\""
                                }
                                if(condensed != ""){
                                    content += " condensed=\""+condensed+"\""
                                }
                                if(caption != ""){
                                    content += " caption=\""+caption+"\""
                                }

                                content += "]";

                                if(text != ""){
                                    content += text
                                }


                                content += "[/thcell]";


                            }
                            else if(inserting == 43)
                            {
                                var content = "[tbody]";
                                content += "[/tbody]";


                            }
                            else if(inserting == 44)
                            {
                                var content = "[tr";

                                var id = $(".pd_sc_"+inserting+"_id").val();
                                var classname = $(".pd_sc_"+inserting+"_class").val();
                                var lg = $(".pd_sc_"+inserting+"_lg").val();
                                var sm = $(".pd_sc_"+inserting+"_sm").val();
                                var md = $(".pd_sc_"+inserting+"_md").val();

                                var type = $(".pd_sc_"+inserting+"_type").val();
                                var rowspan = $(".pd_sc_"+inserting+"_rowspan").val();


                                if(id != ""){
                                    content += " id=\""+id+"\""
                                }
                                if(classname != ""){
                                    content += " class=\""+classname+"\""
                                }
                                if(lg != ""){
                                    content += " lg=\""+lg+"\""
                                }
                                if(md != ""){
                                    content += " md=\""+md+"\""
                                }
                                if(sm != ""){
                                    content += " sm=\""+sm+"\""
                                }
                                if(rowspan != ""){
                                    content += " hover=\""+rowspan+"\""
                                }

                                content += "]";


                                content += "[/tr]";


                            }
                            else if(inserting == 45)
                            {
                                var content = "[td";

                                var id = $(".pd_sc_"+inserting+"_id").val();
                                var classname = $(".pd_sc_"+inserting+"_class").val();

                                var colspan = $(".pd_sc_"+inserting+"_colspan").val();

                                var text = $(".pd_sc_"+inserting+"_text").val();



                                if(id != ""){
                                    content += " id=\""+id+"\""
                                }
                                if(classname != ""){
                                    content += " class=\""+classname+"\""
                                }
                                if(colspan != ""){
                                    content += " colspan=\""+colspan+"\""
                                }
                                content += "]";

                                if(text != ""){
                                    content += text
                                }

                                content += "[/td]";

                            }
                            else if(inserting == 60)
                            {
                                var content = "[p-search]";
                                content += "[/p-search]";


                            }
                            else if(inserting == 80)
                            {
                                var content = "[peadig-button";

                                var id = $(".pd_sc_"+inserting+"_id").val();
                                var classname = $(".pd_sc_"+inserting+"_class").val();
                                var lg = $(".pd_sc_"+inserting+"_lg").val();
                                var sm = $(".pd_sc_"+inserting+"_sm").val();
                                var md = $(".pd_sc_"+inserting+"_md").val();

                                var type = $(".pd_sc_"+inserting+"_type").val();
                                var link = $(".pd_sc_"+inserting+"_link").val();
                                var size = $(".pd_sc_"+inserting+"_size").val();
                                var block = $(".pd_sc_"+inserting+"_block").val();
                                var disbled = $(".pd_sc_"+inserting+"_disabled").val();

                                var text = $(".pd_sc_"+inserting+"_text").val();


                                if(id != ""){
                                    content += " id=\""+id+"\""
                                }
                                if(classname != ""){
                                    content += " class=\""+classname+"\""
                                }
                                if(lg != ""){
                                    content += " lg=\""+lg+"\""
                                }
                                if(md != ""){
                                    content += " md=\""+md+"\""
                                }
                                if(sm != ""){
                                    content += " sm=\""+sm+"\""
                                }
                                if(type != ""){
                                    content += " type=\""+type+"\""
                                }
                                if(link != ""){
                                    content += " link=\""+link+"\""
                                }
                                if(size != ""){
                                    content += " size=\""+size+"\""
                                }
                                if(block != ""){
                                    content += " block=\""+block+"\""
                                }

                                content += "]";

                                if(text != ""){
                                    content += text
                                }

                                content += "[/peadig-button]";


                            }
                            else if(inserting == 81)
                            {
                                var content = "[button-group";

                                var id = $(".pd_sc_"+inserting+"_id").val();
                                var classname = $(".pd_sc_"+inserting+"_class").val();
                                var lg = $(".pd_sc_"+inserting+"_lg").val();
                                var sm = $(".pd_sc_"+inserting+"_sm").val();
                                var md = $(".pd_sc_"+inserting+"_md").val();

                                var vertical = $(".pd_sc_"+inserting+"_vertical").val();
                                var justified = $(".pd_sc_"+inserting+"_justified").val();


                                if(id != ""){
                                    content += " id=\""+id+"\""
                                }
                                if(classname != ""){
                                    content += " class=\""+classname+"\""
                                }
                                if(lg != ""){
                                    content += " lg=\""+lg+"\""
                                }
                                if(md != ""){
                                    content += " md=\""+md+"\""
                                }
                                if(sm != ""){
                                    content += " sm=\""+sm+"\""
                                }
                                if(vertical != ""){
                                    content += " vertical=\""+vertical+"\""
                                }
                                if(justified != ""){
                                    content += " justified=\""+justified+"\""
                                }

                                content += "]";


                                content += "[/button-group]";


                            }
                            else if(inserting == 82)
                            {
                                var content = "[button-toolbar";

                                var id = $(".pd_sc_"+inserting+"_id").val();
                                var classname = $(".pd_sc_"+inserting+"_class").val();
                                var lg = $(".pd_sc_"+inserting+"_lg").val();
                                var sm = $(".pd_sc_"+inserting+"_sm").val();
                                var md = $(".pd_sc_"+inserting+"_md").val();

                                var vertical = $(".pd_sc_"+inserting+"_vertical").val();


                                if(id != ""){
                                    content += " id=\""+id+"\""
                                }
                                if(classname != ""){
                                    content += " class=\""+classname+"\""
                                }
                                if(lg != ""){
                                    content += " lg=\""+lg+"\""
                                }
                                if(md != ""){
                                    content += " md=\""+md+"\""
                                }
                                if(sm != ""){
                                    content += " sm=\""+sm+"\""
                                }
                                if(vertical != ""){
                                    content += " vertical=\""+vertical+"\""
                                }

                                content += "]";


                                content += "[/button-toolbar]";


                            }
                            else if(inserting == 100)
                            {
                                var content = "[peadig-i";

                                var id = $(".pd_sc_"+inserting+"_id").val();
                                var classname = $(".pd_sc_"+inserting+"_class").val();
                                var lg = $(".pd_sc_"+inserting+"_lg").val();
                                var sm = $(".pd_sc_"+inserting+"_sm").val();
                                var md = $(".pd_sc_"+inserting+"_md").val();

                                var type = $(".pd_sc_"+inserting+"_type").val();
                                var white = $(".pd_sc_"+inserting+"_white").val();

                                var text = $(".pd_sc_"+inserting+"_text").val();


                                if(id != ""){
                                    content += " id=\""+id+"\""
                                }
                                if(classname != ""){
                                    content += " class=\""+classname+"\""
                                }
                                if(lg != ""){
                                    content += " lg=\""+lg+"\""
                                }
                                if(md != ""){
                                    content += " md=\""+md+"\""
                                }
                                if(sm != ""){
                                    content += " sm=\""+sm+"\""
                                }
                                if(type != ""){
                                    content += " type=\""+type+"\""
                                }
                                if(white != ""){
                                    content += " white=\""+white+"\""
                                }

                                content += "]";

                                content += "[/peadig-i]";


                            }
                            else if(inserting == 120)
                            {
                                var content = "[peadig-label";

                                var id = $(".pd_sc_"+inserting+"_id").val();
                                var classname = $(".pd_sc_"+inserting+"_class").val();
                                var lg = $(".pd_sc_"+inserting+"_lg").val();
                                var sm = $(".pd_sc_"+inserting+"_sm").val();
                                var md = $(".pd_sc_"+inserting+"_md").val();

                                var type = $(".pd_sc_"+inserting+"_type").val();
                                var white = $(".pd_sc_"+inserting+"_white").val();

                                var text = $(".pd_sc_"+inserting+"_text").val();


                                if(id != ""){
                                    content += " id=\""+id+"\""
                                }
                                if(classname != ""){
                                    content += " class=\""+classname+"\""
                                }
                                if(type != ""){
                                    content += " type=\""+type+"\""
                                }

                                content += "]";

                                if(text != ""){
                                    content += text
                                }

                                content += "[/peadig-label]";


                            }
                            else if(inserting == 160)
                            {
                                var content = "[peadig-alert";

                                var id = $(".pd_sc_"+inserting+"_id").val();
                                var classname = $(".pd_sc_"+inserting+"_class").val();
                                var lg = $(".pd_sc_"+inserting+"_lg").val();
                                var sm = $(".pd_sc_"+inserting+"_sm").val();
                                var md = $(".pd_sc_"+inserting+"_md").val();

                                var type = $(".pd_sc_"+inserting+"_type").val();
                                var block = $(".pd_sc_"+inserting+"_block").val();
                                var dismissbutton = $(".pd_sc_"+inserting+"_dismissbutton").val();

                                var text = $(".pd_sc_"+inserting+"_text").val();


                                if(id != ""){
                                    content += " id=\""+id+"\""
                                }
                                if(classname != ""){
                                    content += " class=\""+classname+"\""
                                }
                                if(lg != ""){
                                    content += " lg=\""+lg+"\""
                                }
                                if(md != ""){
                                    content += " md=\""+md+"\""
                                }
                                if(sm != ""){
                                    content += " sm=\""+sm+"\""
                                }
                                if(type != ""){
                                    content += " type=\""+type+"\""
                                }
                                if(block != ""){
                                    content += " block=\""+block+"\""
                                }
                                if(dismissbutton != ""){
                                    content += " dismissbutton=\""+dismissbutton+"\""
                                }

                                content += "]";

                                if(text != ""){
                                    content += text
                                }

                                content += "[/peadig-alert]";


                            }
                            else if(inserting == 180)
                            {
                                var content = "[progress";

                                var id = $(".pd_sc_"+inserting+"_id").val();
                                var classname = $(".pd_sc_"+inserting+"_class").val();
                                var lg = $(".pd_sc_"+inserting+"_lg").val();
                                var sm = $(".pd_sc_"+inserting+"_sm").val();
                                var md = $(".pd_sc_"+inserting+"_md").val();

                                var type = $(".pd_sc_"+inserting+"_type").val();
                                var striped = $(".pd_sc_"+inserting+"_striped").val();
                                var animated = $(".pd_sc_"+inserting+"_animated").val();
                                var amount = $(".pd_sc_"+inserting+"_amount").val();


                                if(id != ""){
                                    content += " id=\""+id+"\""
                                }
                                if(classname != ""){
                                    content += " class=\""+classname+"\""
                                }
                                if(lg != ""){
                                    content += " lg=\""+lg+"\""
                                }
                                if(md != ""){
                                    content += " md=\""+md+"\""
                                }
                                if(sm != ""){
                                    content += " sm=\""+sm+"\""
                                }
                                if(type != ""){
                                    content += " type=\""+type+"\""
                                }
                                if(striped != ""){
                                    content += " striped=\""+striped+"\""
                                }
                                if(animated != ""){
                                    content += " animated=\""+animated+"\""
                                }
                                if(amount != ""){
                                    content += " amount=\""+amount+"\""
                                }

                                content += "]";

                                content += "[/progress]";


                            }
                            else if(inserting == 200)
                            {
                                var content = "[tab-headings";

                                var id = $(".pd_sc_"+inserting+"_id").val();
                                var classname = $(".pd_sc_"+inserting+"_class").val();
                                var lg = $(".pd_sc_"+inserting+"_lg").val();
                                var sm = $(".pd_sc_"+inserting+"_sm").val();
                                var md = $(".pd_sc_"+inserting+"_md").val();



                                if(id != ""){
                                    content += " id=\""+id+"\""
                                }
                                if(classname != ""){
                                    content += " class=\""+classname+"\""
                                }
                                if(lg != ""){
                                    content += " lg=\""+lg+"\""
                                }
                                if(md != ""){
                                    content += " md=\""+md+"\""
                                }
                                if(sm != ""){
                                    content += " sm=\""+sm+"\""
                                }

                                content += "]";

                                content += "[/tab-headings]";


                            }
                            else if(inserting == 201)
                            {
                                var content = "[tab-title";

                                var id = $(".pd_sc_"+inserting+"_id").val();
                                var classname = $(".pd_sc_"+inserting+"_class").val();
                                var lg = $(".pd_sc_"+inserting+"_lg").val();
                                var sm = $(".pd_sc_"+inserting+"_sm").val();
                                var md = $(".pd_sc_"+inserting+"_md").val();

                                var title = $(".pd_sc_"+inserting+"_title").val();
                                var active = $(".pd_sc_"+inserting+"_active").val();

                                if(id != ""){
                                    content += " id=\""+id+"\""
                                }
                                if(classname != ""){
                                    content += " class=\""+classname+"\""
                                }
                                if(lg != ""){
                                    content += " lg=\""+lg+"\""
                                }
                                if(md != ""){
                                    content += " md=\""+md+"\""
                                }
                                if(sm != ""){
                                    content += " sm=\""+sm+"\""
                                }
                                if(title != ""){
                                    content += " title=\""+title+"\""
                                }
                                if(active != ""){
                                    content += " active=\""+active+"\""
                                }

                                content += "]";

                                content += "[/tab-title]";


                            }
                            else if(inserting == 202)
                            {
                                var content = "[tabs";

                                var text = $(".pd_sc_"+inserting+"_text").val();


                                content += "]";

                                if(text != ""){
                                    content += text
                                }

                                content += "[/tabs]";


                            }
                            else if(inserting == 203)
                            {
                                var content = "[tab-pane";

                                var id = $(".pd_sc_"+inserting+"_id").val();

                                var active = $(".pd_sc_"+inserting+"_active").val();

                                var text = $(".pd_sc_"+inserting+"_text").val();


                                if(id != ""){
                                    content += " id=\""+id+"\""
                                }
                                if(active != ""){
                                    content += " active=\""+active+"\""
                                }

                                content += "]";

                                if(text != ""){
                                    content += text
                                }

                                content += "[/tab-pane]";


                            }
                            else if(inserting == 220)
                            {
                                var content = "[modal-launch";

                                var id = $(".pd_sc_"+inserting+"_id").val();
                                var classname = $(".pd_sc_"+inserting+"_class").val();
                                var lg = $(".pd_sc_"+inserting+"_lg").val();
                                var sm = $(".pd_sc_"+inserting+"_sm").val();
                                var md = $(".pd_sc_"+inserting+"_md").val();

                                var text = $(".pd_sc_"+inserting+"_text").val();


                                if(id != ""){
                                    content += " id=\""+id+"\""
                                }
                                if(classname != ""){
                                    content += " class=\""+classname+"\""
                                }
                                if(lg != ""){
                                    content += " lg=\""+lg+"\""
                                }
                                if(md != ""){
                                    content += " md=\""+md+"\""
                                }
                                if(sm != ""){
                                    content += " sm=\""+sm+"\""
                                }

                                content += "]";

                                if(text != ""){
                                    content += text
                                }

                                content += "[/modal-launch]";


                            }
                            else if(inserting == 221)
                            {
                                var content = "[modal";

                                var id = $(".pd_sc_"+inserting+"_id").val();
                                var classname = $(".pd_sc_"+inserting+"_class").val();
                                var lg = $(".pd_sc_"+inserting+"_lg").val();
                                var sm = $(".pd_sc_"+inserting+"_sm").val();
                                var md = $(".pd_sc_"+inserting+"_md").val();

                                var title = $(".pd_sc_"+inserting+"_title").val();
                                var closebutton = $(".pd_sc_"+inserting+"_closebutton").val();
                                var closetext = $(".pd_sc_"+inserting+"_closetext").val();
                                var xbutton = $(".pd_sc_"+inserting+"_xbutton").val();

                                var text = $(".pd_sc_"+inserting+"_text").val();


                                if(id != ""){
                                    content += " id=\""+id+"\""
                                }
                                if(classname != ""){
                                    content += " class=\""+classname+"\""
                                }
                                if(lg != ""){
                                    content += " lg=\""+lg+"\""
                                }
                                if(md != ""){
                                    content += " md=\""+md+"\""
                                }
                                if(sm != ""){
                                    content += " sm=\""+sm+"\""
                                }
                                if(title != ""){
                                    content += " title=\""+title+"\""
                                }
                                if(closebutton != ""){
                                    content += " closebutton=\""+closebutton+"\""
                                }
                                if(closetext != ""){
                                    content += " closetext=\""+closetext+"\""
                                }
                                if(xbutton != ""){
                                    content += " xbutton=\""+xbutton+"\""
                                }

                                content += "]";

                                if(text != ""){
                                    content += text
                                }

                                content += "[/modal]";


                            }
                            else if(inserting == 240)
                            {
                                var content = "[accordian";

                                var id = $(".pd_sc_"+inserting+"_id").val();
                                var classname = $(".pd_sc_"+inserting+"_class").val();
                                var lg = $(".pd_sc_"+inserting+"_lg").val();
                                var sm = $(".pd_sc_"+inserting+"_sm").val();
                                var md = $(".pd_sc_"+inserting+"_md").val();


                                if(id != ""){
                                    content += " id=\""+id+"\""
                                }
                                if(classname != ""){
                                    content += " class=\""+classname+"\""
                                }
                                if(lg != ""){
                                    content += " lg=\""+lg+"\""
                                }
                                if(md != ""){
                                    content += " md=\""+md+"\""
                                }
                                if(sm != ""){
                                    content += " sm=\""+sm+"\""
                                }

                                content += "]";

                                content += "[/accordian]";


                            }
                            else if(inserting == 241)
                            {
                                var content = "[accordian-item";

                                var id = $(".pd_sc_"+inserting+"_id").val();
                                var classname = $(".pd_sc_"+inserting+"_class").val();

                                var accordianid = $(".pd_sc_"+inserting+"_accordianid").val();
                                var title = $(".pd_sc_"+inserting+"_title").val();
                                var active = $(".pd_sc_"+inserting+"_active").val();

                                var text = $(".pd_sc_"+inserting+"_text").val();

                                if(id != ""){
                                    content += " id=\""+id+"\""
                                }
                                if(classname != ""){
                                    content += " class=\""+classname+"\""
                                }
                                if(accordianid != ""){
                                    content += " accordianid=\""+accordianid+"\""
                                }
                                if(title != ""){
                                    content += " title=\""+title+"\""
                                }
                                if(active != ""){
                                    content += " active=\""+active+"\""
                                }

                                content += "]";

                                if(text != ""){
                                    content += text
                                }

                                content += "[/accordian-item]";


                            }
                            else if(inserting == 260)
                            {
                                var content = "[tooltip";

                                var title = $(".pd_sc_"+inserting+"_title").val();
                                var placement = $(".pd_sc_"+inserting+"_placement").val();
                                var allowhtml = $(".pd_sc_"+inserting+"_allowhtml").val();
                                var classname = $(".pd_sc_"+inserting+"_class").val();

                                var text = $(".pd_sc_"+inserting+"_text").val();


                                if(title != ""){
                                    content += " title=\""+title+"\""
                                }
                                if(placement != ""){
                                    content += " placement=\""+placement+"\""
                                }
                                if(allowhtml != ""){
                                    content += " allowhtml=\""+allowhtml+"\""
                                }
                                if(classname != ""){
                                    content += " classname=\""+classname+"\""
                                }

                                content += "]";

                                if(text != ""){
                                    content += text
                                }

                                content += "[/tooltip]";

                            }
                            else if(inserting == 261)
                            {
                                var content = "[popover";

                                var title = $(".pd_sc_"+inserting+"_title").val();
                                var placement = $(".pd_sc_"+inserting+"_placement").val();
                                var allowhtml = $(".pd_sc_"+inserting+"_allowhtml").val();
                                var classname = $(".pd_sc_"+inserting+"_class").val();
                                var trigger = $(".pd_sc_"+inserting+"_trigger").val();

                                var text = $(".pd_sc_"+inserting+"_text").val();


                                if(title != ""){
                                    content += " title=\""+title+"\""
                                }
                                if(placement != ""){
                                    content += " placement=\""+placement+"\""
                                }
                                if(allowhtml != ""){
                                    content += " allowhtml=\""+allowhtml+"\""
                                }
                                if(classname != ""){
                                    content += " classname=\""+classname+"\""
                                }

                                content += "]";

                                if(text != ""){
                                    content += text
                                }

                                content += "[/popover]";



                            }
                            else if(inserting == 280)
                            {
                                var content = "[carousel";

                                var id = $(".pd_sc_"+inserting+"_id").val();
                                var classname = $(".pd_sc_"+inserting+"_class").val();
                                var lg = $(".pd_sc_"+inserting+"_lg").val();
                                var sm = $(".pd_sc_"+inserting+"_sm").val();
                                var md = $(".pd_sc_"+inserting+"_md").val();

                                var type = $(".pd_sc_"+inserting+"_type").val();
                                var link = $(".pd_sc_"+inserting+"_link").val();
                                var size = $(".pd_sc_"+inserting+"_size").val();
                                var block = $(".pd_sc_"+inserting+"_block").val();
                                var disbled = $(".pd_sc_"+inserting+"_disabled").val();

                                var text = $(".pd_sc_"+inserting+"_text").val();


                                if(id != ""){
                                    content += " id=\""+id+"\""
                                }
                                if(classname != ""){
                                    content += " class=\""+classname+"\""
                                }
                                if(lg != ""){
                                    content += " lg=\""+lg+"\""
                                }
                                if(md != ""){
                                    content += " md=\""+md+"\""
                                }
                                if(sm != ""){
                                    content += " sm=\""+sm+"\""
                                }

                                content += "]";

                                if(text != ""){
                                    content += text
                                }

                                content += "[/carousel]";


                            }
                            else if(inserting == 281)
                            {
                                var content = "[carousel-item";

                                var title = $(".pd_sc_"+inserting+"_title").val();
                                var image = $(".pd_sc_"+inserting+"_image").val();
                                var alt = $(".pd_sc_"+inserting+"_alt").val();
                                var link = $(".pd_sc_"+inserting+"_link").val();
                                var active = $(".pd_sc_"+inserting+"_active").val();

                                var text = $(".pd_sc_"+inserting+"_text").val();

                                if(title != ""){
                                    content += " title=\""+title+"\""
                                }
                                if(image != ""){
                                    content += " image=\""+image+"\""
                                }
                                if(alt != ""){
                                    content += " alt=\""+alt+"\""
                                }
                                if(link != ""){
                                    content += " link=\""+link+"\""
                                }
                                if(active != ""){
                                    content += " active=\""+active+"\""
                                }

                                content += "]";

                                if(text != ""){
                                    content += text
                                }

                                content += "[/carousel-item]";


                            }

                            editor.execCommand('mceInsertContent', false, content);

                            $(".peadig_sc_overlay").hide();
                        });




                    }

                });
                }
            });
        },
        createControl : function(n, cm) {
            return null;
        },
        getInfo : function() {
            return {
                longname : "Peadig Custom Shortcode Options",
                author : 'Shane Jones, Alex Moss   ',
                authorurl : 'http://peadig.com/',
                infourl : 'http://peadig.com/',
                version : "1.1"
            };
        }
    });
    tinymce.PluginManager.add('peadig_button', tinymce.plugins.Peadig);
})();


